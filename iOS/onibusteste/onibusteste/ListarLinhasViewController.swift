//
//  ListarLinhasViewController.swift
//  onibusteste
//
//  Created by Henrique Ciraolo on 19/02/17.
//  Copyright © 2017 Henrique Ciraolo. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import ObjectMapper
import MBProgressHUD

class ListarLinhasViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tbvLinhas: UITableView!
    @IBOutlet weak var nbrTitle: UINavigationBar!
    
    //BUSCA GESTEC
    let headers: HTTPHeaders = [ "Authorization": "Basic cm9iby1lbXR1OmVtdHUxMDMw" ]
    let urlGestec = "http://200.144.29.93:8088/services.svc/linhas/getListaDeLinhas"
    
    var linhasMetropolitanas = [Linha]();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tbvLinhas.delegate = self;
        tbvLinhas.dataSource = self;
        
        let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true);
        loadingNotification.mode = MBProgressHUDMode.indeterminate;
        loadingNotification.labelText = "Carregando informações...";
        
        Alamofire.request(urlGestec, headers: headers).responseObject() { (response : DataResponse<RespostaObterLinhas>) in
            self.completaBuscaLinhas(resposta: response.result.value!)
        }
    }
    
    func completaBuscaLinhas(resposta : RespostaObterLinhas) {
        for linha in resposta.linhas! {
            if linha.chRegiao == 258 {
                linhasMetropolitanas.append(linha);
            }
        }
        
        tbvLinhas.reloadData();
        
        MBProgressHUD.hideAllHUDs(for: self.view, animated: true);
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return linhasMetropolitanas.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LinhaCell", for: indexPath) as! LinhaTableViewCell
        
        let linha = linhasMetropolitanas[indexPath.row]
        
        cell.lblCodigoLinha?.text = linha.codigo
        cell.lblOrigemLinha?.text = linha.origem
        cell.lblDestinoLinha?.text = linha.destino
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

class LinhaTableViewCell: UITableViewCell {
    @IBOutlet weak var lblCodigoLinha: UILabel!
    @IBOutlet weak var lblOrigemLinha: UILabel!
    @IBOutlet weak var lblDestinoLinha: UILabel!
}

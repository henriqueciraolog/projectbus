//
//  Linha2.swift
//  onibusteste
//
//  Created by Henrique Ciraolo on 12/02/17.
//  Copyright © 2017 Henrique Ciraolo. All rights reserved.
//

import Foundation
import ObjectMapper

class Linha2:Mappable {
    var id : Int?;
    var codigo : String?;
    var tp : Any?;
    var ts : Any?;
    var rotas : [Rota]?;
    var veiculos : [Veiculo]?;
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        codigo <- map["codigo"]
        tp <- map["tp"]
        ts <- map["ts"]
        rotas <- map["rotas"]
        veiculos <- map["veiculos"]
    }
}

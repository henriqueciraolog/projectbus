//
//  DetalheLinhaViewController.swift
//  onibusteste
//
//  Created by Henrique Ciraolo on 13/05/17.
//  Copyright © 2017 Henrique Ciraolo. All rights reserved.
//

import Foundation

class DetalheLinhaViewController: UIViewController {
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // remove all current segments to make sure it is empty:
        segmentedControl.removeAllSegments()
        
        // adding your segments, using the "for" loop is just for demonstration:
        for index in 0...4 {
            segmentedControl.insertSegmentWithTitle("Segment \(index + 1)", atIndex: index, animated: false)
        }
        
        // you can also remove a segment like this:
        // this removes the second segment "Segment 2"
        //segmentedControl.removeSegmentAtIndex(1, animated: false)
    }
    
    // and this is how you can access the changing of its value (make sure that event is "Value Changed")
    @IBAction func segmentControlValueChanged(sender: UISegmentedControl) {
        print("index of selected segment is: \(sender.selectedSegmentIndex)")
    }

}

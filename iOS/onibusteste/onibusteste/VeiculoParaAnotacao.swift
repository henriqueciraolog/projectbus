//
//  VeiculoParaAnotacao.swift
//  onibusteste
//
//  Created by Henrique Ciraolo on 15/02/17.
//  Copyright © 2017 Henrique Ciraolo. All rights reserved.
//

import Foundation
import MapKit

class VeiculoParaAnotacao: NSObject, MKAnnotation {
    let veiculo: Veiculo
    let coordinate: CLLocationCoordinate2D
    
    init(veiculo: Veiculo, coordinate: CLLocationCoordinate2D) {
        self.veiculo = veiculo
        self.coordinate = coordinate
        
        super.init()
    }
    
    var subtitle: String? {
        return veiculo.prefixo!
    }
}

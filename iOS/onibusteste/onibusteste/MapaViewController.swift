//
//  MapaViewController.swift
//  onibusteste
//
//  Created by Henrique Ciraolo on 18/02/17.
//  Copyright © 2017 Henrique Ciraolo. All rights reserved.
//

import UIKit
import MapKit
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import MBProgressHUD

class MapaViewController: UIViewController, MKMapViewDelegate {
    //BUSCA GESTEC
    let headers: HTTPHeaders = [ "Authorization": "Basic cm9iby1lbXR1OmVtdHUxMDMw" ]
    let urlGestec = "http://200.144.29.93:8088/services.svc/linhas/getListaDeLinhas"
    //BUSCA GPS
    let urlNoxxonsat = "https://bustime.noxxonsat.com.br/portal?linha=%@"
    let regionRadius: CLLocationDistance = 31000
    
    let base = 209715;
    
    var linhasMetropolitanas : [Linha] = [Linha]();
    var veiculos : [Veiculo] = [Veiculo]();
    @IBOutlet weak var mapView: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true);
        loadingNotification.mode = MBProgressHUDMode.indeterminate;
        loadingNotification.labelText = "Carregando...";
        
        self.mapView.delegate = self
        let initialLocation = CLLocation(latitude: -23.499747, longitude: -47.452970)
        centerMapOnLocation(location: initialLocation)
        Alamofire.request(urlGestec, headers: headers).responseObject() { (response : DataResponse<RespostaObterLinhas>) in
            self.completaBuscaLinhas(resposta: response.result.value!)
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if let polylineOverlay = overlay as? Polyline {
            let render = MKPolylineRenderer(polyline: polylineOverlay)
            let value = procuraPosicaoLinha(codigoLinha: polylineOverlay.codigoLinha!)
            let st = String(format:"%2X", (base * value))
            render.strokeColor = UIColor(hexString: st);
            return render
        }
        return MKOverlayRenderer()
    }
    
    func completaBuscaLinhas(resposta : RespostaObterLinhas) {
        for linha in resposta.linhas! {
            if linha.chRegiao == 258 {
                linhasMetropolitanas.append(linha);
            }
        }
        
        MBProgressHUD.hideAllHUDs(for: self.view, animated: true);
        getRotas();
    }
    
    func getRotas() {
        for linha in linhasMetropolitanas {
            let codigo = linha.codigo!
            let urlFinal = String(format: urlNoxxonsat, codigo)
            Alamofire.request(urlFinal, headers: headers).responseString { (response : DataResponse<String>) in
                if (response.response?.statusCode == 200) {
                    let resposta = self.convertStringToObject(response: response.result.value!);
                    for rota in (resposta.linhas?[0].rotas)! {
                        let coordinates = PolyUtil.decode(encodedPath: rota.encode!);
                        let polyline = Polyline(coordinates: coordinates, count: coordinates.count);
                        polyline.codigoLinha = rota.codigoLinha;
                        self.mapView.add(polyline, level: MKOverlayLevel.aboveRoads)
                    }
                }
            }
        }
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func convertStringToObject(response: String) -> RepostaObterDadosPortal {
        let string : String = String(data: (response.data(using: String.Encoding.isoLatin1))!, encoding: String.Encoding.isoLatin1)!;
        let utf8data : Data = string.data(using: String.Encoding.utf8)!;
        
        do {
            let anyObj: Any? = try JSONSerialization.jsonObject(with: utf8data, options: JSONSerialization.ReadingOptions.init(rawValue: 0))
            let resposta : RepostaObterDadosPortal = Mapper<RepostaObterDadosPortal>().map(JSON: anyObj as! [String : Any])!;
            return resposta;
        } catch {
            
        }
        
        return RepostaObterDadosPortal()!;
    }
    
    func procuraPosicaoLinha(codigoLinha : String) -> Int {
        var i = 0;
        for linha in linhasMetropolitanas {
            if (linha.codigo == codigoLinha) {
                return i;
            }
            i += 1;
        }
        return linhasMetropolitanas.count;
    }
}

extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.characters.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}

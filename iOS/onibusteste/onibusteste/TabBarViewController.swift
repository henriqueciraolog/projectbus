//
//  TabBarViewController.swift
//  onibusteste
//
//  Created by Henrique Ciraolo on 23/02/17.
//  Copyright © 2017 Henrique Ciraolo. All rights reserved.
//

import Foundation
import UIKit

class TabBarViewController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if (appDelegate.startInBusRealTime) {
            self.tabBarController?.selectedIndex = 1;
        }
    }
}

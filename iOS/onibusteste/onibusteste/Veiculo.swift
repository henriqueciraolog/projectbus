//
//  Veiculo.swift
//  onibusteste
//
//  Created by Henrique Ciraolo on 12/02/17.
//  Copyright © 2017 Henrique Ciraolo. All rights reserved.
//

import Foundation
import ObjectMapper
import MapKit

class Veiculo:NSObject, Mappable, MKAnnotation {
    var  version : Int?;
    var  consorcio : String?;
    var  empresa : String?;
    var  prefixo : String?;
    var  status : String?;
    var  codigoLinha : String?;
    var  sentidoLinha : String?;
    var  idRota : Int?;
    var  dataUltimaTransmissao : Int?;
    var  latitude : Double?;
    var  longitude : Double?;
    var  seqPonto : Int?;
    var  acessibilidade : String?;
    var  tipoOnibus : String?;
    var  capacidadeSentados : Int?;
    var  capacidadeEmPe : Int?;
    var  fabricante : String?;
    var  modelo : String?;
    var  ano : String?;
    var  dataAutorizacao : Int?;
    var  dataFimAutorizacao : Int?;
    var  alertaNaoTransmitiuCiclo : Bool?;
    var  dentroRota : Bool?;
    
    required init?(map: Map){
        
    }
    
    override init() {
        super.init();
    }
    
    func mapping(map: Map) {
        version <- map["version"]
        consorcio <- map["consorcio"]
        empresa <- map["empresa"]
        prefixo <- map["prefixo"]
        status <- map["status"]
        codigoLinha <- map["codigoLinha"]
        sentidoLinha <- map["sentidoLinha"]
        idRota <- map["idRota"]
        dataUltimaTransmissao <- map["dataUltimaTransmissao"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        seqPonto <- map["seqPonto"]
        acessibilidade <- map["acessibilidade"]
        tipoOnibus <- map["tipoOnibus"]
        capacidadeSentados <- map["capacidadeSentados"]
        capacidadeEmPe <- map["capacidadeEmPe"]
        fabricante <- map["fabricante"]
        modelo <- map["modelo"]
        ano <- map["ano"]
        dataAutorizacao <- map["dataAutorizacao"]
        dataFimAutorizacao <- map["dataFimAutorizacao"]
        alertaNaoTransmitiuCiclo <- map["alertaNaoTransmitiuCiclo"]
        dentroRota <- map["dentroRota"]
    }
    
    var title: String? {
        return prefixo;
    }
    
    var subtitle: String? {
        return "Linha: " + codigoLinha! + " Empresa: " + empresa!.replacingOccurrences(of: " RMS", with: "");
    }
    
    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2DMake(latitude!, longitude!)
    }
}

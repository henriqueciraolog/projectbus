//
//  RespostaObterLinhas.swift
//  onibusteste
//
//  Created by Henrique Ciraolo on 12/02/17.
//  Copyright © 2017 Henrique Ciraolo. All rights reserved.
//

import Foundation
import ObjectMapper

class RespostaObterLinhas:Mappable {
    var linhas:[Linha]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        linhas <- map["linhas"]
    }
}

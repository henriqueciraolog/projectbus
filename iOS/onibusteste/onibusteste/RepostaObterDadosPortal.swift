//
//  RepostaObterDadosPortal.swift
//  onibusteste
//
//  Created by Henrique Ciraolo on 12/02/17.
//  Copyright © 2017 Henrique Ciraolo. All rights reserved.
//

import Foundation
import ObjectMapper

class RepostaObterDadosPortal:Mappable {
    var linhas:[Linha2]?
    
    required init?(map: Map){
        
    }
    
    init?() {
        
    }
    
    func mapping(map: Map) {
        linhas <- map["linhas"]
    }
}

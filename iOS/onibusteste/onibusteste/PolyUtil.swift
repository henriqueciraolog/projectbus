//
//  PolyUtil.swift
//  onibusteste
//
//  Created by Henrique Ciraolo on 18/02/17.
//  Copyright © 2017 Henrique Ciraolo. All rights reserved.
//

import Foundation
import MapKit

class PolyUtil {
    class func decode (encodedPath : String) -> [CLLocationCoordinate2D] {
        let len = encodedPath.characters.count;
        var path = [CLLocationCoordinate2D]();
        var index = 0;
        var lat = 0;
        var lng = 0;
        var pointIndex = 0;
        while (index < len) {
            var index2 : Int = 0;
            var result = 1;
            var shift : Int = 0;
            while (true) {
                index2 = index + 1;
                let b : Int = ((Int((encodedPath as NSString).character(at: index)) - 63) - 1);
                result += b << shift;
                shift += 5;
                if (b < 31) {
                    break;
                }
                index = index2;
            }
            lat += (result & 1) != 0 ? (result >> 1) ^ -1 : result >> 1;
            result = 1;
            shift = 0;
            index = index2;
            while (true) {
                index2 = index + 1;
                let b : Int = ((Int((encodedPath as NSString).character(at: index)) - 63) - 1);
                result += b << shift;
                shift += 5;
                if (b < 31) {
                    break;
                }
                index = index2;
            }
            lng += (result & 1) != 0 ? (result >> 1) ^ -1 : result >> 1;
            path.append(CLLocationCoordinate2D(latitude: (Double(lat)) * 1.0E-5, longitude: (Double(lng)) * 1.0E-5));
            pointIndex += 1;
            index = index2;
        }
        return path;
    }
}

//
//  Linha.swift
//  onibusteste
//
//  Created by Henrique Ciraolo on 12/02/17.
//  Copyright © 2017 Henrique Ciraolo. All rights reserved.
//

import Foundation
import ObjectMapper

class Linha:Mappable {
    var chElemento : Int?;
    var chRegiao : Int?;
    var codigo : String?;
    var destino : String?;
    var origem : String?;
    var sentido : String?;
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        chElemento <- map["chElemento"]
        chRegiao <- map["chRegiao"]
        codigo <- map["codigo"]
        destino <- map["destino"]
        origem <- map["origem"]
        sentido <- map["sentido"]
    }
}

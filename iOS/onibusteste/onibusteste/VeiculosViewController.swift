//
//  ViewController.swift
//  onibusteste
//
//  Created by Henrique Ciraolo on 05/02/17.
//  Copyright © 2017 Henrique Ciraolo. All rights reserved.
//

import UIKit
import MapKit
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import MBProgressHUD

class VeiculosViewController: UIViewController, MKMapViewDelegate {
    //BUSCA GESTEC
    let headers: HTTPHeaders = [ "Authorization": "Basic cm9iby1lbXR1OmVtdHUxMDMw" ]
    let urlGestec = "http://200.144.29.93:8088/services.svc/linhas/getListaDeLinhas"
    //BUSCA GPS
    let urlNoxxonsat = "https://bustime.noxxonsat.com.br/portal?modo=mobile&linha=%@"
    let empresasOperadoras = ["Viação Calvip RMS", "Rápido Campinas RMS", "Viação São Roque RMS", "Viação Piracema RMS", "Vila Élvio RMS", "VB RMS", "São João RMS", "Tieteense Turismo RMS"];
    let regionRadius: CLLocationDistance = 31000
    
    var linhasMetropolitanas : [Linha] = [Linha]();
    var veiculos : [Veiculo] = [Veiculo]();
    @IBOutlet weak var mapView: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true);
        loadingNotification.mode = MBProgressHUDMode.indeterminate;
        loadingNotification.labelText = "Carregando...";
        
        self.mapView.delegate = self
        let initialLocation = CLLocation(latitude: -23.499747, longitude: -47.452970)
        centerMapOnLocation(location: initialLocation)
        Alamofire.request(urlGestec, headers: headers).responseObject() { (response : DataResponse<RespostaObterLinhas>) in
            self.completaBuscaLinhas(resposta: response.result.value!)
        }
    }
    
    func completaBuscaLinhas(resposta : RespostaObterLinhas) {
        for linha in resposta.linhas! {
            if linha.chRegiao == 258 {
                linhasMetropolitanas.append(linha);
            }
        }
        
        MBProgressHUD.hideAllHUDs(for: self.view, animated: true);
        getVeiculos();
        startUpdate();
    }
    
    func getVeiculos() {
        for linha in linhasMetropolitanas {
            let codigo = linha.codigo!
            let urlFinal = String(format: urlNoxxonsat, codigo)
            Alamofire.request(urlFinal, headers: headers).responseString { (response : DataResponse<String>) in
                if (response.response?.statusCode == 200) {
                    let resposta = self.convertStringToObject(response: response.result.value!);
                    for linha in resposta.linhas! {
                        self.removeAllPinsByCodigoLinha(codigoLinha: codigo);
                        for veiculo in linha.veiculos! {
                            self.veiculos.append(veiculo);
                            self.mapView.addAnnotation(veiculo);
                        }
                    }
                }
            }
        }
    }
    
    func startUpdate() {
        _ = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(VeiculosViewController.getVeiculos), userInfo: nil, repeats: true)
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func convertStringToObject(response: String) -> RepostaObterDadosPortal {
        let string : String = String(data: (response.data(using: String.Encoding.isoLatin1))!, encoding: String.Encoding.isoLatin1)!;
        let utf8data : Data = string.data(using: String.Encoding.utf8)!;
        
        do {
            let anyObj: Any? = try JSONSerialization.jsonObject(with: utf8data, options: JSONSerialization.ReadingOptions.init(rawValue: 0))
            let resposta : RepostaObterDadosPortal = Mapper<RepostaObterDadosPortal>().map(JSON: anyObj as! [String : Any])!;
            return resposta;
        } catch {
            
        }
        
        return RepostaObterDadosPortal()!;
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? Veiculo {
            let identifier = annotation.empresa!+annotation.sentidoLinha!;
            var view: MKAnnotationView
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
                as MKAnnotationView? { // 2
                dequeuedView.annotation = annotation
                view = dequeuedView //as! MKPinAnnotationView
            } else {
                // 3
                view = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                view.canShowCallout = true
                view.calloutOffset = CGPoint(x: -5, y: 5)
                view.rightCalloutAccessoryView = UIButton(type:.detailDisclosure) as UIView
                
                var i = 0;
                var eoSelect = 0;
                while i < empresasOperadoras.count {
                    if (annotation.empresa == empresasOperadoras[i]) {
                        eoSelect = i;
                        break;
                    }
                    i += 1;
                }
                
                if (annotation.sentidoLinha?.uppercased() == "VOLTA") {
                    view.image = UIImage(named: String(format:"onibus_volta_%d", eoSelect+1));
                } else {
                    view.image = UIImage(named: String(format:"onibus_ida_%d", eoSelect+1));
                }
                
                view.backgroundColor = UIColor.clear
            }
            
            return view
        }
        return nil
    }
    
    func removeAllPinsByCodigoLinha(codigoLinha: String) {
        
        var veiculosToRemove : [Veiculo] = [Veiculo]();
        for v in veiculos {
            if (v.codigoLinha == codigoLinha) {
                veiculosToRemove.append(v);
            }
        }
        self.mapView.removeAnnotations(veiculosToRemove);
        for v in veiculosToRemove {
            let index = veiculos.index(of: v);
            veiculos.remove(at: index!);
        }
    }
}


//
//  Ponto.swift
//  onibusteste
//
//  Created by Henrique Ciraolo on 18/02/17.
//  Copyright © 2017 Henrique Ciraolo. All rights reserved.
//

import Foundation
import ObjectMapper

class Ponto : Mappable {
    var id : Int?;
    var latitude : Double?;
    var longitude : Double?;
    var raio : Int?;
    var endereco : String?;
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        raio <- map["raio"]
        endereco <- map["endereco"]
    }
}

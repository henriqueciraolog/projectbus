//
//  Rota.swift
//  onibusteste
//
//  Created by Henrique Ciraolo on 18/02/17.
//  Copyright © 2017 Henrique Ciraolo. All rights reserved.
//

import Foundation
import ObjectMapper

class Rota:Mappable {
    var id : Int?;
    var version : Int?;
    var codigoLinha : String?;
    var sentido : String?;
    var /*List<Ponto>*/ pontos : [Ponto]?;
    var destino : String?;
    var encode : String?;
    var distancia : String?;
    var distanciaGestec : String?;
    var tempo : String?;
    var raioCerca : String?;
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        version <- map["version"]
        codigoLinha <- map["codigoLinha"]
        sentido <- map["sentido"]
        pontos <- map["pontos"]
        destino <- map["destino"]
        encode <- map["encode"]
        distancia <- map["distancia"]
        distanciaGestec <- map["distanciaGestec"]
        tempo <- map["tempo"]
        raioCerca <- map["raioCerca"]
    }

}

import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class AboutScreen extends StatefulWidget {
  AboutScreen();

  @override
  _AboutScreen createState() => _AboutScreen();
}

class _AboutScreen extends State<AboutScreen> {
  _AboutScreen();

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 5,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Sobre o Via Sorocaba'),
          backgroundColor: Colors.indigo[900],
        ),
        body: Container(
          child: Center(
            child: Column(
              children: [
                _informacoesDeSorocabaVotorantim(),
                Divider(),
                _informacoesEMTUeOSM(),
                Divider(),
                InkWell(
                    onTap: _launchOSMLicenseWebsite,
                    child: Container(
                        width: MediaQuery.of(context).size.width * 0.98,
                        padding: EdgeInsets.all(10.0),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                          Container(
                              child: Image(
                                  image: AssetImage('assets/logo-osm.png')),
                              padding: EdgeInsets.all(5.0)),
                          Expanded(child: Text(
                              "Os Mapas são fornecidos pelos © contribuidores do OpenStreetMap.\nO OpenStreetMap® é disponibilizado em dados abertos, sob a licença Open Data Commons Open Database License (ODbL) pela Fundação OpenStreetMap (OSMF).\nVeja mais detalhes clicando aqui.",
                              textAlign: TextAlign.start)),
                        ]))),
                Divider(),
                Container(
                    padding: EdgeInsets.all(10.0),
                    child:
                        Text("Aplicativo desenvolvido por Henrique Ciraolo")),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _informacoesDeSorocabaVotorantim() {
    return IntrinsicHeight(
  child: Row(
    mainAxisSize: MainAxisSize.min,
      children: [
        InkWell(
            onTap: _launchUrbesWebsite,
            child: Container(
                padding: EdgeInsets.all(10.0),
                width: MediaQuery.of(context).size.width * 0.48,
                child: Column(
                  children: [
                    Container(
                        child:
                            Image(image: AssetImage('assets/logo-urbes.png')),
                        padding: EdgeInsets.all(5.0)),
                    Text(
                        "As informações de horários de Sorocaba são fornecidas pela URBES - Trânsito e Transporte",
                        textAlign: TextAlign.center),
                  ],
                ))),
        VerticalDivider(),
        InkWell(
            onTap: _launchCittamobiWebsite,
            child: Container(
              padding: EdgeInsets.all(10.0),
              width: MediaQuery.of(context).size.width * 0.48,
              child: Column(
                children: [
                  Container(
                      child:
                          Image(image: AssetImage('assets/logo-cittamobi.png')),
                      padding: EdgeInsets.all(5.0)),
                  Text(
                      "As informações de localização dos ônibus de Sorocaba são fornecidas pela Cittamobi",
                      textAlign: TextAlign.center),
                ],
              ),
            ))
      ],
    )
    );
  }

  Widget _informacoesEMTUeOSM() {
    return IntrinsicHeight(
  child: Row(children: [
      InkWell(
          onTap: _launchSaoJoaoWebsite,
          child: Container(
              padding: EdgeInsets.all(10.0),
              width: MediaQuery.of(context).size.width * 0.48,
              child: Column(children: [
                Container(
                    child: Image(
                        image: AssetImage('assets/logo-gruposaojoao.png')),
                    padding: EdgeInsets.all(5.0)),
                Text(
                    "As informações de horários de Votorantim são fornecidas pelo Grupo São João",
                    textAlign: TextAlign.center),
              ]))),
      VerticalDivider(),
      InkWell(
          onTap: _launchEmtuWebsite,
          child: Container(
              padding: EdgeInsets.all(10.0),
              width: MediaQuery.of(context).size.width * 0.48,
              child: Column(children: [
                Container(
                    child: Image(image: AssetImage('assets/logo-emtu.jpeg')),
                    padding: EdgeInsets.all(5.0)),
                Text(
                    "As informações de horários e localização dos ônibus das linhas metropolitanas são fornecidas pela EMTU - Empresa Metropolitana Transportes Urbanos.",
                    textAlign: TextAlign.center),
              ])))
    ]));
  }

  String _url = "";

  void _launchUrbesWebsite() {
    _url = "http://www.urbes.com.br";
    _launchURL();
  }

  void _launchSaoJoaoWebsite() {
    _url = "https://www.gruposaojoao.com.br/";
    _launchURL();
  }

  void _launchEmtuWebsite() {
    _url = "http://www.emtu.sp.gov.br";
    _launchURL();
  }

  void _launchCittamobiWebsite() {
    _url = "https://www.cittamobi.com.br/";
    _launchURL();
  }

  void _launchOSMLicenseWebsite() {
    _url = "https://www.openstreetmap.org/copyright";
    _launchURL();
  }

  void _launchURL() async => await canLaunch(_url)
      ? await launch(_url)
      : throw 'Could not launch $_url';
}

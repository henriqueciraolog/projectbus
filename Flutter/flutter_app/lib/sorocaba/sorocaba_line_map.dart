import 'dart:async';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:viasorocaba/api.dart';
import 'package:syncfusion_flutter_maps/maps.dart';

class LineSorocabaBusMap extends StatefulWidget {
  final line;

  LineSorocabaBusMap(this.line);

  @override
  _LineSorocabaBusMapState createState() => _LineSorocabaBusMapState(this.line);
}

class _LineSorocabaBusMapState extends State<LineSorocabaBusMap> {
  var repository = Api.instance;

  final line;

  late MapTileLayerController _controller;

  late List buses;

  List<Image> busesImg = [];

  List<List<MapLatLng>> itinerarios = [];

  _LineSorocabaBusMapState(this.line);

  @override
  void initState() {
    super.initState();
    _controller = MapTileLayerController();
    getItinerariosSorocabaLines();
    loadSorocabaLines();
    startTimer();
  }

  void loadSorocabaLines() async {
    int linha = line["codigo"];

    repository.getMapsSorocabaVeiculosLinhaByLinha(linha, (result) {
      buses = result["gps"];
      List services = result["services"];
      _controller.clearMarkers();
      for (var i = 0; i < buses.length; i++) {
        _controller.insertMarker(i);
      }

      if (buses.isEmpty && services.isNotEmpty) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text("Não há ônibus para serem mostrados no momento."),
        ));
      } else if (buses.isEmpty && services.isEmpty) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(
              "Essa linha não possui serviço de Tempo Real fornecido pela empresa operadora."),
        ));
      }
    }, (error) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(
            "Estamos enfrentando dificuldades para obter as informações para o mapa. Tente novamente mais tarde.\nMotivo: {$error}"),
      ));
    });
  }

  void getItinerariosSorocabaLines() async {
    int linha = line["codigo"];

    repository.getMapsSorocabaItinerariosLinhaByLinha(linha, (result) {
      List polylinesEncodeds = result;

      polylinesEncodeds.forEach((iti) {
        List<MapLatLng> latLngs = _decodeEncodedPolyline(iti["polyline"]);

        itinerarios.add(latLngs);
      });
    }, (error) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(
            "Estamos enfrentando dificuldades para carregar os itinerários no mapa. Tente novamente mais tarde.\nMotivo: {$error}"),
      ));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SfMaps(
      layers: [
        MapTileLayer(
          zoomPanBehavior: MapZoomPanBehavior(
              focalLatLng: MapLatLng(-23.499747, -47.452970),
              zoomLevel: 12,
              maxZoomLevel: 18,
              enableDoubleTapZooming: true),
          urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
          initialMarkersCount: 0,
          markerBuilder: (BuildContext context, int index) {
            return MapMarker(
              size: Size(44, 44),
              latitude: buses[index]["lat"],
              longitude: buses[index]["lng"],
              child: _createMarker(
                  buses[index]["service"]["codigoRota"],
                  buses[index]["service"]["direcao"] == "ARRIVAL",
                  buses[index]["prefix"],
                  buses[index]["service"]["codigoEmpresa"]),
            );
          },
          markerTooltipBuilder: (BuildContext context, int index) {
            return _createMarkerTooltip(buses[index]);
          },
          sublayers: [
            MapPolylineLayer(
              polylines: List<MapPolyline>.generate(
                itinerarios.length,
                (int index) {
                  return MapPolyline(
                    points: itinerarios[index],
                  );
                },
              ).toSet(),
              color: Colors.black,
            )
          ],
          controller: _controller,
        )
      ],
    ));
  }

  Widget _createMarker(
      String routeCode, bool isArrival, String prefix, String companyId) {
    var bgColor;

    if (companyId == "37") {
      bgColor = Colors.green[900];
    } else if (companyId == "655771") {
      bgColor = Colors.blue[800];
    } else {
      bgColor = Colors.amber[900];
    }

    return Column(children: [
      Text(
        prefix,
        style: TextStyle(
            fontSize: 8,
            fontWeight: FontWeight.w800,
            backgroundColor: bgColor,
            color: Colors.white),
      ),
      isArrival
          ? Icon(Icons.directions_bus_outlined, color: bgColor)
          : Icon(Icons.directions_bus_filled, color: bgColor)
    ]);
  }

  Widget _createMarkerTooltip(bus) {
    Map service = bus["service"];

    var bgColor;
    String companyId = service["codigoEmpresa"];
    String company = "";

    if (companyId == "37") {
      bgColor = Colors.green[900];
      company = "Consórcio Sorocaba";
    } else if (companyId == "655771") {
      bgColor = Colors.blue[800];
      company = "BRT Sorocaba";
    } else {
      bgColor = Colors.amber[900];
      company = "City Sorocaba";
    }

    String direction = service["direcao"] == "ARRIVAL" ? "IDA" : "VOLTA";
    return Container(
      color: bgColor,
      width: 300,
      padding: const EdgeInsets.all(10),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Stack(
            children: [
              Center(
                child: Text(
                  "Linha ${service["codigoRota"]} - ${service["nomeRota"]} - $direction",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize:
                          Theme.of(context).textTheme.bodyText2!.fontSize),
                ),
              ),
              const Icon(
                Icons.bus_alert,
                color: Colors.white,
                size: 16,
              ),
            ],
          ),
          const Divider(
            color: Colors.white,
            height: 10,
            thickness: 1.2,
          ),
          Text(
            "Atendimento: ${service["nomeServico"]}",
            style: TextStyle(
                color: Colors.white,
                fontSize: Theme.of(context).textTheme.bodyText2!.fontSize),
          ),
          Text(
            "Placa: ${bus["plate"]}\nOperado por $company",
            style: TextStyle(
                color: Colors.white,
                fontSize: Theme.of(context).textTheme.bodyText2!.fontSize),
          ),
        ],
      ),
    );
  }

  late var _timer;

  void startTimer() {
    const oneSec = const Duration(seconds: 15);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) => setState(
        () {
          loadSorocabaLines();
        },
      ),
    );
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  List<MapLatLng> _decodeEncodedPolyline(String encoded) {
    List<MapLatLng> poly = [];
    int index = 0, len = encoded.length;
    int lat = 0, lng = 0;

    while (index < len) {
      int b, shift = 0, result = 0;
      do {
        b = encoded.codeUnitAt(index++) - 63;
        result |= (b & 0x1f) << shift;
        shift += 5;
      } while (b >= 0x20);
      int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
      lat += dlat;

      shift = 0;
      result = 0;
      do {
        b = encoded.codeUnitAt(index++) - 63;
        result |= (b & 0x1f) << shift;
        shift += 5;
      } while (b >= 0x20);
      int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
      lng += dlng;
      MapLatLng p =
          new MapLatLng((lat / 1E5).toDouble(), (lng / 1E5).toDouble());
      poly.add(p);
    }
    return poly;
  }
}

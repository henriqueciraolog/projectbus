import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'news_entity.dart';

class NewsWidget extends StatefulWidget {
  final NewsEntity newsEntity;

  NewsWidget(this.newsEntity);

  @override
  State<StatefulWidget> createState() => _NewsWidget(this.newsEntity);
}

class _NewsWidget extends State<NewsWidget>{
  final NewsEntity newsEntity;

  _NewsWidget(this.newsEntity);

  @override
  Widget build(BuildContext context) {
    return new GestureDetector(
        onTap: () {},
        child: new Container(
          margin: const EdgeInsets.only(
              left: 10.0, right: 10.0, bottom: 10.0, top: 0.0),
          child: new Material(
            borderRadius: new BorderRadius.circular(6.0),
            elevation: 2.0,
            child: getLineOccurrence(),
          ),
        ));
  }

  Widget getLineOccurrence() {
    return new Container(
      margin: new EdgeInsets.all(10.0),
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          new Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(readTimestamp(newsEntity.ts!),
                  style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 18))
            ],
          ),
          new Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[Flexible(child: Text(newsEntity.text!, style: new TextStyle(fontSize: 18)))],
          ),
        ],
      ),
    );
  }

  String readTimestamp(int timestamp) {
    var format = DateFormat('dd/MM/yyyy HH:mm');
    var date = DateTime.fromMillisecondsSinceEpoch(timestamp);

    return format.format(date);
  }
}
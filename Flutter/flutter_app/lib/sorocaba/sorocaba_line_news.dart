import 'package:flutter/material.dart';
import 'package:viasorocaba/commons/empty_state_widget.dart';
import 'package:viasorocaba/sorocaba/news_widget.dart';
import 'news_entity.dart';

class SorocabaLineNews extends StatefulWidget {
  final List news;
  SorocabaLineNews(this.news);

  @override
  _SorocabaLineNewsState createState() => _SorocabaLineNewsState(news);
}

class _SorocabaLineNewsState extends State<SorocabaLineNews> {
  List news;

  List lineOccurrences = [];
  List<NewsEntity> allMessages = [];

  _SorocabaLineNewsState(this.news);

  @override
  Widget build(BuildContext context) {
    List<Widget> widgets;

    if (news.isNotEmpty) {
      widgets = [_getListOfOccurrences()];
    } else {
      widgets = [
        EmptyStateWidget("Essa linha não possui novidades no momento.")
      ];
    }

    return Container(child: Column(children: widgets));
  }

  @override
  void initState() {
    super.initState();

    loadSorocabaLines();
  }

  Widget _getListOfOccurrences() {
    var list = new ListView.builder(
        itemCount: allMessages.length,
        padding: new EdgeInsets.only(top: 5.0),
        itemBuilder: (context, index) {
          return NewsWidget(allMessages[index]);
        });

    return new Expanded(child: list);
  }

  void loadSorocabaLines() async {
    news.forEach((msg) {
      allMessages.add(NewsEntity.fromJson(msg));
    });

    setState(() {
      allMessages.sort((a, b) => b.ts!.compareTo(a.ts!));
    });
  }
}

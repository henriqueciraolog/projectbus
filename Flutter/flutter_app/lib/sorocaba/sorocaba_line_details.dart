import 'package:flutter/material.dart';
import 'package:viasorocaba/sorocaba/sorocaba_line_news.dart';

import 'sorocaba_departures.dart';
import 'sorocaba_line_map.dart';

class SorocabaLineDetails extends StatefulWidget {
  final Map line;

  SorocabaLineDetails(this.line);

  @override
  _SorocabaLineDetails createState() => _SorocabaLineDetails(line);
}

class _SorocabaLineDetails extends State<SorocabaLineDetails> {
  Map line;

  Widget base = Container();

  _SorocabaLineDetails(this.line);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 5,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            isScrollable: true,
            tabs: [
              Tab(text: "Novidades"),
              Tab(text: "Dias\núteis"),
              Tab(text: "Sabados"),
              Tab(text: "Domingos\nFeriados"),
              Tab(text: "Tempo\nReal"),
            ],
          ),
          title: Text('${line["titulo"]} - ${line["descricao"]}'),
          backgroundColor: Color(0xFF01215E),
        ),
        body: base,
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    loadMetropolitanSorocabaLineDetails(line);
  }

  loadMetropolitanSorocabaLineDetails(line) {
    setState(() {
      base = TabBarView(
          physics: NeverScrollableScrollPhysics(),
          children: <Widget>[
            SorocabaLineNews(line["mensagens"]),
            SorocabaDepartures(line, "diasUteis", line["horariosDiasUteis"]),
            SorocabaDepartures(line, "sabado", line["horariosSabados"]),
            SorocabaDepartures(line, "domingoFeriado", line["horariosDomFer"]),
            LineSorocabaBusMap(line),
          ]);
    });
  }
}

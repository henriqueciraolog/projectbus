import 'package:flutter/material.dart';
import 'package:viasorocaba/api.dart';
import 'package:syncfusion_flutter_maps/maps.dart';

class NewSorocabaBusMap extends StatefulWidget {
  @override
  _NewSorocabaBusMapState createState() => _NewSorocabaBusMapState();
}

class _NewSorocabaBusMapState extends State<NewSorocabaBusMap> {
  bool _showBottomSheet = true;
  double _bottomSheetHeight = 50;
  double _listHeight = 0;

  late MapTileLayerController _controller;

  var _repository = Api.instance;

  List _buses = [];

  List _busesToFilter = [];

  bool _isDisposed = false;

  int linesInAsync = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SfMaps(layers: [_mapLayer()]),
      floatingActionButton: FloatingActionButton(
        backgroundColor: const Color(0xff03dac6),
        foregroundColor: Colors.black,
        onPressed: () {
          if (linesInAsync == 0) {
            _getSorocabaLines();
          } else {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text("Você não pode atualizar tão rapidamente..."),
            ));
          }
        },
        child: Icon(Icons.refresh_sharp),
      ),
      bottomSheet: _showBottomSheet
          ? BottomSheet(
              elevation: 10,
              backgroundColor: Colors.amber,
              onClosing: () {
                // Do something
              },
              builder: (BuildContext ctx) => _buildBottomSheetVisual())
          : null,
    );
  }

  Container _buildBottomSheetVisual() {
    return Container(
      width: double.infinity,
      height: _bottomSheetHeight,
      padding: EdgeInsets.only(left: 8, right: 8),
      color: Colors.white,
      child: Center(
        child: Column(
          children: [_buildSearchBar(), _buildBusList()],
        ),
      ),
    );
  }

  Widget _buildSearchBar() {
    return TextField(
      style: TextStyle(fontSize: 12),
      maxLength: 4,
      decoration: InputDecoration(
        prefixIcon: Icon(Icons.search),
        isDense: true,
        contentPadding:
            EdgeInsets.only(left: 12, right: 12, top: 12, bottom: 12),
        border: OutlineInputBorder(),
        hintText: 'Digite o prefixo a buscar',
        counterText: "",
      ),
      onChanged: (text) {
        if (text.length >= 2) {
          List busesFilter = [];
          _buses.forEach((element) {
            String prefix = element["prefix"];
            if (prefix.contains(text)) {
              busesFilter.add(element);
            }
          });

          setState(() {
            _bottomSheetHeight = 450;
            _listHeight = 380;
            _busesToFilter.clear();
            _busesToFilter.addAll(busesFilter);
          });
        } else {
          setState(() {
            _bottomSheetHeight = 50;
            _listHeight = 0;
            _busesToFilter.clear();
          });
        }
      },
    );
  }

  Widget _buildBusList() {
    return Container(
      height: _listHeight,
      color: Colors.white,
      child: Center(
        child: new ListView.builder(
            itemCount: _busesToFilter.length,
            padding: new EdgeInsets.only(top: 5.0),
            itemBuilder: (context, index) {
              return _buildBus(_busesToFilter[index]);
            }),
      ),
    );
  }

  Widget _buildBus(var bus) {
    Map b = bus;
    Map s = bus["service"];

    String direction = s["direcao"] == "ARRIVAL" ? "IDA" : "VOLTA";

    String companyId = s["codigoEmpresa"];
    String company = "";

    if (companyId == "37") {
      company = "Consórcio Sorocaba";
    } else if (companyId == "655771") {
      company = "BRT Sorocaba";
    } else {
      company = "City Sorocaba";
    }

    return Container(
        child: Column(
      children: [
        Text("Prefixo: ${b["prefix"]}"),
        Text("Linha ${s["codigoRota"]} - ${s["nomeRota"]} - $direction"),
        Text("Atendimento: ${s["nomeServico"]}"),
        Text("Placa: ${b["plate"]}"),
        Text("Empresa Operadora: $company"),
        Text(
            "-----------------------------------------------------------------------"),
      ],
    ));
  }

  MapTileLayer _mapLayer() {
    return MapTileLayer(
      zoomPanBehavior: MapZoomPanBehavior(
          focalLatLng: MapLatLng(-23.499747, -47.452970),
          zoomLevel: 12,
          maxZoomLevel: 18,
          enableDoubleTapZooming: true),
      urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
      initialMarkersCount: 0,
      markerBuilder: (BuildContext context, int index) {
        return MapMarker(
          size: Size(50, 50),
          latitude: _buses[index]["lat"],
          longitude: _buses[index]["lng"],
          child: _createMarker(
              _buses[index]["service"]["codigoRota"],
              _buses[index]["service"]["direcao"] == "ARRIVAL",
              _buses[index]["prefix"],
              _buses[index]["service"]["codigoEmpresa"]),
        );
      },
      markerTooltipBuilder: (BuildContext context, int index) {
        return _createMarkerTooltip(_buses[index]);
      },
      controller: _controller,
    );
  }

  Widget _createMarker(
      String routeCode, bool isArrival, String prefix, String companyId) {
    var bgColor;

    if (companyId == "37") {
      bgColor = Colors.green[900];
    } else if (companyId == "655771") {
      bgColor = Colors.blue[800];
    } else {
      bgColor = Colors.amber[900];
    }

    return Column(children: [
      Container(
          padding: EdgeInsets.all(1),
          color: bgColor,
          child: Text(routeCode,
              style: TextStyle(
                  fontSize: 9,
                  fontWeight: FontWeight.w600,
                  color: Colors.white))),
      Container(
        padding: EdgeInsets.all(1),
        color: bgColor,
        child: Text(prefix,
            style: TextStyle(
                fontSize: 8, fontWeight: FontWeight.w900, color: Colors.white)),
      ),
      isArrival
          ? Icon(Icons.directions_bus_outlined, color: bgColor)
          : Icon(Icons.directions_bus_filled, color: bgColor)
    ]);
  }

  @override
  void initState() {
    super.initState();
    _controller = MapTileLayerController();

    _getSorocabaLines();
  }

  void _getSorocabaLines() async {
    _repository.getSorocabaLinhas((result) {
      List lines = result!;

      linesInAsync = lines.length;

      for (var i = 0; i < lines.length; i++) {
        if (!_isDisposed) {
          _getBusFromSorocabaLine(lines[i]);
        } else {
          break;
        }
      }
    }, (error) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(
            "Estamos enfrentando dificuldades para obter as informações para o mapa. Tente novamente mais tarde.\nMotivo: {$error}"),
      ));
    });
  }

  void _getBusFromSorocabaLine(Map line) async {
    int linha = line["codigo"];
    _repository.getMapsSorocabaVeiculosLinhaByLinha(linha, (result) {
      if (!_isDisposed) {
        List gpss = result["gps"];

        gpss.forEach((gps) {
          gps["linhaControle"] = line["titulo"];
          _buses.removeWhere((bus) => bus["prefix"] == gps["prefix"]);
        });

        _buses.removeWhere(
            (element) => element["linhaControle"] == line["titulo"]);
        _buses.addAll(gpss);

        _controller.clearMarkers();
        for (var i = 0; i < _buses.length; i++) {
          _controller.insertMarker(i);
        }

        linesInAsync--;
      }
    }, (error) {
      linesInAsync--;
      print("Erro ao obter dados da linha $linha. Motivo: $error");
    });
  }

  @override
  void dispose() {
    _isDisposed = true;
    super.dispose();
  }

  Widget _createMarkerTooltip(bus) {
    Map service = bus["service"];

    String companyId = service["codigoEmpresa"];
    String company = "";

    if (companyId == "37") {
      company = "Consórcio Sorocaba";
    } else if (companyId == "655771") {
      company = "BRT Sorocaba";
    } else {
      company = "City Sorocaba";
    }

    String direction = "";

    if (service.containsKey("direcao")) {
      direction = service["direcao"] == "ARRIVAL" ? "IDA" : "VOLTA";
    } else {
      direction = "SEM DIREÇÃO";
      print(bus);
    }

    return Container(
      color: Color(0xFF01215E),
      padding: const EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            "Linha ${service["codigoRota"]} - ${service["nomeRota"]} - $direction",
            style: TextStyle(
                fontWeight: FontWeight.w500,
                color: Colors.white,
                fontSize: Theme.of(context).textTheme.bodyText2!.fontSize),
            textAlign: TextAlign.center,
          ),
          Text(
            "Atendimento: ${service["nomeServico"]}",
            style: TextStyle(
                color: Colors.white,
                fontSize: Theme.of(context).textTheme.bodyText2!.fontSize),
          ),
          Text(
            "Placa: ${bus["plate"]}\nOperado por $company",
            style: TextStyle(
                color: Colors.white,
                fontSize: Theme.of(context).textTheme.bodyText2!.fontSize),
          ),
        ],
      ),
    );
  }
}

class NewsEntity {
  String? text;
  int? ts;

  NewsEntity({this.text, this.ts});

  NewsEntity.fromJson(Map<String, dynamic> json) {
    text = json['mensagem'];
    ts = json['codigo'];
  }
}

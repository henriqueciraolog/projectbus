import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:viasorocaba/commons/line_widget.dart';
import 'package:viasorocaba/commons/try_again_widget.dart';

import '../api.dart';
import 'sorocaba_line_details.dart';

class SorocabaLineList extends StatefulWidget {
  @override
  _SorocabaLineListPageState createState() => _SorocabaLineListPageState();
}

class _SorocabaLineListPageState extends State<SorocabaLineList> {
  List sorocabaLines = [];
  List database = [];
  Api repository = Api.instance;

  Widget bodyReplace = Container();

  var result;

  @override
  Widget build(BuildContext context) {
    return bodyReplace;
  }

  void _textControllerChanged(String text) {
    if (text.isEmpty) {
      database.clear();
      database.addAll(sorocabaLines);
    } else {
      database.clear();

      for (int i = 0; i < sorocabaLines.length; i++) {
        String codigo = sorocabaLines[i]["titulo"];
        String titulo = sorocabaLines[i]["descricao"];

        if (codigo.contains(text.toUpperCase()) ||
            titulo.contains(text.toUpperCase())) {
          database.add(sorocabaLines[i]);
        }
      }
    }

    setState(() {
      bodyReplace = _createStackWithSearchAndList();
    });
  }

  Widget _createStackWithSearchAndList() {
    return Container(
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
            child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [_createSearchField(), _getListViewWidget()])));
  }

  Widget _createSearchField() {
    return Container(
        padding: EdgeInsets.all(10.0),
        child: CupertinoSearchTextField(
          placeholder: "Busque pelo número ou nome da linha",
          onChanged: (String value) {
            _textControllerChanged(value);
            print('The text has changed to: $value');
          },
        ));
  }

  @override
  void initState() {
    super.initState();

    _loadSorocabaLines();
  }

  _loadSorocabaLines() async {
    context.loaderOverlay.show();

    repository.getSorocabaLinhas((result) {
      List? lines = result;

      sorocabaLines.addAll(lines!);
      database.addAll(sorocabaLines);

      setState(() {
        bodyReplace = _createStackWithSearchAndList();
      });

      context.loaderOverlay.hide();
    }, (error) {
      setState(() {
        bodyReplace = TryAgainWidget(
            "Não foi possível obter as linhas municipais de Sorocaba no momento.\nMotivo:$error",
            () {
          _loadSorocabaLines();
        });
      });
      context.loaderOverlay.hide();
    });
  }

  Widget _getListViewWidget() {
    return new ListView.builder(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: database.length,
        padding: new EdgeInsets.only(top: 10.0),
        itemBuilder: (context, index) {
          return buildLine(database[index]);
        });
  }

  Widget buildLine(line) {
    return new GestureDetector(
        onTap: () {
          onTapLine(line);
        },
        child: Container(
            key: Key(line["titulo"]),
            child: LineWidget(TypeLine.VIA, line["titulo"], line["descricao"],
                line["terminal"], Color(0xFF37b349), Colors.white)));
  }

  void onTapLine(line) {
    _loadHorarios(line);
  }

  _loadHorarios(line) async {
    context.loaderOverlay.show();

    int linhaId = line["codigo"];
    repository.getSorocabaLinhasByLinhaId(linhaId, (result) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => SorocabaLineDetails(result)),
      );
      context.loaderOverlay.hide();
    }, (error) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(
            "Estamos enfrentando dificuldades para obter as informações dessa linha no momento. Tente novamente mais tarde.\nMotivo: {$error}"),
      ));
      context.loaderOverlay.hide();
    });
  }
}

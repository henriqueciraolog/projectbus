import 'package:flutter/material.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:viasorocaba/commons/empty_state_widget.dart';
import 'package:viasorocaba/commons/try_again_widget.dart';
import 'package:viasorocaba/sorocaba/news_widget.dart';
import '../api.dart';
import 'news_entity.dart';

class News extends StatefulWidget {
  News();

  @override
  _News createState() => _News();
}

class _News extends State<News> {
  var repository = Api.instance;

  List lineOccurrences = [];
  List<NewsEntity> allMessages = [];

  Widget bodyReplace = Container();

  _News();

  @override
  Widget build(BuildContext context) {
    return bodyReplace;
  }

  @override
  void initState() {
    super.initState();

    _loadSorocabaLines();
  }

  Widget _createListOfNews() {
    return Container(child: Column(children: [_getListOfOccurrences()]));
  }

  Widget _getListOfOccurrences() {
    var list = new ListView.builder(
        itemCount: allMessages.length,
        padding: new EdgeInsets.only(top: 5.0),
        itemBuilder: (context, index) {
          return NewsWidget(allMessages[index]);
        });

    return new Expanded(child: list);
  }

  void _loadSorocabaLines() async {
    context.loaderOverlay.show();

    repository.getSorocabaNoticias((result) {
      List messages = result;

      messages.forEach((msg) {
        allMessages.add(NewsEntity.fromJson(msg));
      });

      allMessages.sort((a, b) => b.ts!.compareTo(a.ts!));

      setState(() {
        if (allMessages.isNotEmpty) {
          bodyReplace = _createListOfNews();
        } else {
          bodyReplace = EmptyStateWidget(
              "Não temos novidades nas linhas municipais de Sorocaba no momento.");
        }
      });

      context.loaderOverlay.hide();
    }, (error) {
      setState(() {
        bodyReplace = TryAgainWidget(
            "Não foi possível obter as novidades das linhas municipais de Sorocaba no momento.\nMotivo:$error",
            () {
          _loadSorocabaLines();
        });
      });

      context.loaderOverlay.hide();
    });
  }
}

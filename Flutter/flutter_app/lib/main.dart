import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:loader_overlay/loader_overlay.dart';

import 'first_loading.dart';

void main() {
  runApp(ViaSorocabaApp());
}

class ViaSorocabaApp extends StatefulWidget {
  final state = new _ViaSorocabaPageState();

  @override
  _ViaSorocabaPageState createState() => state;
}

class _ViaSorocabaPageState extends State<ViaSorocabaApp> {
  @override
  Widget build(BuildContext context) {
    return GlobalLoaderOverlay(
      child: MaterialApp(
        title: 'Via Sorocaba',
        theme: ThemeData(
          primarySwatch: Colors.indigo,
        ),
        home: LoaderOverlay(
          useDefaultLoading: false,
          overlayWidget: Center(
            child: SpinKitFadingCircle(color: Colors.indigo[900]),
          ),
          overlayOpacity: 0.8,
          child: InitialLoading(),
        ),
      ),
    );
  }
}

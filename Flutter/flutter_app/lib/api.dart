import 'dart:async';
import 'package:http/http.dart' as http;

import 'result_api.dart';

typedef RequestApiSuccessCallback = void Function(dynamic result);
typedef RequestApiErrorCallback = void Function(String error);

class Api {
  static const String URL_PROJECT_BUS =
      "https://via-sorocaba.azurewebsites.net/projectbus/";
  static const String URL_LOCALHOST = "http://localhost:8080/";
  static const String URL_LOCALHOST_ANDROID = "http://10.0.2.2:8080/";
  static const String URL = URL_PROJECT_BUS;

  static const bool LOGGING = false;

  Api._();

  static Api? _instance;

  static Api get instance {
    return _instance ??= Api._();
  }

  List? loadMetropolitanLinesCache;
  List? loadSorocabaLinesCache;
  List? loadVotorantimLinesCache;

  Future<ResultApi<dynamic>> _loadUrl(String url, Map<String, String> headers,
      {bool newParse = true}) async {
    try {
      http.Response response = await http.get(Uri.parse(url), headers: headers);
      return ResultApi.fromResponse(response, newParse);
    } on Exception catch (e) {
      print(e);
      return ResultApi.fromException(e);
    }
  }

  void getEmtuLinhas(RequestApiSuccessCallback onSuccess,
      RequestApiErrorCallback onError) async {
    if (loadMetropolitanLinesCache == null) {
      String url = "${URL}emtu/linhas";

      ResultApi result = await _loadUrl(url,
          {"Accept": "application/json", "Access-Control-Allow-Origin": "*"});

      if (result.isSuccessful()) {
        loadMetropolitanLinesCache = result.getResult();
        onSuccess(result.getResult());
      } else {
        onError(result.getError());
      }
    } else {
      Future.delayed(const Duration(milliseconds: 1000), () {
        onSuccess(loadMetropolitanLinesCache);
      });
    }
  }

  void getMapsEmtuByLinha(String linha, RequestApiSuccessCallback onSuccess,
      RequestApiErrorCallback onError) async {
    String url = "${URL}maps/emtu/$linha";

    ResultApi result = await _loadUrl(url,
        {"Accept": "application/json", "Access-Control-Allow-Origin": "*"});

    if (result.isSuccessful()) {
      onSuccess(result.getResult());
    } else {
      onError(result.getError());
    }
  }

  void getEmtuOcorrenciasByLinha(
      String linha,
      RequestApiSuccessCallback onSuccess,
      RequestApiErrorCallback onError) async {
    String url = "${URL}emtu/ocorrencias?route=$linha";

    ResultApi result = await _loadUrl(url,
        {"Accept": "application/json", "Access-Control-Allow-Origin": "*"});

    if (result.isSuccessful()) {
      onSuccess(result.getResult());
    } else {
      onError(result.getError());
    }
  }

  void getEmtuOcorrencias(RequestApiSuccessCallback onSuccess,
      RequestApiErrorCallback onError) async {
    String url = "${URL}emtu/ocorrencias";

    ResultApi result = await _loadUrl(url,
        {"Accept": "application/json", "Access-Control-Allow-Origin": "*"});

    if (result.isSuccessful()) {
      onSuccess(result.getResult());
    } else {
      onError(result.getError());
    }
  }

  void getSorocabaLinhas(RequestApiSuccessCallback onSuccess,
      RequestApiErrorCallback onError) async {
    if (loadSorocabaLinesCache == null) {
      String url = "${URL}sorocaba/linhas/v2";

      ResultApi result = await _loadUrl(url,
          {"Accept": "application/json", "Access-Control-Allow-Origin": "*"});

      if (result.isSuccessful()) {
        loadSorocabaLinesCache = result.getResult();
        onSuccess(result.getResult());
      } else {
        onError(result.getError());
      }
    } else {
      Future.delayed(const Duration(milliseconds: 1000), () {
        onSuccess(loadSorocabaLinesCache);
      });
    }
  }

  void getSorocabaLinhasByLinhaId(
      int linhaId,
      RequestApiSuccessCallback onSuccess,
      RequestApiErrorCallback onError) async {
    String url = "${URL}sorocaba/linhas/v3/$linhaId";

    ResultApi result = await _loadUrl(url,
        {"Accept": "application/json", "Access-Control-Allow-Origin": "*"});

    if (result.isSuccessful()) {
      onSuccess(result.getResult());
    } else {
      onError(result.getError());
    }
  }

  void getMapsSorocabaVeiculosLinhaByLinha(
      int linha,
      RequestApiSuccessCallback onSuccess,
      RequestApiErrorCallback onError) async {
    String url = "${URL}maps/sorocaba/v2/veiculos/linha/$linha";

    ResultApi result = await _loadUrl(url,
        {"Accept": "application/json", "Access-Control-Allow-Origin": "*"});

    if (result.isSuccessful()) {
      onSuccess(result.getResult());
    } else {
      onError(result.getError());
    }
  }

  void getMapsSorocabaItinerariosLinhaByLinha(
      int linha,
      RequestApiSuccessCallback onSuccess,
      RequestApiErrorCallback onError) async {
    String url = "${URL}maps/sorocaba/v2/itinerarios/linha/$linha";

    ResultApi result = await _loadUrl(url,
        {"Accept": "application/json", "Access-Control-Allow-Origin": "*"});

    if (result.isSuccessful()) {
      onSuccess(result.getResult());
    } else {
      onError(result.getError());
    }
  }

  void getSorocabaNoticias(RequestApiSuccessCallback onSuccess,
      RequestApiErrorCallback onError) async {
    String url = "${URL}sorocaba/noticias";

    ResultApi result = await _loadUrl(url,
        {"Accept": "application/json", "Access-Control-Allow-Origin": "*"});

    if (result.isSuccessful()) {
      onSuccess(result.getResult());
    } else {
      onError(result.getError());
    }
  }

  void getVotorantimLinhas(RequestApiSuccessCallback onSuccess,
      RequestApiErrorCallback onError) async {
    if (loadVotorantimLinesCache == null) {
      String url = "${URL}votorantim/linhas";

      ResultApi result = await _loadUrl(url,
          {"Accept": "application/json", "Access-Control-Allow-Origin": "*"});

      if (result.isSuccessful()) {
        onSuccess(result.getResult());
      } else {
        onError(result.getError());
      }
    } else {
      Future.delayed(const Duration(milliseconds: 1000), () {
        onSuccess(loadVotorantimLinesCache);
      });
    }
  }
}

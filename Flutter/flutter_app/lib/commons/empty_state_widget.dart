import 'package:flutter/material.dart';

class EmptyStateWidget extends StatefulWidget {
  final String message;

  EmptyStateWidget(this.message);

  @override
  State<StatefulWidget> createState() => _EmptyStateWidget(message);
}

class _EmptyStateWidget extends State<EmptyStateWidget> {
  final String message;

  _EmptyStateWidget(this.message);

  @override
  Widget build(BuildContext context) {
    return _buildEmptyView();
  }

  Widget _buildEmptyView() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * 0.8,
      child: Center(
        child: Text(
          message,
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 20,
              color: Color(0xFF01215E)),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';

class LineBlockWidget extends StatefulWidget {
  final String lineCode;

  LineBlockWidget(this.lineCode);

  @override
  State<StatefulWidget> createState() => _LineBlockWidget(this.lineCode);
}

class _LineBlockWidget extends State<LineBlockWidget> {
  final String lineCode;

  _LineBlockWidget(this.lineCode);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Color(0xFFba251e),
      borderRadius: BorderRadius.circular(6.0),
      child: Container(
          margin: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
          child: FittedBox(
              fit: BoxFit.fitWidth,
              child: Text(
                lineCode,
                style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w900,
                    color: Colors.white),
              ))),
    );
  }
}
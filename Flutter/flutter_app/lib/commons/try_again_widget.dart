import 'package:flutter/material.dart';

typedef TryAgainCallback = void Function();

class TryAgainWidget extends StatefulWidget {
  final String message;
  final TryAgainCallback callback;

  TryAgainWidget(this.message, this.callback);

  @override
  State<StatefulWidget> createState() => _TryAgainWidget(message, callback);
}

class _TryAgainWidget extends State<TryAgainWidget> {
  final String message;
  final TryAgainCallback callback;

  _TryAgainWidget(this.message, this.callback);

  @override
  Widget build(BuildContext context) {
    return _buildTryAgainView();
  }

  Widget _buildTryAgainView() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * 0.8,
      child: Center(
        child: Column(
          children: [
            Text(message,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                    color: Color(0xFF01215E)),
                textAlign: TextAlign.center),
            TextButton(
                onPressed: () {
                  callback();
                },
                child: Text("Tentar novamente"))
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';

class LineWidget extends StatefulWidget {
  final TypeLine type;
  final String code;
  final String source;
  final String destination;
  final Color backgroundColor;
  final Color textColor;

  LineWidget(this.type, this.code, this.source, this.destination,
      this.backgroundColor, this.textColor);

  @override
  State<StatefulWidget> createState() =>
      _LineWidget(type, code, source, destination, backgroundColor, textColor);
}

class _LineWidget extends State<LineWidget> {
  TypeLine type;
  String code;
  String source;
  String destination;
  Color backgroundColor;
  Color textColor;

  _LineWidget(this.type, this.code, this.source, this.destination,
      this.backgroundColor, this.textColor);

  @override
  Widget build(BuildContext context) {
    return _buildLineInfo();
  }

  Widget _buildLineInfo() {
    return Container(
      margin:
          const EdgeInsets.only(left: 10.0, right: 10.0, bottom: 0.0, top: 0.0),
      child: Column(
        children: [
          Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              _getLineNumber(),
              Container(width: 10),
              Expanded(child: _getLineName()),
            ],
          ),
          Divider(thickness: 2)
        ],
      ),
    );
  }

  Widget _getLineNumber() {
    return Material(
      color: backgroundColor,
      borderRadius: BorderRadius.circular(6.0),
      child: Container(
          margin: EdgeInsets.all(10.0),
          width: 50,
          height: 50,
          child: FittedBox(
              fit: BoxFit.fitWidth,
              child: Text(
                this.code,
                style: TextStyle(
                    fontSize: 40,
                    fontWeight: FontWeight.w900,
                    color: textColor),
              ))),
    );
  }

  Widget _getLineName() {
    List<Widget> lineTexts = [];

    if (type == TypeLine.VIA) {
      if (source.isNotEmpty) lineTexts.add(_getLineText(source));
      if (destination.isNotEmpty) lineTexts.add(_getTerminalText(destination));
    } else if (type == TypeLine.DESTINATION) {
      if (source.isNotEmpty) lineTexts.add(_getLineText2(source));
      if (destination.isNotEmpty) lineTexts.add(_getLineText2(destination));
    }

    return Material(
      borderRadius: BorderRadius.circular(6.0),
      elevation: 0.0,
      child: Container(
        margin: EdgeInsets.all(10.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: lineTexts,
        ),
      ),
    );
  }

  Widget _getLineText(String lineText) {
    return FittedBox(
        fit: BoxFit.fitWidth,
        child: Text(
          lineText,
          style: TextStyle(
              fontSize: 26, fontWeight: FontWeight.w800, color: Colors.black),
          maxLines: lineText.length < 30 ? 1 : 2,
        ));
  }

  Widget _getLineText2(String lineText) {
    return FittedBox(
        fit: BoxFit.fitWidth,
        child: Text(
          lineText,
          style: TextStyle(
              fontSize: 21, fontWeight: FontWeight.w800, color: Colors.black),
          maxLines: lineText.length < 30 ? 1 : 2,
        ));
  }

  Widget _getTerminalText(String terminalText) {
    return FittedBox(
        fit: BoxFit.fitWidth,
        child: Text(
          terminalText,
          style: TextStyle(
              fontSize: 16, fontWeight: FontWeight.w900, color: Colors.black),
          maxLines: terminalText.length < 30 ? 1 : 2,
        ));
  }
}

enum TypeLine { VIA, DESTINATION }

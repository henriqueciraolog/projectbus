import 'package:flutter/material.dart';
import 'package:viasorocaba/about.dart';
import 'package:viasorocaba/emtu/metropolitan_linelist.dart';
import 'package:viasorocaba/emtu/new_busmap.dart';
import 'package:viasorocaba/sorocaba/sorocaba_busmap.dart';
import 'package:viasorocaba/sorocaba/sorocaba_linelist.dart';
import 'package:viasorocaba/votorantim/votorantim_linelist.dart';
import 'emtu/general_occurrences.dart';
import 'sorocaba/news_sorocaba.dart';

class TelaPrincipal extends StatefulWidget {
  final state = new _TelaPrincipalState();

  @override
  State<StatefulWidget> createState() => state;
}

class _TelaPrincipalState extends State<TelaPrincipal> {
  late StatefulWidget baseSorocaba = SorocabaLineList();
  late StatefulWidget baseVotorantim = VotorantimLineList();
  late StatefulWidget baseEmtu = MetropolitanLineList();

  Widget titleText = new Text('Via Sorocaba');
  late Widget bottomNav;

  var _currentIndexSor = 0;

  var _currentIndexEmtu = 0;
  @override
  Widget build(BuildContext context) {
    bottomNav = _getSorocabaBottomNavigationBar();
    return _getTabBar();
  }

  Widget _getTabBar() {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: titleText,
          backgroundColor: Color(0xFF01215E),
          bottom: TabBar(
            isScrollable: false,
            onTap: onTabTapped,
            tabs: [
              Tab(text: "SOROCABA"),
              Tab(text: "VOTORANTIM"),
              Tab(text: "EMTU")
            ],
          ),
          actions: <Widget>[
            PopupMenuButton<String>(
              onSelected: _handleClick,
              itemBuilder: (BuildContext context) {
                return {'Sobre...'}.map((String choice) {
                  return PopupMenuItem<String>(
                    value: choice,
                    child: Text(choice),
                  );
                }).toList();
              },
            ),
          ],
        ),
        body: TabBarView(
          physics: NeverScrollableScrollPhysics(),
          children: [
            _getSorocabaCommonHome(),
            _getVotorantimCommonHome(),
            _getEmtuCommonHome()
          ],
        ),
      ),
    );
  }

  void _handleClick(String value) {
    switch (value) {
      case 'Sobre...':
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => AboutScreen()),
        );
        break;
    }
  }

  Widget _getSorocabaCommonHome() {
    return new Scaffold(
        bottomNavigationBar: _getSorocabaBottomNavigationBar(),
        body: baseSorocaba);
  }

  Widget _getVotorantimCommonHome() {
    return new Scaffold(bottomNavigationBar: null, body: baseVotorantim);
  }

  Widget _getEmtuCommonHome() {
    return new Scaffold(
        bottomNavigationBar: _getEmtuBottomNavigationBar(), body: baseEmtu);
  }

  Widget _getSorocabaBottomNavigationBar() {
    return new BottomNavigationBar(
      onTap: onSorocabaTabTapped,
      currentIndex: _currentIndexSor,
      type: BottomNavigationBarType.shifting,
      items: [
        new BottomNavigationBarItem(
            icon: const Icon(Icons.list),
            label: 'Linhas',
            backgroundColor: Color(0xFF01215E)),
        new BottomNavigationBarItem(
            icon: const Icon(Icons.map),
            label: 'Mapa',
            backgroundColor: Color(0xFF01215E)),
        new BottomNavigationBarItem(
            icon: const Icon(Icons.bookmarks_outlined),
            label: 'Novidades',
            backgroundColor: Color(0xFF01215E))
      ],
    );
  }

  Widget _getEmtuBottomNavigationBar() {
    return new BottomNavigationBar(
      onTap: onEmtuTabTapped, // new
      currentIndex: _currentIndexEmtu, // new
      type: BottomNavigationBarType.shifting,
      items: [
        new BottomNavigationBarItem(
            icon: const Icon(Icons.list),
            label: 'Linhas',
            backgroundColor: Color(0xFF01215E)),
        new BottomNavigationBarItem(
            icon: const Icon(Icons.map),
            label: 'Mapa',
            backgroundColor: Color(0xFF01215E)),
        new BottomNavigationBarItem(
            icon: const Icon(Icons.warning),
            label: 'Ocorrências',
            backgroundColor: Color(0xFF01215E))
      ],
    );
  }

  void onTabTapped(int index) {
    switch (index) {
      case 0:
        {
          switch (_currentIndexSor) {
            case 0:
              {
                setState(() {
                  bottomNav = _getSorocabaBottomNavigationBar();
                  baseSorocaba = SorocabaLineList();
                });
                break;
              }
            case 1:
              {
                setState(() {
                  bottomNav = _getSorocabaBottomNavigationBar();
                  baseSorocaba = NewSorocabaBusMap();
                });
                break;
              }
            case 2:
              {
                setState(() {
                  bottomNav = _getSorocabaBottomNavigationBar();
                  baseSorocaba = News();
                });
                break;
              }
          }

          break;
        }
      case 1:
        {
          switch (_currentIndexEmtu) {
            case 0:
              {
                setState(() {
                  bottomNav = _getEmtuBottomNavigationBar();
                  baseEmtu = MetropolitanLineList();
                });
                break;
              }
            case 1:
              {
                setState(() {
                  bottomNav = _getEmtuBottomNavigationBar();
                  baseEmtu = NewBusMap();
                });
                break;
              }
            case 2:
              {
                setState(() {
                  bottomNav = _getEmtuBottomNavigationBar();
                  baseEmtu = InfiniteScrollListView();
                });
                break;
              }
          }
          break;
        }
    }
  }

  void onSorocabaTabTapped(int index) {
    setState(() {
      _currentIndexSor = index;
      switch (_currentIndexSor) {
        case 0:
          {
            setState(() {
              baseSorocaba = SorocabaLineList();
            });
            break;
          }
        case 1:
          {
            setState(() {
              baseSorocaba = NewSorocabaBusMap();
            });
            break;
          }
        case 2:
          {
            setState(() {
              baseSorocaba = News();
            });
            break;
          }
      }
    });
  }

  void onEmtuTabTapped(int index) {
    setState(() {
      _currentIndexEmtu = index;
      switch (_currentIndexEmtu) {
        case 0:
          {
            setState(() {
              baseEmtu = MetropolitanLineList();
            });
            break;
          }
        case 1:
          {
            setState(() {
              baseEmtu = NewBusMap();
            });
            break;
          }
        case 2:
          {
            setState(() {
              baseEmtu = InfiniteScrollListView();
            });
            break;
          }
      }
    });
  }
}

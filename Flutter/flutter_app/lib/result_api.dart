import 'dart:convert';

import 'package:http/http.dart' as http;

class ResultApi<T> {

  static const bool LOGGING = false;

  late T? _result;
  late int _statusCode;
  late String _error;

  ResultApi.fromResponse(http.Response response, bool newParse) {
    this._statusCode = response.statusCode;

    if (isSuccessful()) {
      if (newParse) {
        response.headers["charset"] = "UTF-8";
        String respStr =
            Utf8Decoder(allowMalformed: true).convert(response.bodyBytes);

        if (LOGGING) {
          print(respStr);
          print("=========================================================");
        }

        const JsonDecoder decoder = const JsonDecoder();
        _result = decoder.convert(respStr);
      } else {
        if (LOGGING) {
          print(response.body);
          print("=========================================================");
        }

        const JsonDecoder decoder = const JsonDecoder();
        _result = decoder.convert(response.body);
      }
      _error = "";
    } else {
      String respStr =
            Utf8Decoder(allowMalformed: true).convert(response.bodyBytes);
      _result = null;
      _error = respStr;
    }
  }

  ResultApi.fromException(Exception e) {
    this._statusCode = -1;
    this._result = null;
    _error = e.toString();
  }

  bool isSuccessful() {
    return _statusCode >= 200 && _statusCode <= 299;
  }
  
  T? getResult() {
    return _result;
  }

  String getError() {
    return _error;
  }
}
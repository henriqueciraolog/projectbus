import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class VotorantimDepartures extends StatefulWidget {
  final line;
  final String type;
  final times;

  VotorantimDepartures(this.line, this.type, this.times);

  @override
  _VotorantimDepartures createState() =>
      _VotorantimDepartures(line, type, times);
}

class _VotorantimDepartures extends State<VotorantimDepartures> {
  var line;
  String type;
  var times;

  var sentidoTerminal = [];
  var sentidoBairro = [];

  var listaDeHorarios = [];

  Widget base = new Container();

  _VotorantimDepartures(this.line, this.type, this.times) {
    sentidoBairro = times["sentidoIda"];
    sentidoTerminal = times["sentidoVolta"];
  }

  late String bairro;
  late String terminal;

  @override
  Widget build(BuildContext context) {
    bairro = line["origem"];
    terminal = line["destino"];

    return new Scaffold(body: _buildTwoColumns());
  }

  Widget _buildTwoColumns() {
    List<Widget> schedules;
    List<Widget> headers;

    if (sentidoTerminal.isNotEmpty && sentidoBairro.isNotEmpty) {
      schedules = [
        buildScheduleList(terminal, sentidoTerminal),
        buildScheduleList(bairro, sentidoBairro),
      ];
      headers = [headerDirections(terminal), headerDirections(bairro)];
    } else if (sentidoTerminal.isEmpty && sentidoBairro.isNotEmpty) {
      schedules = [
        buildScheduleList(bairro, sentidoBairro),
      ];
      headers = [headerDirections(bairro)];
    } else if (sentidoTerminal.isNotEmpty && sentidoBairro.isEmpty) {
      schedules = [
        buildScheduleList(terminal, sentidoTerminal),
      ];
      headers = [headerDirections(terminal)];
    } else {
      schedules = [buildEmptyView()];
      headers = [];
    }

    return Stack(children: [
      SingleChildScrollView(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: schedules,
        ),
      ),
      Container(
          decoration: BoxDecoration(color: Colors.transparent),
          child: Row(children: headers)),
    ]);
  }

  Widget buildEmptyView() {
    String text;

    if (type == "diasUteis") {
      text =
          "Essa linha não opera em Dias úteis.\nRecomendamos buscar linhas alternativas.";
    } else if (type == "sabado") {
      text =
          "Essa linha não opera aos Sábados.\nRecomendamos buscar linhas alternativas.";
    } else if (type == "domingoFeriado") {
      text =
          "Essa linha não opera aos Domingos e Feriados.\nRecomendamos buscar linhas alternativas.";
    } else {
      text =
          "Essa linha não opera durante estes dias.\nRecomendamos buscar linhas alternativas.";
    }

    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * 0.8,
      child: Center(
        child: Text(
          text,
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 20,
              color: Color(0xFF01215E)),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  Widget buildScheduleList(String direction, List schedules) {
    return Container(
        width: MediaQuery.of(context).size.width * 0.5,
        child: ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: schedules.length,
            padding: new EdgeInsets.only(top: 90.0),
            itemBuilder: (context, index) {
              var antes = (index - 1 >= 0) ? schedules[index - 1] : null;
              var agora = schedules[index];
              var depois =
                  (index + 1 < schedules.length) ? schedules[index + 1] : null;

              return buildTimes(
                  schedules[index], destacarHorario(antes, agora, depois));
            }));
  }

  bool destacarHorario(antes, agora, depois) {
    DateFormat format = new DateFormat("HH:mm");

    DateTime? tAntes;
    DateTime tAgora;
    DateTime? tDepois;

    DateTime tmp = DateTime.now();
    DateTime now = format.parse("${tmp.hour}:${tmp.minute}");

    if (antes != null) tAntes = format.parse(antes["horario"]);

    tAgora = format.parse(agora["horario"]);

    if (depois != null) tDepois = format.parse(depois["horario"]);

    if (antes != null) {
      //Tenho Antes
      if (tAntes!.millisecondsSinceEpoch <
              now.millisecondsSinceEpoch && // Antes é passado
          tAgora.millisecondsSinceEpoch >
              now.millisecondsSinceEpoch) // Agora é futuro
      {
        return true;
      } else {
        return false;
      }
    } else if (depois != null) {
      //Não tenho Antes, mas tenho Depois
      if (tAgora.millisecondsSinceEpoch >
              now.millisecondsSinceEpoch && // Agora é futuro
          tDepois!.millisecondsSinceEpoch > now.millisecondsSinceEpoch) {
        // Depois é futuro
        return true;
      } else {
        return false;
      }
    } else if (tAgora.millisecondsSinceEpoch > now.millisecondsSinceEpoch) {
      //Não tenho Antes e nem Depois e Agora é futuro
      return true;
    } else {
      return false;
    }
  }

  Widget headerDirections(String direction) {
    return Container(
      decoration: BoxDecoration(color: Colors.transparent),
      width: MediaQuery.of(context).size.width * 0.5,
      padding: EdgeInsets.only(
          left: 10.0, right: 10.0, bottom: 0.0, top: 15.0),
      child: new Material(
        borderRadius: new BorderRadius.circular(6.0),
        elevation: 2.0,
        child: Container(
          margin: EdgeInsets.only(
              left: 10.0, right: 10.0, bottom: 10.0, top: 10.0),
          child: FittedBox(
              fit: BoxFit.scaleDown,
              child: Text("Sentido\n$direction".toUpperCase(),
                  maxLines: 2,
                  textAlign: TextAlign.center,
                  style: new TextStyle(
                      fontSize: 17,
                      fontWeight: FontWeight.w900,
                      color: Color(0xFF01215E)))),
        ),
      ),
    );
  }

  Widget buildTimes(time, bool destacarHorario) {
    return new GestureDetector(
        onTap: () {
          //onTapLine(line);
        },
        child: new Container(
          height: 100,
          margin: const EdgeInsets.only(
              left: 10.0, right: 10.0, bottom: 10.0, top: 0.0),
          child: new Material(
            color: destacarHorario ? Color(0xFFffcd00) : Colors.white,
            borderRadius: new BorderRadius.circular(6.0),
            elevation: 2.0,
            child: getListTime(time, destacarHorario),
          ),
        ));
  }

  Widget getListTime(time, bool destacarHorario) {
    return new Container(
      margin: new EdgeInsets.all(10.0),
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          getLineNumber(time["horario"], destacarHorario),
          getLineName(time["tipo"], destacarHorario),
        ],
      ),
    );
  }

  Widget getLineNumber(lineCode, bool destacarHorario) {
    return new Container(
        child: new Text(
      lineCode,
      style: new TextStyle(
          fontSize: 36, fontWeight: FontWeight.bold, color: Colors.black),
    ));
  }

  Widget getLineName(origem, bool destacarHorario) {
    return new Container(
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[getLineText(origem, destacarHorario)],
      ),
    );
  }

  Widget getLineText(title, bool destacarHorario) {
    return new Text(
      title,
      style: new TextStyle(
          fontWeight: FontWeight.w600, fontSize: 16, color: Colors.black),
      maxLines: 2,
      overflow: TextOverflow.clip,
    );
  }
}

import 'package:flutter/material.dart';
import 'votorantim_departures.dart';

class VotorantimLineDetails extends StatefulWidget {
  final Map line;

  VotorantimLineDetails(this.line);

  @override
  _VotorantimLineDetails createState() => _VotorantimLineDetails(line);
}

class _VotorantimLineDetails extends State<VotorantimLineDetails> {
  Map line;

  Widget base = Container();

  _VotorantimLineDetails(this.line);

  @override
  Widget build(BuildContext context) {
    String nomeLinha = "${line["origem"]} - ${line["destino"]} ${line["rota"]}";
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            isScrollable: false,
            tabs: [
              Tab(text: "Dias úteis"),
              Tab(text: "Sabado"),
              Tab(text: "Domingos\nFeriados"),
            ],
          ),
          title: Text('${line["codigo"]} - $nomeLinha'),
          backgroundColor: Color(0xFF01215E),
        ),
        body: base,
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    loadMetropolitanVotorantimLineDetails(line);
  }

  loadMetropolitanVotorantimLineDetails(line) {
    setState(() {
      base = TabBarView(
          physics: NeverScrollableScrollPhysics(),
          children: <Widget>[
            VotorantimDepartures(line, "diasUteis", line["horariosDiasUteis"]),
            VotorantimDepartures(line, "sabado", line["horariosSabados"]),
            VotorantimDepartures(line, "domingoFeriado", line["horariosDomFer"])
          ]);
    });
  }
}

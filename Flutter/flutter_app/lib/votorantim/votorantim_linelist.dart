import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:viasorocaba/commons/line_widget.dart';
import 'package:viasorocaba/commons/try_again_widget.dart';
import '../api.dart';
import 'votorantim_line_details.dart';

class VotorantimLineList extends StatefulWidget {
  @override
  _VotorantimLineListPageState createState() => _VotorantimLineListPageState();
}

class _VotorantimLineListPageState extends State<VotorantimLineList> {
  List votorantimLines = [];
  List database = [];
  Api repository = Api.instance;

  Widget bodyReplace = Container();

  @override
  Widget build(BuildContext context) {
    return bodyReplace;
  }

  @override
  void initState() {
    super.initState();

    _loadVotorantimLines();
  }

  Widget _createList() {
    return Container(
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
            child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [_createSearchField(), _getListViewWidget()])));
  }

  Widget _createSearchField() {
    return Container(
        padding: EdgeInsets.all(10.0),
        child: CupertinoSearchTextField(
          placeholder: "Busque pelo número, origem, destino ou 'via' da linha",
          onChanged: (String value) {
            _textControllerChanged(value);
            print('The text has changed to: $value');
          },
        ));
  }

  void _textControllerChanged(String text) {
    if (text.isEmpty) {
      database.clear();
      database.addAll(votorantimLines);
    } else {
      database.clear();

      for (int i = 0; i < votorantimLines.length; i++) {
        String codigo = votorantimLines[i]["codigo"];
        String titulo =
            "${votorantimLines[i]["origem"]} ${votorantimLines[i]["destino"]} ${votorantimLines[i]["rota"]}";

        if (codigo.contains(text.toUpperCase()) ||
            titulo.contains(text.toUpperCase())) {
          database.add(votorantimLines[i]);
        }
      }
    }

    setState(() {
      bodyReplace = _createList();
    });
  }

  void _loadVotorantimLines() async {
    context.loaderOverlay.show();

    repository.getVotorantimLinhas((result) {
      List lines = result;
      votorantimLines.addAll(lines);
      database.clear();
      database.addAll(votorantimLines);

      setState(() {
        bodyReplace = _createList();
      });

      context.loaderOverlay.hide();
    }, (error) {
      setState(() {
        bodyReplace = TryAgainWidget(
            "Não foi possível obter as linhas municipais de Votorantim no momento.\nMotivo:$error",
            () {
          _loadVotorantimLines();
        });
      });
      context.loaderOverlay.hide();
    });
  }

  Widget _getListViewWidget() {
    return new ListView.builder(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: database.length,
        padding: new EdgeInsets.only(top: 10.0),
        itemBuilder: (context, index) {
          return buildLine(database[index]);
        });
  }

  Widget buildLine(line) {
    return new GestureDetector(
        key: Key(line["codigo"]),
        onTap: () {
          onTapLine(line);
        },
        child: LineWidget(
            TypeLine.VIA,
            line["codigo"],
            "${line["origem"]} - ${line["destino"]}",
            line["rota"],
            Color(0xFFffcd00),
            Colors.black));
  }

  void onTapLine(line) {
    _loadHorarios(line);
  }

  _loadHorarios(line) async {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => VotorantimLineDetails(line)),
    );
  }
}

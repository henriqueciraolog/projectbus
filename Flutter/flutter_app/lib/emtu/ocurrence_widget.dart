import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'occurrence_entity.dart';

class OcurrenceWidget extends StatefulWidget {
  final OccurrenceEntity occurrenceEntity;

  OcurrenceWidget(this.occurrenceEntity);

  @override
  State<StatefulWidget> createState() => _OcurrenceWidget(occurrenceEntity);

}

class _OcurrenceWidget extends State<OcurrenceWidget> {
  final OccurrenceEntity occurrenceEntity;

  _OcurrenceWidget(this.occurrenceEntity);

  @override
  Widget build(BuildContext context) {
    return new GestureDetector(
        onTap: () {},
        child: new Container(
          margin: const EdgeInsets.only(
              left: 10.0, right: 10.0, bottom: 10.0, top: 0.0),
          child: new Material(
            borderRadius: new BorderRadius.circular(6.0),
            elevation: 2.0,
            child: getLineOccurrence(),
          ),
        ));
  }

  Widget getLineOccurrence() {
    return new Container(
      margin: new EdgeInsets.all(10.0),
      child: new Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          getOccurenceDetails(),
          new Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              occurrenceEntity.status == "open" ? Icon(Icons.lock_open) : Icon(Icons.lock),
              Text(occurrenceEntity.region!,
                  style: new TextStyle(fontWeight: FontWeight.bold))
            ],
          ),
        ],
      ),
    );
  }

  Widget getOccurenceDetails() {
    return Expanded(
        child: Container(
            margin: new EdgeInsets.fromLTRB(0, 0, 10.0, 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: _getListOfWidgetsForOccurrences(),
            )));
  }

  List<Widget> _getListOfWidgetsForOccurrences() {
    List<Widget> occurrenceWidgets = [];

    occurrenceWidgets.add(Text(occurrenceEntity.incidentType!,
        style: new TextStyle(fontWeight: FontWeight.bold)));
    occurrenceWidgets.add(Text("Local: ${occurrenceEntity.premises}"));

    if (occurrenceEntity.startDate != null && occurrenceEntity.startTime != null)
      occurrenceWidgets.add(Text(
          "Inicio: ${new DateFormat("dd/MM/yyyy").format(new DateFormat("yyyy-MM-dd").parse(occurrenceEntity.startDate!))} ${occurrenceEntity.startTime}"));
    else if (occurrenceEntity.startDate != null)
      occurrenceWidgets.add(Text(
          "Inicio: ${new DateFormat("dd/MM/yyyy").format(new DateFormat("yyyy-MM-dd").parse(occurrenceEntity.startDate!))}"));

    if (occurrenceEntity.endDate != null && occurrenceEntity.endTime != null)
      occurrenceWidgets.add(Text(
          "Fim: ${new DateFormat("dd/MM/yyyy").format(new DateFormat("yyyy-MM-dd").parse(occurrenceEntity.endDate!))} ${occurrenceEntity.endTime}"));
    else if (occurrenceEntity.endDate != null)
      occurrenceWidgets.add(Text(
          "Fim: ${new DateFormat("dd/MM/yyyy").format(new DateFormat("yyyy-MM-dd").parse(occurrenceEntity.endDate!))}"));

    if (occurrenceEntity.routes!.length > 0)
      occurrenceWidgets.add(Text("Linha(s) afetada(s): " + _concatLines()));

    if (!occurrenceEntity.contract!.contains("Permissionárias") &&
        !occurrenceEntity.contract!.contains("ÁREA"))
      occurrenceWidgets.add(Text("Consórcio: ${occurrenceEntity.contract}"));

    occurrenceWidgets.add(Text("Empresa(s): " + _concatCompanies()));

    if (occurrenceEntity.vehicles!.length > 0)
      occurrenceWidgets.add(Text("Veículo(s): " + _concatVehicles()));

    List<Widget> descriptions = _getOccurrenceDescriptions();
    if (descriptions.length > 0) {
      occurrenceWidgets.add(Text("Descrição:"));
      occurrenceWidgets.addAll(descriptions);
    }

    List<Widget> measures = _getOccurrenceMeasures();
    if (measures.length > 0) {
      occurrenceWidgets.add(Text("Dados adicionais:"));
      occurrenceWidgets.addAll(measures);
    }

    return occurrenceWidgets;
  }

  List<Widget> _getOccurrenceDescriptions() {
    List<Widget> descriptions = [];

    occurrenceEntity.descriptionFields!.forEach((item) {
      if (item.name != null && item.content != null) {
        descriptions.add(new Text("    ${item.name}: ${item.content}"));
      }
    });

    return descriptions;
  }

  List<Widget> _getOccurrenceMeasures() {
    List<Widget> measures = [];

    occurrenceEntity.measureFields!.forEach((item) {
      if (item.name != null && item.content != null) {
        measures.add(new Text("${item.name}: ${item.content}"));
      }
    });

    return measures;
  }

  String _concatLines() {
    if (occurrenceEntity.routes!.length == 0)
      return "";
    else if (occurrenceEntity.routes!.length == 1)
      return occurrenceEntity.routes![0];
    else {
      String lines = occurrenceEntity.routes![0];
      for (int i = 1; i < occurrenceEntity.routes!.length; i++) {
        lines = lines + ", " + occurrenceEntity.routes![i];
      }
      return lines;
    }
  }

  String _concatCompanies() {
    if (occurrenceEntity.companies!.length == 0)
      return "";
    else if (occurrenceEntity.companies!.length == 1)
      return occurrenceEntity.companies![0];
    else {
      String companies = occurrenceEntity.companies![0];
      for (int i = 1; i < occurrenceEntity.companies!.length; i++) {
        companies = companies + ", " + occurrenceEntity.companies![i];
      }
      return companies;
    }
  }

  String _concatVehicles() {
    if (occurrenceEntity.vehicles!.length == 0)
      return "";
    else if (occurrenceEntity.vehicles!.length == 1)
      return occurrenceEntity.vehicles![0].prefix! + " (${occurrenceEntity.vehicles![0].plate})";
    else {
      String companies =
          occurrenceEntity.vehicles![0].prefix! + " (${occurrenceEntity.vehicles![0].plate})";
      for (int i = 1; i < occurrenceEntity.companies!.length; i++) {
        companies = companies +
            ", " +
            occurrenceEntity.vehicles![i].prefix! +
            " (${occurrenceEntity.vehicles![i].plate})";
      }
      return companies;
    }
  }
}
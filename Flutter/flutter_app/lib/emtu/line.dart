
class MetropolitanLine {
  final codigo;
  final origem;
  final destino;
  final chRegiao;
  final sentido;
  final detalhes;

  MetropolitanLine(this.codigo, this.origem, this.destino, this.chRegiao, this.sentido,
      this.detalhes);
}

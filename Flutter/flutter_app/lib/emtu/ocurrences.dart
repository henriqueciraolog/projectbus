import 'package:flutter/material.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:viasorocaba/commons/empty_state_widget.dart';
import 'package:viasorocaba/commons/try_again_widget.dart';
import 'package:viasorocaba/emtu/ocurrence_widget.dart';
import '../api.dart';
import 'entity_factory.dart';
import 'line.dart';
import 'occurrence_entity.dart';

class Occurrences extends StatefulWidget {
  final MetropolitanLine line;

  Occurrences(this.line);

  @override
  _Occurrences createState() => _Occurrences(line);
}

class _Occurrences extends State<Occurrences> {
  var repository = Api.instance;

  MetropolitanLine line;
  List<OccurrenceEntity> lineOccurrences = [];

  _Occurrences(this.line);

  Widget bodyReplace = Container();

  @override
  Widget build(BuildContext context) {
    return bodyReplace;
  }

  @override
  void initState() {
    super.initState();

    _loadMetropolitanLineOccurrences();
  }

  Widget _createOcurrencesView() {
    return Container(child: Column(children: [_getListOfOccurrences()]));
  }

  Widget _getListOfOccurrences() {
    var list = new ListView.builder(
        itemCount: lineOccurrences.length,
        padding: new EdgeInsets.only(top: 5.0),
        itemBuilder: (context, index) {
          return OcurrenceWidget(lineOccurrences[index]);
        });

    return new Expanded(child: list);
  }

  void _loadMetropolitanLineOccurrences() async {
    context.loaderOverlay.show();

    repository.getEmtuOcorrenciasByLinha(line.codigo, (result) {
      lineOccurrences.clear();

      result!.forEach((item) {
        OccurrenceEntity occurrence = EntityFactory.generateOBJ(item);
        lineOccurrences.add(occurrence);
      });

      setState(() {
        if (lineOccurrences.isNotEmpty) {
          bodyReplace = _createOcurrencesView();
        } else {
          bodyReplace = EmptyStateWidget(
              "Não há ocorrências para essa linha no momento.");
        }
      });

      context.loaderOverlay.hide();
    }, (error) {
      setState(() {
        bodyReplace = TryAgainWidget(
            "Não foi possível carregar as ocorrências no momento.\nMotivo: $error",
            () {
          _loadMetropolitanLineOccurrences();
        });
      });

      context.loaderOverlay.hide();
    });
  }
}

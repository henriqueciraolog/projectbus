class OccurrenceEntity {
  String? endDate;
  String? incidentType;
  String? informedDate;
  String? code;
  String? createdAt;
  List<OccurrenceVehicle>? vehicles;
  List<String>? routes;
  String? incidentSubgroup;
  List<String>? companies;
  String? updatedAt;
  String? incidentGroup;
  String? premises;
  int? id;
  String? startDate;
  List<OccurrenceDescriptionField>? descriptionFields;
  String? contract;
  String? endTime;
  String? informedTime;
  String? startTime;
  List<OccurrenceMeasureField>? measureFields;
  String? region;
  String? status;

  OccurrenceEntity(
      {this.endDate,
      this.incidentType,
      this.informedDate,
      this.code,
      this.createdAt,
      this.vehicles,
      this.routes,
      this.incidentSubgroup,
      this.companies,
      this.updatedAt,
      this.incidentGroup,
      this.premises,
      this.id,
      this.startDate,
      this.descriptionFields,
      this.contract,
      this.endTime,
      this.informedTime,
      this.startTime,
      this.measureFields,
      this.region,
      this.status});

  OccurrenceEntity.fromJson(Map<String, dynamic> json) {
    endDate = json['end_date'];
    incidentType = json['incident_type'];
    informedDate = json['informed_date'];
    code = json['code'];
    createdAt = json['created_at'];
    if (json['vehicles'] != null) {
      vehicles = [];
      (json['vehicles'] as List).forEach((v) {
        vehicles!.add(new OccurrenceVehicle.fromJson(v));
      });
    }
    routes = json['routes']?.cast<String>();
    incidentSubgroup = json['incident_subgroup'];
    companies = json['companies']?.cast<String>();
    updatedAt = json['updated_at'];
    incidentGroup = json['incident_group'];
    premises = json['premises'];
    id = json['id'];
    startDate = json['start_date'];
    if (json['description_fields'] != null) {
      descriptionFields = [];
      (json['description_fields'] as List).forEach((v) {
        descriptionFields!.add(new OccurrenceDescriptionField.fromJson(v));
      });
    }
    contract = json['contract'];
    endTime = json['end_time'];
    informedTime = json['informed_time'];
    startTime = json['start_time'];
    if (json['measure_fields'] != null) {
      measureFields = [];
      (json['measure_fields'] as List).forEach((v) {
        measureFields!.add(new OccurrenceMeasureField.fromJson(v));
      });
    }
    region = json['region'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['end_date'] = this.endDate;
    data['incident_type'] = this.incidentType;
    data['informed_date'] = this.informedDate;
    data['code'] = this.code;
    data['created_at'] = this.createdAt;
    if (this.vehicles != null) {
      data['vehicles'] = this.vehicles!.map((v) => v.toJson()).toList();
    }
    data['routes'] = this.routes;
    data['incident_subgroup'] = this.incidentSubgroup;
    data['companies'] = this.companies;
    data['updated_at'] = this.updatedAt;
    data['incident_group'] = this.incidentGroup;
    data['premises'] = this.premises;
    data['id'] = this.id;
    data['start_date'] = this.startDate;
    if (this.descriptionFields != null) {
      data['description_fields'] =
          this.descriptionFields!.map((v) => v.toJson()).toList();
    }
    data['contract'] = this.contract;
    data['end_time'] = this.endTime;
    data['informed_time'] = this.informedTime;
    data['start_time'] = this.startTime;
    if (this.measureFields != null) {
      data['measure_fields'] =
          this.measureFields!.map((v) => v.toJson()).toList();
    }
    data['region'] = this.region;
    data['status'] = this.status;
    return data;
  }
}

class OccurrenceVehicle {
  String? prefix;
  String? plate;

  OccurrenceVehicle({this.prefix, this.plate});

  OccurrenceVehicle.fromJson(Map<String, dynamic> json) {
    prefix = json['prefix'];
    plate = json['plate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['prefix'] = this.prefix;
    data['plate'] = this.plate;
    return data;
  }
}

class OccurrenceDescriptionField {
  String? code;
  String? name;
  String? content;

  OccurrenceDescriptionField({this.code, this.name, this.content});

  OccurrenceDescriptionField.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    name = json['name'];
    content = json['content'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['name'] = this.name;
    data['content'] = this.content;
    return data;
  }
}

class OccurrenceMeasureField {
  String? code;
  String? name;
  dynamic content;

  OccurrenceMeasureField({this.code, this.name, this.content});

  OccurrenceMeasureField.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    name = json['name'];
    content = json['content'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['name'] = this.name;
    data['content'] = this.content;
    return data;
  }
}

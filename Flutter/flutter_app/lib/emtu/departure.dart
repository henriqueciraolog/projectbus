import 'package:flutter/material.dart';

class Departure extends StatelessWidget {
  final time;

  Departure(this.time);

  //BuildContext _context;

  @override
  Widget build(BuildContext context) {
    //this._context = context;
    return Container(
      width: 90.0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(
            time,
          )
        ],
      ),
    );
  }
}

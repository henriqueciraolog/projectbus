import 'dart:async';

import 'package:flutter/material.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:viasorocaba/commons/empty_state_widget.dart';
import 'package:viasorocaba/commons/try_again_widget.dart';

import '../api.dart';
import 'entity_factory.dart';
import 'occurrence_entity.dart';

class InfiniteScrollListView extends StatefulWidget {
  _InfiniteScrollListViewState createState() => _InfiniteScrollListViewState();
}

class _InfiniteScrollListViewState extends State<InfiniteScrollListView> {
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();
  var occurrences = [];
  var repository = Api.instance;

  Widget bodyReplace = Container();

  @override
  void initState() {
    super.initState();
    _loadMetropolitanOccurrences();
  }

  void _loadMetropolitanOccurrences() async {
    context.loaderOverlay.show();

    repository.getEmtuOcorrencias((result) {
      result.forEach((item) {
        OccurrenceEntity? occurrence = EntityFactory.generateOBJ(item);
        occurrences.add(occurrence);
      });

      setState(() {
        if (occurrences.isNotEmpty) {
          bodyReplace = _createListOfOcurrences();
        } else {
          bodyReplace = EmptyStateWidget(
              "Não há ocorrências nas linhas metropolitanas da Região de Sorocaba");
        }
      });

      context.loaderOverlay.hide();
    }, (error) {
      setState(() {
        bodyReplace = TryAgainWidget(
            "Não foi possível obter as ocorrências no momento.\nMotivo:$error",
            () {
          _loadMetropolitanOccurrences();
        });
      });

      context.loaderOverlay.hide();
    });
  }

  @override
  Widget build(BuildContext context) {
    return bodyReplace;
  }

  Widget _createListOfOcurrences() {
    return RefreshIndicator(
        key: _refreshIndicatorKey,
        onRefresh: _onRefresh,
        child: ListView.builder(
          itemCount: occurrences.length,
          itemBuilder: (context, index) {
            return occurrences[index];
          },
        ));
  }

  Future _onRefresh() async {
    setState(() {
      occurrences.clear();
    });

    _loadMetropolitanOccurrences();
  }
}

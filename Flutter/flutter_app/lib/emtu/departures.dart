import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../api.dart';
import 'departure.dart';
import 'line.dart';

class Departures extends StatefulWidget {
  final MetropolitanLine line;
  final String type;
  final departuresFrom;
  final departuresTo;
  final departuresCircle;

  Departures(this.line, this.type, this.departuresFrom, this.departuresTo,
      this.departuresCircle);

  @override
  _NewDepartures createState() => _NewDepartures(
      line, type, departuresFrom, departuresTo, departuresCircle);
}

class _NewDepartures extends State<Departures> {
  MetropolitanLine line;
  String type;
  var departuresFrom;
  var departuresTo;
  var departuresCircle;

  Widget base = new Container();

  var lineDeparturesAB = [];
  var lineDeparturesBA = [];
  var lineDeparturesCircle = [];

  var repository = Api.instance;

  _NewDepartures(this.line, this.type, this.departuresFrom, this.departuresTo,
      this.departuresCircle);

  @override
  Widget build(BuildContext context) {
    _organizeLists();
    return new Container(child: _buildTwoColumns());
  }

  _organizeLists() {
    if (departuresFrom != null) {
      List departuresStr = departuresFrom['horarios'][this.type];

      departuresStr.forEach((item) {
        var departure = new Departure(item);
        lineDeparturesAB.add(departure);
      });
    }

    if (departuresTo != null) {
      List departuresStr = departuresTo['horarios'][this.type];

      departuresStr.forEach((item) {
        var departure = new Departure(item);
        lineDeparturesBA.add(departure);
      });
    }

    if (departuresCircle != null) {
      List departuresStr = departuresCircle['horarios'][this.type];

      departuresStr.forEach((item) {
        var departure = new Departure(item);
        lineDeparturesAB.add(departure);
      });
    }
  }

  Widget _buildTwoColumns() {
    List<Widget> schedules;
    List<Widget> headers;

    String origem = line.origem.substring(0, line.origem.lastIndexOf("(")-1);
    String destino = line.destino.substring(0, line.destino.lastIndexOf("(")-1);

    if (origem == destino) {
      origem = line.origem;
      destino = line.destino;
    }

    if (departuresCircle == null) {
      if (lineDeparturesAB.length == 0 && lineDeparturesBA.length == 0) {
        schedules = [buildEmptyView()];
        headers = [];
      } else if (lineDeparturesAB.length == 0) {
        schedules = [
          buildScheduleList(line.origem, lineDeparturesBA),
        ];
        headers = [headerDirections(origem)];
      } else if (lineDeparturesBA.length == 0) {
        schedules = [
          buildScheduleList(line.destino, lineDeparturesAB),
        ];
        headers = [headerDirections(destino)];
      } else {
        schedules = [
          buildScheduleList(line.destino, lineDeparturesAB),
          buildScheduleList(line.origem, lineDeparturesBA),
        ];
        headers = [headerDirections(destino), headerDirections(origem)];
      }
    } else {
      if (lineDeparturesAB.length > 0) {
        schedules = [
          buildScheduleList(line.destino, lineDeparturesAB),
        ];
        headers = [headerDirections(destino)];
      } else {
        schedules = [buildEmptyView()];
        headers = [];
      }
    }

    /* return SingleChildScrollView(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: schedules,
      ),
    ); */
    return Stack(children: [
      SingleChildScrollView(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: schedules,
        ),
      ),
      Container(
          decoration: BoxDecoration(color: Colors.transparent),
          child: Row(children: headers)),
    ]);
  }

  Widget buildEmptyView() {
    String text;

    if (type == "diasUteis") {
      text =
          "Essa linha não opera em Dias úteis.\nRecomendamos buscar linhas alternativas.";
    } else if (type == "sabado") {
      text =
          "Essa linha não opera aos Sábados.\nRecomendamos buscar linhas alternativas.";
    } else if (type == "domingoFeriado") {
      text =
          "Essa linha não opera aos Domingos e Feriados.\nRecomendamos buscar linhas alternativas.";
    } else {
      text =
          "Essa linha não opera durante estes dias.\nRecomendamos buscar linhas alternativas.";
    }

    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * 0.8,
      child: Center(
        child: Text(
          text,
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 20,
              color: Color(0xFF01215E)),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  Widget buildScheduleList(String direction, List schedules) {
    return Container(
        width: MediaQuery.of(context).size.width * 0.5,
        child: 
            ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: schedules.length,
                padding: new EdgeInsets.only(top: 90.0),
                itemBuilder: (context, index) {
                  var antes = (index - 1 >= 0) ? schedules[index - 1] : null;
                  var agora = schedules[index];
                  var depois = (index + 1 < schedules.length)
                      ? schedules[index + 1]
                      : null;

                  return buildTimes(
                      schedules[index], destacarHorario(antes, agora, depois));
                })
          );
  }

  bool destacarHorario(Departure? antes, Departure agora, Departure? depois) {
    DateFormat format = new DateFormat("HH:mm");

    DateTime? tAntes;
    DateTime tAgora;
    DateTime? tDepois;

    DateTime tmp = DateTime.now();
    DateTime now = format.parse("${tmp.hour}:${tmp.minute}");

    if (antes != null) tAntes = format.parse(antes.time);

    tAgora = format.parse(agora.time);

    if (depois != null) tDepois = format.parse(depois.time);

    if (antes != null) {
      //Tenho Antes
      if (tAntes!.millisecondsSinceEpoch <
              now.millisecondsSinceEpoch && // Antes é passado
          tAgora.millisecondsSinceEpoch >
              now.millisecondsSinceEpoch) // Agora é futuro
      {
        return true;
      } else {
        return false;
      }
    } else if (depois != null) {
      //Não tenho Antes, mas tenho Depois
      if (tAgora.millisecondsSinceEpoch >
              now.millisecondsSinceEpoch && // Agora é futuro
          tDepois!.millisecondsSinceEpoch > now.millisecondsSinceEpoch) {
        // Depois é futuro
        return true;
      } else {
        return false;
      }
    } else if (tAgora.millisecondsSinceEpoch > now.millisecondsSinceEpoch) {
      //Não tenho Antes e nem Depois e Agora é futuro
      return true;
    } else {
      return false;
    }
  }

  Widget headerDirections(String direction) {
    return Container(
      decoration: BoxDecoration(color: Colors.transparent),
      width: MediaQuery.of(context).size.width * 0.5,
      padding: EdgeInsets.only(
          left: 10.0, right: 10.0, bottom: 0.0, top: 15.0),
      child: new Material(
        borderRadius: new BorderRadius.circular(6.0),
        elevation: 2.0,
        child: Container(
          margin: EdgeInsets.only(
              left: 10.0, right: 10.0, bottom: 10.0, top: 10.0),
          child: FittedBox(
              fit: BoxFit.scaleDown,
              child: Text("Sentido\n$direction".toUpperCase(),
                  maxLines: 2,
                  textAlign: TextAlign.center,
                  style: new TextStyle(
                      fontSize: 17,
                      fontWeight: FontWeight.w900,
                      color: Color(0xFF01215E)))),
        ),
      ),
    );
  }

  Widget buildTimes(Departure time, bool destacarHorario) {
    return new GestureDetector(
        onTap: () {
          //onTapLine(line);
        },
        child: new Container(
          height: 100,
          margin: const EdgeInsets.only(
              left: 10.0, right: 10.0, bottom: 10.0, top: 0.0),
          child: new Material(
            color: destacarHorario ? Color(0xFFba251e) : Colors.white,
            borderRadius: new BorderRadius.circular(6.0),
            elevation: 2.0,
            child: getListTime(time, destacarHorario),
          ),
        ));
  }

  Widget getListTime(Departure time, bool destacarHorario) {
    return new Container(
      margin: new EdgeInsets.all(10.0),
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          getLineNumber(time.time, destacarHorario),
        ],
      ),
    );
  }

  Widget getLineNumber(lineCode, bool destacarHorario) {
    return new Center(
        child: new Container(
            child: new Text(
      lineCode,
      style: new TextStyle(
          fontSize: 36,
          fontWeight: FontWeight.bold,
          color: destacarHorario ? Colors.white : Colors.black),
    )));
  }
}

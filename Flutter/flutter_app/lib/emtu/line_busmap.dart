import 'dart:async';

import 'package:flutter/material.dart';
import 'package:viasorocaba/emtu/line.dart';
import 'package:syncfusion_flutter_maps/maps.dart';

import '../api.dart';

class LineBusMap extends StatefulWidget {
  final MetropolitanLine line;

  LineBusMap(this.line);

  @override
  _LineBusMapState createState() => _LineBusMapState(this.line);
}

class _LineBusMapState extends State<LineBusMap> {
  final MetropolitanLine line;

  _LineBusMapState(this.line);

  late MapTileLayerController _controller;

  var _repository = Api.instance;

  List _buses = [];

  List<List<MapLatLng>> itinerarios = [];

  bool firstCall = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SfMaps(layers: [_mapLayer()]),
    );
  }

  late var _timer;

  void startTimer() {
    const oneSec = const Duration(seconds: 15);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) => setState(
        () {
          _loadBusInfoByLine();
        },
      ),
    );
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  MapTileLayer _mapLayer() {
    return MapTileLayer(
      zoomPanBehavior: MapZoomPanBehavior(
          focalLatLng: MapLatLng(-23.499747, -47.452970),
          zoomLevel: 9,
          maxZoomLevel: 18,
          enableDoubleTapZooming: true),
      urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
      initialMarkersCount: 0,
      markerBuilder: (BuildContext context, int index) {
        return MapMarker(
          size: Size(44, 44),
          latitude: _buses[index]["latitude"],
          longitude: _buses[index]["longitude"],
          child: _createMarker(
              _buses[index]["codigoLinha"],
              _buses[index]["sentidoLinha"] == "ida",
              _buses[index]["prefixo"],
              _buses[index]["empresa"]),
        );
      },
      markerTooltipBuilder: (BuildContext context, int index) {
        return _createMarkerTooltip(_buses[index]);
      },
      sublayers: [
        MapPolylineLayer(
          polylines: List<MapPolyline>.generate(
            itinerarios.length,
            (int index) {
              return MapPolyline(
                points: itinerarios[index],
              );
            },
          ).toSet(),
          color: Colors.black,
        )
      ],
      controller: _controller,
    );
  }

  Widget _createMarker(
      String codigoLinha, bool isIda, String prefixo, String companyName) {
    var bgColor = _getBackgroundColorByCompany(companyName);
    var fontColor = _getFontColorByCompany(companyName);

    return Column(children: [
      Text(
        prefixo,
        style: TextStyle(
            fontSize: 8,
            fontWeight: FontWeight.w800,
            backgroundColor: bgColor,
            color: fontColor),
      ),
      isIda
          ? Icon(Icons.directions_bus_outlined, color: bgColor)
          : Icon(Icons.directions_bus_filled, color: bgColor)
    ]);
  }

  @override
  void initState() {
    super.initState();
    _controller = MapTileLayerController();

    _loadBusInfoByLine();

    startTimer();
  }

  void _loadBusInfoByLine() async {
    _repository.getMapsEmtuByLinha(line.codigo, (result) {
      if (firstCall) {
        _updateMapWithRoutes(result);
        firstCall = false;
      }
      List buses = result!["linhas"][0]["veiculos"];

      buses.forEach((bus) {
        bus["detalhesLinha"] = line;
        _buses.removeWhere((bus2) => bus["prefixo"] == bus2["prefixo"]);
      });

      _buses.removeWhere((bus) => bus["codigoLinha"] == line);

      _buses.addAll(buses);

      _controller.clearMarkers();
      for (var i = 0; i < _buses.length; i++) {
        _controller.insertMarker(i);
      }
    }, (error) {
      print("Erro ao obter dados da linha ${line.codigo}. Motivo: $error");
    });
  }

  void _updateMapWithRoutes(result) {
    List polylinesEncodeds = result["linhas"][0]["rotas"];

    polylinesEncodeds.forEach((iti) {
      List<MapLatLng> latLngs = _decodeEncodedPolyline(iti["encode"]);

      setState(() {
        itinerarios.add(latLngs);
      });
    });
  }

  Widget _createMarkerTooltip(bus) {
    MetropolitanLine mLine = bus["detalhesLinha"];

    String company = bus["empresa"];
    Color? bgColor = _getBackgroundColorByCompany(company);
    Color? fontColor = _getFontColorByCompany(company);

    String direction = bus["sentidoLinha"].toString().toUpperCase();
    String nomeLinha = direction == "ida" ? mLine.destino : mLine.origem;
    return Container(
      color: bgColor,
      width: 400,
      padding: const EdgeInsets.all(10),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Stack(
            children: [
              Center(
                child: Text(
                  "${mLine.codigo}: $nomeLinha",
                  style: TextStyle(
                      color: fontColor,
                      fontSize:
                          Theme.of(context).textTheme.bodyText2!.fontSize),
                ),
              ),
              Icon(
                Icons.bus_alert,
                color: fontColor,
                size: 16,
              ),
            ],
          ),
          Divider(
            color: fontColor,
            height: 10,
            thickness: 1.2,
          ),
          Text(
            "Placa: ${bus["placa"]}\nOperado por $company",
            style: TextStyle(
                color: fontColor,
                fontSize: Theme.of(context).textTheme.bodyText2!.fontSize),
          ),
        ],
      ),
    );
  }

  Color? _getBackgroundColorByCompany(String company) {
    if (company == "VIAÃAO CALVIP") {
      return Colors.blueGrey[700];
    } else if (company == "SAO JOAO") {
      return Colors.lightBlue[900];
    } else if (company == "VILA ELVIO") {
      return Colors.grey[850];
    } else if (company == "EXPRESSO AMARELINHO") {
      return Colors.amber[400];
    } else if (company == "TIETEENSE TURISMO") {
      return Colors.white;
    } else if (company == "RAPIDO CAMPINAS") {
      return Colors.orange[800];
    } else if (company == "VB TRANSPORTES E TURISMO") {
      return Colors.black;
    } else {
      print(company);
      return Colors.green;
    }
  }

  Color? _getFontColorByCompany(String company) {
    if (company == "VIAÃAO CALVIP") {
      return Colors.white;
    } else if (company == "SAO JOAO") {
      return Colors.white;
    } else if (company == "VILA ELVIO") {
      return Colors.white;
    } else if (company == "EXPRESSO AMARELINHO") {
      return Colors.black;
    } else if (company == "TIETEENSE TURISMO") {
      return Colors.black;
    } else if (company == "RAPIDO CAMPINAS") {
      return Colors.white;
    } else if (company == "VB TRANSPORTES E TURISMO") {
      return Colors.white;
    } else {
      print(company);
      return Colors.white;
    }
  }

  List<MapLatLng> _decodeEncodedPolyline(String encoded) {
    List<MapLatLng> poly = [];
    int index = 0, len = encoded.length;
    int lat = 0, lng = 0;

    while (index < len) {
      int b, shift = 0, result = 0;
      do {
        b = encoded.codeUnitAt(index++) - 63;
        result |= (b & 0x1f) << shift;
        shift += 5;
      } while (b >= 0x20);
      int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
      lat += dlat;

      shift = 0;
      result = 0;
      do {
        b = encoded.codeUnitAt(index++) - 63;
        result |= (b & 0x1f) << shift;
        shift += 5;
      } while (b >= 0x20);
      int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
      lng += dlng;
      MapLatLng p =
          new MapLatLng((lat / 1E5).toDouble(), (lng / 1E5).toDouble());
      poly.add(p);
    }
    return poly;
  }
}

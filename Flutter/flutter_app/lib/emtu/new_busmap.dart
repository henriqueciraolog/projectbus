import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_maps/maps.dart';

import '../api.dart';

class NewBusMap extends StatefulWidget {
  @override
  _NewBusMapState createState() => _NewBusMapState();
}

class _NewBusMapState extends State<NewBusMap> {

  late MapTileLayerController _controller;

  var _repository = Api.instance;

  List _buses = [];

  bool _isDisposed = false;

  int linesInAsync = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SfMaps(layers: [_mapLayer()]),
      floatingActionButton: FloatingActionButton(
        backgroundColor: const Color(0xff03dac6),
        foregroundColor: Colors.black,
        onPressed: () {
          if (linesInAsync == 0) {
            _loadMetropolitanLines();
          } else {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text("Você não pode atualizar tão rapidamente..."),
            ));
          }
        },
        child: Icon(Icons.refresh_sharp),
      )
    );
  }

  MapTileLayer _mapLayer() {
    return MapTileLayer(
      zoomPanBehavior: MapZoomPanBehavior(
          focalLatLng: MapLatLng(-23.499747, -47.452970),
          zoomLevel: 9,
          maxZoomLevel: 18,
          enableDoubleTapZooming: true),
      urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
      initialMarkersCount: 0,
      markerBuilder: (BuildContext context, int index) {
        return MapMarker(
          size: Size(44, 44),
          latitude: _buses[index]["latitude"],
          longitude: _buses[index]["longitude"],
          child: _createMarker(
              _buses[index]["codigoLinha"],
              _buses[index]["sentidoLinha"] == "ida",
              _buses[index]["prefixo"],
              _buses[index]["empresa"]),
        );
      },
      markerTooltipBuilder: (BuildContext context, int index) {
        return _createMarkerTooltip(_buses[index]);
      },
      controller: _controller,
    );
  }

  Widget _createMarker(
      String codigoLinha, bool isIda, String prefixo, String companyName) {
    var bgColor = _getBackgroundColorByCompany(companyName);
    var fontColor = _getFontColorByCompany(companyName);

    return Column(children: [
      Text(
        prefixo,
        style: TextStyle(
            fontSize: 8,
            fontWeight: FontWeight.w800,
            backgroundColor: bgColor,
            color: fontColor),
      ),
      isIda
          ? Icon(Icons.directions_bus_outlined, color: bgColor)
          : Icon(Icons.directions_bus_filled, color: bgColor),
      Text(codigoLinha,
          style: TextStyle(
              fontSize: 8,
              fontWeight: FontWeight.w800,
              backgroundColor: bgColor,
              color: fontColor))
    ]);
  }

  @override
  void initState() {
    super.initState();
    _controller = MapTileLayerController();

    _loadMetropolitanLines();
  }

  void _loadMetropolitanLines() async {
    /* var retorno = await _repository.loadMetropolitanLines();

    List lines = retorno!;

    linesInAsync = lines.length;

    for (Map line in lines) {
      if (!_isDisposed) {
        _loadBusInfoByLine(line);
      } else {
        break;
      }
    } */

    _repository.getEmtuLinhas((result) { 
      List lines = result!;

      linesInAsync = lines.length;

      for (Map line in lines) {
        if (!_isDisposed) {
          _loadBusInfoByLine(line);
        } else {
          break;
        }
      }
    }, (error) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Estamos enfrentando dificuldades para obter as informações para o mapa. Tente novamente mais tarde.\nMotivo: {$error}"),
      ));
    });
  }

  void _loadBusInfoByLine(Map line) async {
    String linha = line["codigo"];
    _repository.getMapsEmtuByLinha(linha, (result) {
      linesInAsync--;

      List buses = result!["linhas"][0]["veiculos"];

      buses.forEach((bus) {
        bus["detalhesLinha"] = line;
        _buses.removeWhere((bus2) => bus["prefixo"] == bus2["prefixo"]);
      });

      _buses.removeWhere((bus) => bus["codigoLinha"] == line);

      _buses.addAll(buses);

      _controller.clearMarkers();
      for (var i = 0; i < _buses.length; i++) {
        _controller.insertMarker(i);
      }
    }, (error) {
      linesInAsync--;
      print("Erro ao obter dados da linha $linha. Motivo: $error");
    });
  }

  @override
  void dispose() {
    _isDisposed = true;
    super.dispose();
  }

  Widget _createMarkerTooltip(bus) {
    Map detalhesLinha = bus["detalhesLinha"];

    //int companyId = service["companyId"];
    String company = bus["empresa"];
    Color? bgColor = _getBackgroundColorByCompany(company);
    Color? fontColor = _getFontColorByCompany(company);

    // if (companyId == 37) {
    //   bgColor = Colors.green[900];
    //   company = "Consórcio Sorocaba";
    // } else if (companyId == 655771) {
    //   bgColor = Colors.blue[800];
    //   company = "BRT Sorocaba";
    // } else {
    //   bgColor = Colors.amber[900];
    //   company = "City Sorocaba";
    // }

    String direction = bus["sentidoLinha"].toString().toUpperCase();
    String nomeLinha = direction == "ida" ? detalhesLinha["destino"] : detalhesLinha["origem"];
    return Container(
      color: bgColor,
      width: 400,
      padding: const EdgeInsets.all(10),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Stack(
            children: [
              Center(
                child: Text(
                  "${detalhesLinha["codigo"]}: $nomeLinha",
                  style: TextStyle(
                      color: fontColor,
                      fontSize:
                          Theme.of(context).textTheme.bodyText2!.fontSize),
                ),
              ),
              Icon(
                Icons.bus_alert,
                color: fontColor,
                size: 16,
              ),
            ],
          ),
          Divider(
            color: fontColor,
            height: 10,
            thickness: 1.2,
          ),
          Text(
            "Placa: ${bus["placa"]}\nOperado por $company",
            style: TextStyle(
                color: fontColor,
                fontSize: Theme.of(context).textTheme.bodyText2!.fontSize),
          ),
        ],
      ),
    );
  }

  Color? _getBackgroundColorByCompany(String company) {
    if (company == "VIAÃAO CALVIP") {
      return Colors.blueGrey[700];
    } else if (company == "SAO JOAO") {
      return Colors.lightBlue[900];
    } else if (company == "VILA ELVIO") {
      return Colors.grey[850];
    } else if (company == "EXPRESSO AMARELINHO") {
      return Colors.amber[400];
    } else if (company == "TIETEENSE TURISMO") {
      return Colors.white;
    } else if (company == "RAPIDO CAMPINAS") {
      return Colors.orange[800];
    } else if (company == "VB TRANSPORTES E TURISMO") {
      return Colors.black;
    } else {
      print(company);
      return Colors.green;
    }
  }

  Color? _getFontColorByCompany(String company) {
    if (company == "VIAÃAO CALVIP") {
      return Colors.white;
    } else if (company == "SAO JOAO") {
      return Colors.white;
    } else if (company == "VILA ELVIO") {
      return Colors.white;
    } else if (company == "EXPRESSO AMARELINHO") {
      return Colors.black;
    } else if (company == "TIETEENSE TURISMO") {
      return Colors.black;
    } else if (company == "RAPIDO CAMPINAS") {
      return Colors.white;
    } else if (company == "VB TRANSPORTES E TURISMO") {
      return Colors.white;
    } else {
      print(company);
      return Colors.white;
    }
  }

}

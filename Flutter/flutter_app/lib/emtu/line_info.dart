import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'line.dart';

class LineInfo extends StatefulWidget {
  final MetropolitanLine line;

  LineInfo(this.line);

  @override
  _LineInfo createState() => _LineInfo(line);
}

class _LineInfo extends State<LineInfo> {
  final formatCurrency = NumberFormat.simpleCurrency(locale: "pt_BR");
  final double tarifaColumnWidth = 70;
  final TextStyle boldStyle = TextStyle(fontWeight: FontWeight.bold);

  MetropolitanLine line;

  _LineInfo(this.line);

  @override
  Widget build(BuildContext context) {
    List<Widget> cards = [];
    Map detalhesSentido;

    if (line.detalhes["detalhesIda"] != null) {
      detalhesSentido = line.detalhes["detalhesIda"];
    } else if (line.detalhes["detalhesVolta"] != null) {
      detalhesSentido = line.detalhes["detalhesVolta"];
    } else {
      detalhesSentido = line.detalhes["detalhesCircular"];
    }

    cards.add(_makeCardContainer(
        _createDenominationCard(line.codigo, line.origem, line.destino)));
    cards.add(
        _makeCardContainer(_createTarifaCard(detalhesSentido["valorTarifa"])));

    String? observacoes = detalhesSentido["observacao"];
    if (observacoes != null && observacoes.isNotEmpty) {
      cards.add(_makeCardContainer(_createObservacaoCard(observacoes)));
    }

    List? seccionamentos = detalhesSentido["seccionamento"];
    if (seccionamentos != null && seccionamentos.isNotEmpty) {
      cards.add(_makeCardContainer(_createSeccionamentosCard(seccionamentos)));
    }

    List? integracoes = detalhesSentido["integracoes"];
    if (integracoes != null && integracoes.isNotEmpty) {
      cards.add(_makeCardContainer(_createIntegracoesCard(integracoes)));
    }

    return Scaffold(
      body: SingleChildScrollView(
      child: Column(
        children: cards,
      ),
    )
    );
  }

  Widget _createDenominationCard(
      String lineCode, String origem, String destino) {
    return Row(
      children: [
        _createLineCodeStylized(lineCode),
        Container(width: 10),
        Expanded(child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
          children: [Text(origem, style: boldStyle), Text(destino, style: boldStyle)],
        ))
      ],
    );
  }

  Widget _createLineCodeStylized(String lineCode) {
    return Material(
      color: Color(0xFFba251e),
      borderRadius: BorderRadius.circular(6.0),
      child: Container(
          margin: EdgeInsets.all(10.0),
          child: FittedBox(
              fit: BoxFit.fitWidth,
              child: Text(
                lineCode,
                style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w900,
                    color: Colors.white),
              ))),
    );
  }

  Widget _createTarifaCard(double tarifa) {
    String tarifaStr = formatCurrency.format(tarifa);
    return Row(children: [
      Text("Tarifa praticada: ", style: boldStyle),
      Text(tarifaStr)
    ]);
  }

  Widget _createSeccionamentosCard(List seccionamentos) {
    List<Widget> seccionamentosWidgets = [];

    seccionamentosWidgets.add(Text("Seccionamentos:", style: boldStyle));
    seccionamentosWidgets.add(Text(" ", style: boldStyle));

    seccionamentos.forEach((seccionamento) {
      seccionamentosWidgets.add(_createSeccionamentoItem(
        seccionamento["trecho"],
        seccionamento["valor"],
      ));
      seccionamentosWidgets.add(Divider(
        thickness: 2,
      ));
    });

    seccionamentosWidgets.removeLast();

    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: seccionamentosWidgets);
  }

  Widget _createSeccionamentoItem(String descricao, double tarifa) {
    String descricaoLimpa = descricao
        .replaceAll(RegExp(r'[\)]+\s+[\-]'), ") -")
        .replaceAll(RegExp(r'[\)]+\s+$'), ")");
    List<String> seccionamentos = descricaoLimpa.split("-");
    String tarifaStr = formatCurrency.format(tarifa);

    return Row(children: [
      Expanded(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [Text(seccionamentos[0].trim()), Text(seccionamentos[1].trim())])),
      Text(tarifaStr)
    ]);
  }

  Widget _createIntegracoesCard(List integracoes) {
    List<Widget> integracoesWidget = [];

    integracoesWidget.add(Text("Integrações:", style: boldStyle));
    integracoesWidget.add(Text(" ", style: boldStyle));
    integracoesWidget.add(_createIntegracaoHeader());
    integracoesWidget.add(Text(" ", style: boldStyle));

    integracoes.forEach((integracao) {
      integracoesWidget.add(_createIntegracaoItem(integracao["nomeintegracao"],
          integracao["tarifaIntegracao"], integracao["tarifaVT"]));
      integracoesWidget.add(Divider(
        thickness: 2,
      ));
    });

    integracoesWidget.removeLast();

    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: integracoesWidget);
  }

  Widget _createIntegracaoHeader() {
    return Row(
      children: [
        Expanded(child: Text("NOME", style: boldStyle)),
        Container(
            width: tarifaColumnWidth, child: Text("TARIFA", style: boldStyle)),
        Container(
            width: tarifaColumnWidth,
            child: Text("TARIFA VT", style: boldStyle))
      ],
    );
  }

  Widget _createIntegracaoItem(
      String nomeIntegracao, double tarifaIntegracao, double tarifaVT) {
    String tarifaIntegracaoStr = formatCurrency.format(tarifaIntegracao);
    String tarifaVTStr = formatCurrency.format(tarifaIntegracao);

    return Row(
      children: [
        Expanded(child: Text(nomeIntegracao)),
        Container(width: tarifaColumnWidth, child: Text(tarifaIntegracaoStr)),
        Container(width: tarifaColumnWidth, child: Text(tarifaVTStr))
      ],
    );
  }


  Widget _createObservacaoCard(String observacao) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(observacao, style: boldStyle)
        ],
    );
  }

  Widget _makeCardContainer(Widget content) {
    return Container(
        width: MediaQuery.of(context).size.width,
        margin: const EdgeInsets.only(
            left: 10.0, right: 10.0, bottom: 10.0, top: 15.0),
        child: Material(
            borderRadius: BorderRadius.circular(6.0),
            elevation: 2.0,
            child: Container(
                margin: const EdgeInsets.only(
                    left: 10.0, right: 10.0, bottom: 10.0, top: 10.0),
                child: content)));
  }
}

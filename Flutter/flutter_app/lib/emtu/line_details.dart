import 'package:flutter/material.dart';
import 'package:viasorocaba/emtu/line_busmap.dart';
import 'departures.dart';
import 'line.dart';
import 'line_info.dart';
import 'ocurrences.dart';

class LineDetails extends StatefulWidget {
  final MetropolitanLine line;

  LineDetails(this.line);

  @override
  _LineDetails createState() => _LineDetails(line);
}

class _LineDetails extends State<LineDetails> {
  MetropolitanLine line;

  Widget base = Container();

  var departuresFrom;
  var departuresTo;
  var departuresCircle;

  _LineDetails(this.line);

  @override
  Widget build(BuildContext context) {
    String origem = line.origem
        .toString()
        .substring(0, line.origem.toString().lastIndexOf("(") - 1);
    String destino = line.destino
        .toString()
        .substring(0, line.destino.toString().lastIndexOf("(") - 1);

    if (origem == destino) {
      origem = line.origem;
      destino = line.destino;
    }

    return DefaultTabController(
      length: 6,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            isScrollable: true,
            tabs: [
              Tab(text: "Infomações"),
              Tab(text: "Dias\núteis"),
              Tab(text: "Sabados"),
              Tab(text: "Domingos\nFeriados"),
              Tab(text: "Tempo\nReal"),
              Tab(text: "Ocorrências"),
            ],
          ),
          title: FittedBox(child: Text("${line.codigo} - $origem / $destino")),
          backgroundColor: Color(0xFF01215E),
        ),
        body: base,
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    loadMetropolitanLineDetails(line);
  }

  loadMetropolitanLineDetails(MetropolitanLine line) {
    if (line.sentido.toString() == "IDA" ||
        line.sentido.toString() == "VOLTA") {
      departuresFrom = line.detalhes["detalhesIda"];
      departuresTo = line.detalhes["detalhesVolta"];
    } else if (line.sentido.toString() == "CIRCULAR") {
      departuresCircle = line.detalhes["detalhesCircular"];
    }

    setState(() {
      base = TabBarView(
          physics: NeverScrollableScrollPhysics(),
          children: <Widget>[
            LineInfo(line),
            Departures(line, "diasUteis", departuresFrom, departuresTo,
                departuresCircle),
            Departures(
                line, "sabado", departuresFrom, departuresTo, departuresCircle),
            Departures(line, "domingoFeriado", departuresFrom, departuresTo,
                departuresCircle),
            LineBusMap(line),
            Occurrences(line),
          ]);
    });
  }
}

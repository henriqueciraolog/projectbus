import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:viasorocaba/commons/line_widget.dart';
import 'package:viasorocaba/commons/try_again_widget.dart';

import '../api.dart';
import 'line.dart';
import 'line_details.dart';

class MetropolitanLineList extends StatefulWidget {
  @override
  _MetropolitanLineList createState() => _MetropolitanLineList();
}

class _MetropolitanLineList extends State<MetropolitanLineList> {
  List<MetropolitanLine> metropolitanLines = [];
  List<MetropolitanLine> database = [];
  var repository = Api.instance;

  Widget bodyReplace = Container();

  var result;

  @override
  Widget build(BuildContext context) {
    return bodyReplace;
  }

  @override
  void initState() {
    super.initState();

    _loadMetropolitanLines();
  }

  void _loadMetropolitanLines() async {
    context.loaderOverlay.show();

    metropolitanLines.clear();

    repository.getEmtuLinhas((result) {
      _onGetEmtuLinhasSuccessful(result);
    }, (error) {
      context.loaderOverlay.hide();
      setState(() {
        bodyReplace = TryAgainWidget(
            "Não foi possível carregar as linhas da EMTU no momento.\nMotivo: $error",
            () {
          _loadMetropolitanLines();
        });
      });
    });
  }

  Widget _createList() {
    return Container(
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
            child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [_createSearchField(), _getListViewWidget()])));
  }

  Widget _createSearchField() {
    return Container(
        padding: EdgeInsets.all(10.0),
        child: CupertinoSearchTextField(
          placeholder: "Busque pelo número, origem ou destino da linha",
          onChanged: (String value) {
            _textControllerChanged(value);
            print('The text has changed to: $value');
          },
        ));
  }

  void _textControllerChanged(String text) {
    if (text.isEmpty) {
      database.clear();
      database.addAll(metropolitanLines);
    } else {
      database.clear();

      for (int i = 0; i < metropolitanLines.length; i++) {
        String codigo = metropolitanLines[i].codigo;
        String titulo =
            "${metropolitanLines[i].origem} ${metropolitanLines[i].sentido}";

        if (codigo.contains(text.toUpperCase()) ||
            titulo.contains(text.toUpperCase())) {
          database.add(metropolitanLines[i]);
        }
      }
    }

    setState(() {
      bodyReplace = _createList();
    });
  }

  Widget _getListViewWidget() {
    return new ListView.builder(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: database.length,
        padding: new EdgeInsets.only(top: 10.0),
        itemBuilder: (context, index) {
          return _createLineWidget(database[index]);
        });
  }

  Widget _createLineWidget(MetropolitanLine metropolitanLine) {
    return new GestureDetector(
        key: Key(metropolitanLine.codigo),
        onTap: () {
          _onTapLine(metropolitanLine);
        },
        child: LineWidget(
            TypeLine.DESTINATION,
            metropolitanLine.codigo,
            metropolitanLine.origem,
            metropolitanLine.destino,
            Color(0xFFba251e),
            Colors.white));
  }

  void _onTapLine(MetropolitanLine metropolitanLine) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => LineDetails(metropolitanLine)),
    );
  }

  void _onGetEmtuLinhasSuccessful(result) {
    context.loaderOverlay.hide();
    List lines = result;

    lines.forEach((item) {
      var line = new MetropolitanLine(item['codigo'], item['origem'],
          item['destino'], item['chRegiao'], item['sentido'], item['detalhes']);

      metropolitanLines.add(line);
    });

    database.clear();
    database.addAll(metropolitanLines);

    setState(() {
      bodyReplace = _createList();
    });
  }
}

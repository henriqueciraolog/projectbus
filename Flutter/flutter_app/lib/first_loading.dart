import 'dart:async';
import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'tela_principal.dart';

class InitialLoading extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _InitialLoadingState();
}

class _InitialLoadingState extends State<InitialLoading> {
  @override
  void initState() {
    super.initState();

    _loadNotificationsConfig();
    _loadMenuConfig();

    new Timer(Duration(seconds: 1), () {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute<void>(
          builder: (BuildContext context) => LoaderOverlay(
            useDefaultLoading: false,
            overlayWidget: Center(
              child: SpinKitFadingCircle(color: Colors.indigo[900]),
            ),
            overlayOpacity: 0.8,
            child: TelaPrincipal(),
          ),
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      color: Color(0xFF01215E),
      child: Center(
        child: Column(
          children: [
            /* SpinKitFadingCircle(color: Colors.indigo[900]), */
            Text(
              "Estamos preparando tudo para você...",
              style: TextStyle(
                color: Colors.white,
              ),
            )
          ],
        ),
      ),
    ));
  }

  _loadNotificationsConfig() async {
    if (Platform.isIOS || Platform.isAndroid) {
      await Firebase.initializeApp();

      FirebaseMessaging messaging = FirebaseMessaging.instance;

      NotificationSettings settings = await messaging.requestPermission(
        alert: true,
        announcement: false,
        badge: true,
        carPlay: false,
        criticalAlert: false,
        provisional: false,
        sound: true,
      );

      if (settings.authorizationStatus == AuthorizationStatus.authorized) {
        print('User granted permission');
      } else if (settings.authorizationStatus ==
          AuthorizationStatus.provisional) {
        print('User granted provisional permission');
      } else {
        print('User declined or has not accepted permission');
      }
    }
  }

  void _loadMenuConfig() {}
}

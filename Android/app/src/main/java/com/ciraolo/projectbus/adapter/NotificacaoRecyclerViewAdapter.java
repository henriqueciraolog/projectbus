package com.ciraolo.projectbus.adapter;

//================================================================================
// IMPORTS
//================================================================================

import android.content.Context;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ciraolo.projectbus.R;
import com.ciraolo.projectbus.firebase.Notificacao;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificacaoRecyclerViewAdapter extends RecyclerView.Adapter<NotificacaoRecyclerViewAdapter.ViewHolder> {

    //================================================================================
    // PROPERTIES
    //================================================================================
    private List<Notificacao> mDataset;
    private final Context mContext;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {

        //PROPERTIES
        @BindView(R.id.txvTituloNotificacao)
        TextView txvTituloNotificacao;
        @BindView(R.id.txvMensagemNotificacao)
        TextView txvMensagemNotificacao;
        @BindView(R.id.imgIconNotificacao)
        AppCompatImageView imgIconNotificacao;

        ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    //================================================================================
    // CONSTRUCTOR
    //================================================================================
    // Provide a suitable constructor (depends on the kind of dataset)
    public NotificacaoRecyclerViewAdapter(Context context, List<Notificacao> myDataset) {
        mContext = context;
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_view_notificacao, parent, false);

        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Notificacao notificacao = mDataset.get(position);

        holder.txvTituloNotificacao.setText(notificacao.titulo);
        holder.txvMensagemNotificacao.setText(notificacao.mensagem);

        switch (notificacao.prioridade){

            case URGENTE:
                holder.imgIconNotificacao.getBackground().setColorFilter(ContextCompat.getColor(mContext, R.color.vermelho_notificacao), PorterDuff.Mode.SRC_ATOP);
                break;
            case IMPORTANTE:
                holder.imgIconNotificacao.getBackground().setColorFilter(ContextCompat.getColor(mContext, R.color.laranja_notificacao), PorterDuff.Mode.SRC_ATOP);
                break;
            case AVISO:
                holder.imgIconNotificacao.getBackground().setColorFilter(ContextCompat.getColor(mContext, R.color.ambar_notificacao), PorterDuff.Mode.SRC_ATOP);
                break;
            case NOTICIA:
                holder.imgIconNotificacao.getBackground().setColorFilter(ContextCompat.getColor(mContext, R.color.azul_notificacao), PorterDuff.Mode.SRC_ATOP);
                break;
            case INFORMACAO:
                holder.imgIconNotificacao.getBackground().setColorFilter(ContextCompat.getColor(mContext, R.color.verde_notificacao), PorterDuff.Mode.SRC_ATOP);
                break;
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void updateList(List<Notificacao> newlist) {
        mDataset = new ArrayList<>();
        mDataset.addAll(newlist);
        notifyDataSetChanged();
    }
}




package com.ciraolo.projectbus.model.information;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.List;

public class RespostaObterInformacoes implements Serializable {

    public Horarios horarios;
    public List<Seccionamento> seccionamento;
    public Integer tempoDePercurso;
    public Double valorTarifa;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
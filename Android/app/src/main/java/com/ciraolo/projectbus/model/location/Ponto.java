package com.ciraolo.projectbus.model.location;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class Ponto implements Serializable{

    public Long id;
    public Double latitude;
    public Double longitude;
    public Integer raio;
    public String endereco;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
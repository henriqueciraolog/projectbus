package com.ciraolo.projectbus.api;

import com.ciraolo.projectbus.model.location.RepostaObterDadosPortal;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by henriqueciraolo on 22/01/17.
 */

public interface ApontamentoLocalizacao {
        @Headers({"Authorization: Basic cm9iby1lbXR1OmVtdHUxMDMw"})
        @GET("portal?modo=mobile")
        Call<RepostaObterDadosPortal> obterDadosPortalApenasVeiculos(@Query("linha") String numLinha);

        @Headers({"Authorization: Basic cm9iby1lbXR1OmVtdHUxMDMw"})
        @GET("portal")
        Call<RepostaObterDadosPortal> obterDadosPortalCompleto(@Query("linha") String numLinha);

}

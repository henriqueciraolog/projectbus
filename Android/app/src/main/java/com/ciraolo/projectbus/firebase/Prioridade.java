package com.ciraolo.projectbus.firebase;

public enum Prioridade {
    URGENTE(0),
    IMPORTANTE(1),
    AVISO(2),
    NOTICIA(3),
    INFORMACAO(4);

    private final int prioridadeNum;

    Prioridade(final int prioridadeNum) {
        this.prioridadeNum = prioridadeNum;
    }

    public static Prioridade getPrioridade(int prioridadeNum) {
        switch (prioridadeNum) {
            case 0: return URGENTE;
            case 1: return IMPORTANTE;
            case 2: return AVISO;
            case 3: return NOTICIA;
            default: return INFORMACAO;
        }
    }

    @Override
    public String toString() {
        return String.valueOf(prioridadeNum);
    }
}

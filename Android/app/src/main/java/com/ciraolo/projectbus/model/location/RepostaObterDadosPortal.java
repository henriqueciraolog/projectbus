package com.ciraolo.projectbus.model.location;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class RepostaObterDadosPortal implements Serializable {

    public List<Linha> linhas = null;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
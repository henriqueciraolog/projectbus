package com.ciraolo.projectbus.urbes;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


public class DatabaseControl {
    private static final String tag = DatabaseControl.class.getSimpleName();
    private static SQLiteDatabase mDb;
    private static MainDBOpenHelper mDbHelper;

    public static SQLiteDatabase openDatabase(Context context) {
        if (mDbHelper == null) {
            mDbHelper = new MainDBOpenHelper(context, MainDBOpenHelper.DATABASE_NAME, null, MainDBOpenHelper.DATABASE_VERSION);
        }
        if (mDb == null || !mDb.isOpen()) {
            mDb = mDbHelper.getWritableDatabase();
            Log.i(tag, "A Base de Dados foi instanciada!");
        }

        return mDb;
    }

    public static void closeDatabase() {
        if (mDb != null)
            mDb.close();
        if (mDbHelper != null)
            mDbHelper.close();

        mDbHelper = null;
        mDb = null;

        Log.i(tag, "A Base de Dados foi fechada e liberada!");
    }

}



/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ciraolo.projectbus.firebase;
/*

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.ciraolo.projectbus.R;
import com.ciraolo.projectbus.activitys.MainActivity;
import com.ciraolo.projectbus.util.AuxiliarPreferencias;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    */
/**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     *//*

    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Notificacao notificacao = new Notificacao();

        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
             notificacao.prioridade = Prioridade.getPrioridade(Integer.parseInt(remoteMessage.getData().get("prioridade")));
            notificacao.titulo = remoteMessage.getData().get("titulo");
            notificacao.mensagem = remoteMessage.getData().get("mensagem");
        }

        inserirNotificacao(this, notificacao);

        if (notificacao.prioridade == Prioridade.URGENTE || notificacao.prioridade == Prioridade.IMPORTANTE) {
            sendNotification(notificacao);
        }
    }

    */
/**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param notificacao FCM message body received.
     *//*

    private void sendNotification(Notificacao notificacao) {

        String titulo = notificacao.titulo;
        if (notificacao.prioridade == Prioridade.URGENTE) titulo = "[URGENTE]" + titulo;

        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("openNotifications", true);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 123, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_directions_bus_black_24dp)
                .setContentTitle(titulo)
                .setContentText(notificacao.mensagem)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(notificacao.mensagem))
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(123, notificationBuilder.build());
    }

    public static void inserirNotificacao(Context context, Notificacao notificacao) {
        AuxiliarPreferencias auxiliarPreferencias = AuxiliarPreferencias.getInstance();
        List<Notificacao> notificacoes = new ArrayList<>();
        notificacoes.add(notificacao);
        notificacoes.addAll(Arrays.asList(auxiliarPreferencias.getObject(context, "notificacoes", Notificacao[].class)));

        auxiliarPreferencias.setObject(context, notificacoes, "notificacoes");
    }
}*/

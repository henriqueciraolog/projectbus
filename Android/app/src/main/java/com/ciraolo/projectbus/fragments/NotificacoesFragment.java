package com.ciraolo.projectbus.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ciraolo.projectbus.R;
import com.ciraolo.projectbus.adapter.NotificacaoRecyclerViewAdapter;
import com.ciraolo.projectbus.firebase.Notificacao;
import com.ciraolo.projectbus.util.AuxiliarPreferencias;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificacoesFragment extends Fragment {

    @BindView(R.id.rcvNotificacoes)
    RecyclerView rcvNotificacoes;

    @BindView(R.id.txvEmptyView)
    TextView txvEmptyView;

    List<Notificacao> dataSet;

    private NotificacaoRecyclerViewAdapter rcvAdapter;
    private Toolbar toolbar;

    public NotificacoesFragment() {
    }

    public static NotificacoesFragment newInstance() {
        NotificacoesFragment fragment = new NotificacoesFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_notificacoes, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Project Bus");

        setupRecycleView();
        obterNotificacoes();
    }

    private void setupRecycleView() {
        //ADAPTER
        dataSet = new ArrayList<>();
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        //rcvLinhas.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager rcvLayoutManager = new LinearLayoutManager(getContext());
        rcvNotificacoes.setLayoutManager(rcvLayoutManager);
        //rcvLinhas.addItemDecoration();

        //ADAPTER
        rcvAdapter = new NotificacaoRecyclerViewAdapter(getActivity(), dataSet);
        rcvNotificacoes.setAdapter(rcvAdapter);

    }

    private void obterNotificacoes() {
        AuxiliarPreferencias auxiliarPreferencias = AuxiliarPreferencias.getInstance();

        List<Notificacao> notificacaos = Arrays.asList(auxiliarPreferencias.getObject(getActivity(), "notificacoes", Notificacao[].class));
        rcvAdapter.updateList(notificacaos);
        dataSet = notificacaos;

        if (dataSet.size() == 0) {
            txvEmptyView.setVisibility(View.VISIBLE);
        } else {
            txvEmptyView.setVisibility(View.GONE);
        }
    }
}

package com.ciraolo.projectbus.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ciraolo.projectbus.R;
import com.ciraolo.projectbus.adapter.LinhaUrbesRecyclerViewAdapter;
import com.ciraolo.projectbus.urbes.LinhaUrbes;
import com.ciraolo.projectbus.urbes.LinhaUrbesDao;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class LinhasUrbesFragment extends Fragment implements LinhaUrbesRecyclerViewAdapter.OnItemClickListerner, SearchView.OnQueryTextListener{

    @BindView(R.id.rcvLinhas)
    RecyclerView rcvLinhas;

    @BindView(R.id.txvEmptyView)
    TextView txvEmptyView;

    View contentView;

    List<LinhaUrbes> dataSet;

    private LinhaUrbesRecyclerViewAdapter rcvAdapter;

    public LinhasUrbesFragment() {
    }

    public static LinhasUrbesFragment newInstance() {
        LinhasUrbesFragment fragment = new LinhasUrbesFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_listar_linhas, container, false);
        ButterKnife.bind(this, view);
        contentView = view;

        return view;
    }

    private void setupRecycleView() {
        //ADAPTER
        dataSet = new ArrayList<>();
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        //rcvLinhas.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager rcvLayoutManager = new LinearLayoutManager(getContext());
        rcvLinhas.setLayoutManager(rcvLayoutManager);
        //rcvLinhas.addItemDecoration(new DividerItemDecoration(getContext()));

        //ADAPTER
        rcvAdapter = new LinhaUrbesRecyclerViewAdapter(dataSet, this);
        rcvLinhas.setAdapter(rcvAdapter);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupRecycleView();
        obterLinhas();
    }

    private void obterLinhas() {
        LinhaUrbesDao lud = new LinhaUrbesDao(getActivity());
        dataSet = lud.selectAll();

        rcvAdapter.updateList(dataSet);

        if (dataSet.size() == 0) {
            txvEmptyView.setVisibility(View.VISIBLE);
        } else {
            txvEmptyView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onItemClick(LinhaUrbes linha) {
        /*Intent intent = new Intent(getContext(), LinhaDetalheActivity.class);
        Bundle extras = new Bundle();
        extras.putSerializable("linha", linha);
        intent.putExtras(extras);
        startActivity(intent);*/
    }


    @Override
    public boolean onQueryTextChange(String newText) {
        final List<LinhaUrbes> filteredModelList = filter(dataSet, newText);

        rcvAdapter.updateList(filteredModelList);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    private List<LinhaUrbes> filter(List<LinhaUrbes> models, String query) {
        query = query.toUpperCase();
        final List<LinhaUrbes> filteredModelList = new ArrayList<>();
        for (LinhaUrbes model : models) {

            if (model.linDescricao.contains(query) || model.linNumero.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }
}

package com.ciraolo.projectbus.model.location;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class Veiculo implements Serializable {

    public Integer version;
    public String consorcio;
    public String empresa;
    public String prefixo;
    public String status;
    public String codigoLinha;
    public String sentidoLinha;
    public Long idRota;
    public Long dataUltimaTransmissao;
    public Double latitude;
    public Double longitude;
    public Integer seqPonto;
    public String acessibilidade;
    public String tipoOnibus;
    public Integer capacidadeSentados;
    public Integer capacidadeEmPe;
    public String fabricante;
    public String modelo;
    public String ano;
    public Long dataAutorizacao;
    public Long dataFimAutorizacao;
    public Boolean alertaNaoTransmitiuCiclo;
    public Boolean dentroRota;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
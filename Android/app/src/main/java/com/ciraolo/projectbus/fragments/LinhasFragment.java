package com.ciraolo.projectbus.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ciraolo.projectbus.R;
import com.ciraolo.projectbus.activitys.LinhaDetalheActivity;
import com.ciraolo.projectbus.activitys.MainActivity;
import com.ciraolo.projectbus.adapter.LinhaRecyclerViewAdapter;
import com.ciraolo.projectbus.model.information.Linha;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class LinhasFragment extends Fragment implements LinhaRecyclerViewAdapter.OnItemClickListerner, SearchView.OnQueryTextListener{

    @BindView(R.id.rcvLinhas)
    RecyclerView rcvLinhas;

    @BindView(R.id.txvEmptyView)
    TextView txvEmptyView;

    View contentView;

    List<Linha> dataSet;

    private LinhaRecyclerViewAdapter rcvAdapter;

    public LinhasFragment() {
    }

    public static LinhasFragment newInstance() {
        LinhasFragment fragment = new LinhasFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_listar_linhas, container, false);
        ButterKnife.bind(this, view);
        contentView = view;

        return view;
    }

    private void setupRecycleView() {
        //ADAPTER
        dataSet = new ArrayList<>();
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        //rcvLinhas.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager rcvLayoutManager = new LinearLayoutManager(getContext());
        rcvLinhas.setLayoutManager(rcvLayoutManager);
        //rcvLinhas.addItemDecoration();

        //ADAPTER
        rcvAdapter = new LinhaRecyclerViewAdapter(dataSet, this);
        rcvLinhas.setAdapter(rcvAdapter);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rcvLinhas.getContext(), LinearLayout.VERTICAL);
        rcvLinhas.addItemDecoration(dividerItemDecoration);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupRecycleView();
        obterLinhas();
    }

    private void obterLinhas() {
        rcvAdapter.updateList(MainActivity.LINHAS_METROPOLITANAS);
        dataSet = MainActivity.LINHAS_METROPOLITANAS;

        if (dataSet.size() == 0) {
            txvEmptyView.setVisibility(View.VISIBLE);
        } else {
            txvEmptyView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onItemClick(Linha linha) {
        Intent intent = new Intent(getContext(), LinhaDetalheActivity.class);
        Bundle extras = new Bundle();
        extras.putSerializable("linha", linha);
        intent.putExtras(extras);
        startActivity(intent);
    }


    @Override
    public boolean onQueryTextChange(String newText) {
        final List<Linha> filteredModelList = filter(dataSet, newText);

        rcvAdapter.updateList(filteredModelList);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    private List<Linha> filter(List<Linha> models, String query) {
        query = query.toUpperCase();
        final List<Linha> filteredModelList = new ArrayList<>();
        for (Linha model : models) {

            if (model.origem.contains(query) || model.destino.contains(query) || model.codigo.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }
}

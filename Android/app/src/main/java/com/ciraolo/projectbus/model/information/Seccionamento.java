package com.ciraolo.projectbus.model.information;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Seccionamento {

    public Integer nrosecta;
    public String trecho;
    public Double valor;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
package com.ciraolo.projectbus.activitys;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ciraolo.projectbus.R;
import com.ciraolo.projectbus.firebase.Notificacao;
import com.ciraolo.projectbus.urbes.DBFile;
import com.ciraolo.projectbus.urbes.LinhaUrbes;
import com.ciraolo.projectbus.urbes.LinhaUrbesDao;
import com.ciraolo.projectbus.util.AuxiliarPreferencias;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

/**
 * Created by henriqueciraolo on 07/05/17.
 */

public class LauncherActivity extends AppCompatActivity {
    private final String tag = LauncherActivity.class.getSimpleName();

    private boolean isExecutando;
    private boolean isTempoEncerrado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            setContentView(R.layout.activity_splash);

            isTempoEncerrado = true;

            //new LauncherTask().execute();

            new CountDownTimer(1500, 1500) {
                @Override
                public void onTick(long millisUntilFinished) {
                }

                @Override
                public void onFinish() {
                    try {
                        if (isTempoEncerrado) {
                            goToNextScreen();
                        } else {
                            isTempoEncerrado = true;
                            ProgressBar pbLoading = (ProgressBar) findViewById(R.id.pbLoading);
                            pbLoading.setVisibility(View.VISIBLE);
                            TextView txvLoading = (TextView) findViewById(R.id.txvLoading);
                            txvLoading.setVisibility(View.VISIBLE);
                            pbLoading.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(LauncherActivity.this, R.color.verde_urbes), PorterDuff.Mode.SRC_IN);
                        }
                    } catch (Exception e) {
                        Log.i(tag, "", e);
                    }
                }
            }.start();
        } catch (Exception e) {
            Log.i(tag, "", e);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            isExecutando = true;
        } catch (Exception e) {
            Log.i(tag, "", e);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            isExecutando = true;
        } catch (Exception e) {
            Log.i(tag, "", e);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        isExecutando = false;
    }

    @Override
    protected void onStop() {
        super.onStop();

        try {
            isExecutando = false;
        } catch (Exception e) {
            Log.i(tag, "", e);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            isExecutando = false;
        } catch (Exception e) {
            Log.i(tag, "", e);
        }
    }

    @Override
    public void onBackPressed() {
    }

    private void goToNextScreen() {
        if (isExecutando) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Intent intent = new Intent(LauncherActivity.this, MainActivity.class);
                        startActivity(intent);
                    } catch (Exception e) {
                        Log.i(tag, "", e);
                    }
                }
            });
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    finish();
                }
            });
        }
    }

    class LauncherTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Void doInBackground(Void... params) {
            Thread.currentThread().setName(this.getClass().getSimpleName());
            try {
                inicializationTask();
            } catch (Exception e) {
                Log.i(tag, "", e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void voids) {
            try {
                if (isTempoEncerrado) {
                    goToNextScreen();
                } else {
                    isTempoEncerrado = true;
                }
            } catch (Exception e) {
                Log.i(tag, "", e);
            }
        }

        public void inicializationTask() {
            try {
                FirebaseMessaging.getInstance().subscribeToTopic("notificacao");

                AuxiliarPreferencias auxiliarPreferencias = AuxiliarPreferencias.getInstance();
                Notificacao[] notificacoes = auxiliarPreferencias.getObject(LauncherActivity.this, "notificacoes", Notificacao[].class);
                if (notificacoes == null)
                    auxiliarPreferencias.setObject(LauncherActivity.this, new ArrayList<Notificacao>(), "notificacoes");

                LinhaUrbesDao lud = new LinhaUrbesDao(LauncherActivity.this);
                List<LinhaUrbes> listLinhaUrbes = lud.selectAll();
                Log.e(tag, new Gson().toJson(listLinhaUrbes));

                DBFile dbfile = new DBFile(LauncherActivity.this);
                dbfile.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                dbfile.get();

                listLinhaUrbes = lud.selectAll();
                LinhaUrbes linha81 = new LinhaUrbes();
                linha81.linNumero = "81";
                int index = Collections.binarySearch(listLinhaUrbes, linha81, new Comparator<LinhaUrbes>() {
                    @Override
                    public int compare(LinhaUrbes lu1, LinhaUrbes lu2) {
                        return lu1.linNumero.equals(lu2.linNumero) ? 0 : -1;
                    }
                });
                Log.e(tag, new Gson().toJson(listLinhaUrbes.get(index)));
            } catch (Exception e) {
                Log.i(tag, "", e);
            }
        }
    }
}

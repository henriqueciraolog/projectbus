package com.ciraolo.projectbus.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Modifier;

/**
 * Created by guest on 21/07/2016.
 */
public class AuxiliarPreferencias {
    private static AuxiliarPreferencias instance;

    private Gson gson;

    private AuxiliarPreferencias() {
    }

    public static AuxiliarPreferencias getInstance() {
        if (instance == null) {
            instance = new AuxiliarPreferencias();
        }

        return instance;
    }

    private SharedPreferences getGCMPreferences(Context context) {
        return (context.getSharedPreferences("com.ciraolo.onibusteste", Context.MODE_PRIVATE));
    }

    private Gson myGson() {
        if (gson == null) {
            gson = new GsonBuilder()
                    .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                    .serializeNulls()
                    .create();
        }

        return gson;
    }

    public <T> void setObject(Context context, T t, String key) {
        String json = myGson().toJson(t);
        SharedPreferences prefs = getGCMPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, json);
        editor.apply();
    }

    public void setBoolean(Context context, String key, boolean value) {
        SharedPreferences prefs = getGCMPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public boolean getBoolean(Context context, String key) {
        SharedPreferences prefs = getGCMPreferences(context);
        return prefs.getBoolean(key, false);
    }

    public void setString(Context context, String key, String value) {
        SharedPreferences prefs = getGCMPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public String getString(Context context, String key) {
        SharedPreferences prefs = getGCMPreferences(context);
        return prefs.getString(key, null);
    }

    public void setLong(Context context, String key, long value) {
        SharedPreferences prefs = getGCMPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    public long getLong(Context context, String key) {
        SharedPreferences prefs = getGCMPreferences(context);
        return prefs.getLong(key, 0);
    }

    public <T> T getObject(Context context, String key, Class<T> myClass) {

        SharedPreferences prefs = getGCMPreferences(context);
        String json = prefs.getString(key, "");

        if (json.equals("")) {
            return null;
        } else {

            try {
                return myGson().fromJson(json, myClass);
            } catch (Exception e) {
                return null;
            }

        }

    }

    public String getStringObject(Context context, String key) {
        SharedPreferences prefs = getGCMPreferences(context);
        String json = prefs.getString(key, "");

        if (json.equals("")) {
            return "";
        } else {
            return json;
        }
    }

    public void deleteObject(Context context, String key) {
        SharedPreferences prefs = getGCMPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(key);
        editor.apply();
    }
}

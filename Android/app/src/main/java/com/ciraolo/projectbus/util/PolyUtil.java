package com.ciraolo.projectbus.util;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds.Builder;
import java.util.ArrayList;
import java.util.List;

public class PolyUtil {
    private static final double DEFAULT_TOLERANCE = 1.0d;
    static final double EARTH_RADIUS = 6371009.0d;

    private PolyUtil() {
    }

    public static double distFrom(double lat1, double lng1, double lat2, double lng2) {
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.pow(Math.sin(dLat / 2.0d), 2.0d) + ((Math.pow(Math.sin(dLng / 2.0d), 2.0d) * Math.cos(Math.toRadians(lat1))) * Math.cos(Math.toRadians(lat2)));
        return 6371.0d * (2.0d * Math.atan2(Math.sqrt(a), Math.sqrt(DEFAULT_TOLERANCE - a)));
    }

    public static void fixZoom(GoogleMap map, List<LatLng> p) {
        if (p.size() > 0) {
            Builder bc = new Builder();
            for (LatLng item : p) {
                bc.include(item);
            }
            map.moveCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 80));
        }
    }

    static double computeAngleBetween(LatLng from, LatLng to) {
        double fromLat = Math.toRadians(from.latitude);
        double fromLng = Math.toRadians(from.longitude);
        double toLat = Math.toRadians(to.latitude);
        return 2.0d * Math.asin(Math.sqrt(Math.pow(Math.sin((fromLat - toLat) / 2.0d), 2.0d) + ((Math.cos(fromLat) * Math.cos(toLat)) * Math.pow(Math.sin((fromLng - Math.toRadians(to.longitude)) / 2.0d), 2.0d))));
    }

    public static double computeDistanceBetween(LatLng from, LatLng to) {
        return computeAngleBetween(from, to) * 6378137.0d;
    }

    public static double computeLength(List<LatLng> path) {
        double length = 0.0d;
        for (int i = 0; i < path.size() - 1; i++) {
            length += computeDistanceBetween((LatLng) path.get(i), (LatLng) path.get(i + 1));
        }
        return length;
    }

    public static double round(double value, int places) {
        if (places < 0) {
            throw new IllegalArgumentException();
        }
        long factor = (long) Math.pow(10.0d, (double) places);
        return ((double) Math.round(value * ((double) factor))) / ((double) factor);
    }

    private static double mercator(double lat) {
        return Math.log(Math.tan((0.5d * lat) + 0.7853981633974483d));
    }

    private static double inverseMercator(double y) {
        return (2.0d * Math.atan(Math.exp(y))) - 1.5707963267948966d;
    }

    private static double hav(double x) {
        double sinHalf = Math.sin(0.5d * x);
        return sinHalf * sinHalf;
    }

    private static double havDistance(double lat1, double lat2, double dLng) {
        return hav(lat1 - lat2) + ((hav(dLng) * Math.cos(lat1)) * Math.cos(lat2));
    }

    private static double clamp(double x, double low, double high) {
        if (x < low) {
            return low;
        }
        return x > high ? high : x;
    }

    private static double sinFromHav(double h) {
        return 2.0d * Math.sqrt((DEFAULT_TOLERANCE - h) * h);
    }

    private static double havFromSin(double x) {
        double x2 = x * x;
        return (x2 / (Math.sqrt(DEFAULT_TOLERANCE - x2) + DEFAULT_TOLERANCE)) * 0.5d;
    }

    private static double sinSumFromHav(double x, double y) {
        double a = Math.sqrt((DEFAULT_TOLERANCE - x) * x);
        double b = Math.sqrt((DEFAULT_TOLERANCE - y) * y);
        return ((a + b) - (((a * y) + (b * x)) * 2.0d)) * 2.0d;
    }

    public static double computeHeading(LatLng from, LatLng to) {
        double fromLat = Math.toRadians(from.latitude);
        double fromLng = Math.toRadians(from.longitude);
        double toLat = Math.toRadians(to.latitude);
        double dLng = Math.toRadians(to.longitude) - fromLng;
        return wrap(Math.toDegrees(Math.atan2(Math.sin(dLng) * Math.cos(toLat), (Math.cos(fromLat) * Math.sin(toLat)) - ((Math.sin(fromLat) * Math.cos(toLat)) * Math.cos(dLng)))), -180.0d, 180.0d);
    }

    private static double sinDeltaBearing(double lat1, double lng1, double lat2, double lng2, double lat3, double lng3) {
        double sinLat1 = Math.sin(lat1);
        double cosLat2 = Math.cos(lat2);
        double cosLat3 = Math.cos(lat3);
        double lng31 = lng3 - lng1;
        double lng21 = lng2 - lng1;
        double a = Math.sin(lng31) * cosLat3;
        double c = Math.sin(lng21) * cosLat2;
        double b = Math.sin(lat3 - lat1) + (((2.0d * sinLat1) * cosLat3) * hav(lng31));
        double d = Math.sin(lat2 - lat1) + (((2.0d * sinLat1) * cosLat2) * hav(lng21));
        double denom = ((a * a) + (b * b)) * ((c * c) + (d * d));
        return denom <= 0.0d ? DEFAULT_TOLERANCE : ((a * d) - (b * c)) / Math.sqrt(denom);
    }

    private static double tanLatGC(double lat1, double lat2, double lng2, double lng3) {
        return ((Math.tan(lat1) * Math.sin(lng2 - lng3)) + (Math.tan(lat2) * Math.sin(lng3))) / Math.sin(lng2);
    }

    private static double mercatorLatRhumb(double lat1, double lat2, double lng2, double lng3) {
        return ((mercator(lat1) * (lng2 - lng3)) + (mercator(lat2) * lng3)) / lng2;
    }

    private static boolean intersects(double lat1, double lat2, double lng2, double lat3, double lng3, boolean geodesic) {
        if ((lng3 >= 0.0d && lng3 >= lng2) || (lng3 < 0.0d && lng3 < lng2)) {
            return false;
        }
        if (lat3 <= -1.5707963267948966d) {
            return false;
        }
        if (lat1 <= -1.5707963267948966d || lat2 <= -1.5707963267948966d || lat1 >= 1.5707963267948966d || lat2 >= 1.5707963267948966d) {
            return false;
        }
        if (lng2 <= -3.141592653589793d) {
            return false;
        }
        double linearLat = (((lng2 - lng3) * lat1) + (lat2 * lng3)) / lng2;
        if (lat1 >= 0.0d && lat2 >= 0.0d && lat3 < linearLat) {
            return false;
        }
        if (lat1 <= 0.0d && lat2 <= 0.0d && lat3 >= linearLat) {
            return true;
        }
        if (lat3 >= 1.5707963267948966d) {
            return true;
        }
        return geodesic ? Math.tan(lat3) >= tanLatGC(lat1, lat2, lng2, lng3) : mercator(lat3) >= mercatorLatRhumb(lat1, lat2, lng2, lng3);
    }

    public static boolean containsLocation(LatLng point, List<LatLng> polygon, boolean geodesic) {
        int size = polygon.size();
        if (size == 0) {
            return false;
        }
        double lat3 = Math.toRadians(point.latitude);
        double lng3 = Math.toRadians(point.longitude);
        LatLng prev = (LatLng) polygon.get(size - 1);
        double lat1 = Math.toRadians(prev.latitude);
        double lng1 = Math.toRadians(prev.longitude);
        int nIntersect = 0;
        for (LatLng point2 : polygon) {
            double dLng3 = wrap(lng3 - lng1, -3.141592653589793d, 3.141592653589793d);
            if (lat3 == lat1 && dLng3 == 0.0d) {
                return true;
            }
            double lat2 = Math.toRadians(point2.latitude);
            double lng2 = Math.toRadians(point2.longitude);
            if (intersects(lat1, lat2, wrap(lng2 - lng1, -3.141592653589793d, 3.141592653589793d), lat3, dLng3, geodesic)) {
                nIntersect++;
            }
            lat1 = lat2;
            lng1 = lng2;
        }
        if ((nIntersect & 1) != 0) {
            return true;
        }
        return false;
    }

    public static boolean isLocationOnEdge(LatLng point, List<LatLng> polygon, boolean geodesic, double tolerance) {
        return isLocationOnEdgeOrPath(point, polygon, true, geodesic, tolerance);
    }

    public static boolean isLocationOnEdge(LatLng point, List<LatLng> polygon, boolean geodesic) {
        return isLocationOnEdge(point, polygon, geodesic, DEFAULT_TOLERANCE);
    }

    public static boolean isLocationOnPath(LatLng point, List<LatLng> polyline, boolean geodesic, double tolerance) {
        return isLocationOnEdgeOrPath(point, polyline, false, geodesic, tolerance);
    }

    public static boolean isLocationOnPath(LatLng point, List<LatLng> polyline, boolean geodesic) {
        return isLocationOnPath(point, polyline, geodesic, DEFAULT_TOLERANCE);
    }

    private static boolean isLocationOnEdgeOrPath(LatLng point, List<LatLng> poly, boolean closed, boolean geodesic, double toleranceEarth) {
        int size = poly.size();
        if (size == 0) {
            return false;
        }
        double tolerance = toleranceEarth / EARTH_RADIUS;
        double havTolerance = hav(tolerance);
        double lat3 = Math.toRadians(point.latitude);
        double lng3 = Math.toRadians(point.longitude);
        LatLng prev = (LatLng) poly.get(closed ? size - 1 : 0);
        double lat1 = Math.toRadians(prev.latitude);
        double lng1 = Math.toRadians(prev.longitude);
        double lat2;
        double lng2;
        if (geodesic) {
            for (LatLng point2 : poly) {
                lat2 = Math.toRadians(point2.latitude);
                lng2 = Math.toRadians(point2.longitude);
                if (isOnSegmentGC(lat1, lng1, lat2, lng2, lat3, lng3, havTolerance)) {
                    return true;
                }
                lat1 = lat2;
                lng1 = lng2;
            }
        } else {
            double minAcceptable = lat3 - tolerance;
            double maxAcceptable = lat3 + tolerance;
            double y1 = mercator(lat1);
            double y3 = mercator(lat3);
            double[] xTry = new double[3];
            for (LatLng point22 : poly) {
                lat2 = Math.toRadians(point22.latitude);
                double y2 = mercator(lat2);
                lng2 = Math.toRadians(point22.longitude);
                if (Math.max(lat1, lat2) >= minAcceptable && Math.min(lat1, lat2) <= maxAcceptable) {
                    double x2 = wrap(lng2 - lng1, -3.141592653589793d, 3.141592653589793d);
                    double x3Base = wrap(lng3 - lng1, -3.141592653589793d, 3.141592653589793d);
                    xTry[0] = x3Base;
                    xTry[1] = 6.283185307179586d + x3Base;
                    xTry[2] = x3Base - 6.283185307179586d;
                    for (double x3 : xTry) {
                        double dy = y2 - y1;
                        double len2 = (x2 * x2) + (dy * dy);
                        double t = len2 <= 0.0d ? 0.0d : clamp(((x3 * x2) + ((y3 - y1) * dy)) / len2, 0.0d, DEFAULT_TOLERANCE);
                        if (havDistance(lat3, inverseMercator(y1 + (t * dy)), x3 - (t * x2)) < havTolerance) {
                            return true;
                        }
                    }
                    continue;
                }
                lat1 = lat2;
                lng1 = lng2;
                y1 = y2;
            }
        }
        return false;
    }

    private static boolean isOnSegmentGC(double lat1, double lng1, double lat2, double lng2, double lat3, double lng3, double havTolerance) {
        double havDist13 = havDistance(lat1, lat3, lng1 - lng3);
        if (havDist13 <= havTolerance) {
            return true;
        }
        double havDist23 = havDistance(lat2, lat3, lng2 - lng3);
        if (havDist23 <= havTolerance) {
            return true;
        }
        double havCrossTrack = havFromSin(sinFromHav(havDist13) * sinDeltaBearing(lat1, lng1, lat2, lng2, lat3, lng3));
        if (havCrossTrack > havTolerance) {
            return false;
        }
        double havDist12 = havDistance(lat1, lat2, lng1 - lng2);
        double term = havDist12 + ((DEFAULT_TOLERANCE - (2.0d * havDist12)) * havCrossTrack);
        if (havDist13 > term || havDist23 > term) {
            return false;
        }
        if (havDist12 < 0.74d) {
            return true;
        }
        double cosCrossTrack = DEFAULT_TOLERANCE - (2.0d * havCrossTrack);
        return sinSumFromHav((havDist13 - havCrossTrack) / cosCrossTrack, (havDist23 - havCrossTrack) / cosCrossTrack) > 0.0d;
    }

    public static List<LatLng> decode(String encodedPath) {
        int len = encodedPath.length();
        List<LatLng> path = new ArrayList();
        int index = 0;
        int lat = 0;
        int lng = 0;
        int pointIndex = 0;
        while (index < len) {
            int index2;
            int result = 1;
            int shift = 0;
            while (true) {
                index2 = index + 1;
                int b = (encodedPath.charAt(index) - 63) - 1;
                result += b << shift;
                shift += 5;
                if (b < 31) {
                    break;
                }
                index = index2;
            }
            lat += (result & 1) != 0 ? (result >> 1) ^ -1 : result >> 1;
            result = 1;
            shift = 0;
            index = index2;
            while (true) {
                index2 = index + 1;
                int b = (encodedPath.charAt(index) - 63) - 1;
                result += b << shift;
                shift += 5;
                if (b < 31) {
                    break;
                }
                index = index2;
            }
            lng += (result & 1) != 0 ? (result >> 1) ^ -1 : result >> 1;
            path.add(new LatLng(((double) lat) * 1.0E-5d, ((double) lng) * 1.0E-5d));
            pointIndex++;
            index = index2;
        }
        return path;
    }

    public static String encode(List<LatLng> path) {
        long lastLat = 0;
        long lastLng = 0;
        StringBuffer result = new StringBuffer();
        for (LatLng point : path) {
            long lat = Math.round(point.latitude * 100000.0d);
            long lng = Math.round(point.longitude * 100000.0d);
            long dLng = lng - lastLng;
            encode(lat - lastLat, result);
            encode(dLng, result);
            lastLat = lat;
            lastLng = lng;
        }
        return result.toString();
    }

    private static void encode(long v, StringBuffer result) {
        v = v < 0 ? (v << 1) ^ -1 : v << 1;
        while (v >= 32) {
            result.append(Character.toChars((int) (((31 & v) | 32) + 63)));
            v >>= 5;
        }
        result.append(Character.toChars((int) (v + 63)));
    }

    static double wrap(double n, double min, double max) {
        return (n < min || n >= max) ? mod(n - min, max - min) + min : n;
    }

    static double mod(double x, double m) {
        return ((x % m) + m) % m;
    }
}

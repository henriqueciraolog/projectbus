package com.ciraolo.projectbus.adapter;

//================================================================================
// IMPORTS
//================================================================================

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ciraolo.projectbus.R;
import com.ciraolo.projectbus.urbes.LinhaUrbes;

import java.util.ArrayList;
import java.util.List;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class LinhaUrbesRecyclerViewAdapter extends RecyclerView.Adapter<LinhaUrbesRecyclerViewAdapter.ViewHolder> {

    //================================================================================
    // PROPERTIES
    //================================================================================
    private List<LinhaUrbes> mDataset;
    private final OnItemClickListerner mListerner;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {

        //PROPERTIES
        @BindView(R.id.lnlLinha)
        CardView lnlLinha;
        @BindView(R.id.txvCodLinha)
        TextView txvCodLinha;
        @BindView(R.id.txvOrigemLinha)
        TextView txvOrigemLinha;
        @BindView(R.id.txvDestinoLinha)
        TextView txvDestinoLinha;

        ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    //================================================================================
    // CONSTRUCTOR
    //================================================================================
    // Provide a suitable constructor (depends on the kind of dataset)
    public LinhaUrbesRecyclerViewAdapter(List<LinhaUrbes> myDataset, OnItemClickListerner listerner) {
        mDataset = myDataset;
        mListerner = listerner;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_view_linha_urbes, parent, false);

        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        LinhaUrbes linha = mDataset.get(position);

        holder.lnlLinha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListerner != null) mListerner.onItemClick(mDataset.get(position));
            }
        });
        String linhaNumero = linha.linNumero;
        try {
            int num = Integer.parseInt(linhaNumero);
            if (num >= 1 && num <= 9) {
                linhaNumero = String.format("%02d", num);
            }
        } catch (Exception ignore) {

        }
        holder.txvCodLinha.setText(linhaNumero);
        holder.txvOrigemLinha.setText(linha.linDescricao.replace("/", "\n"));
        holder.txvDestinoLinha.setVisibility(View.GONE);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void updateList(List<LinhaUrbes> newlist) {
        mDataset = new ArrayList<>();
        mDataset.addAll(newlist);
        notifyDataSetChanged();
    }

    public interface OnItemClickListerner {
        void onItemClick(LinhaUrbes linha);
    }
}




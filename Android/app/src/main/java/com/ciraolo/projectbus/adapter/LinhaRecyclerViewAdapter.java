package com.ciraolo.projectbus.adapter;

//================================================================================
// IMPORTS
//================================================================================

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ciraolo.projectbus.R;
import com.ciraolo.projectbus.model.information.Linha;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class LinhaRecyclerViewAdapter extends RecyclerView.Adapter<LinhaRecyclerViewAdapter.ViewHolder> {

    //================================================================================
    // PROPERTIES
    //================================================================================
    private List<Linha> mDataset;
    private final OnItemClickListerner mListerner;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {

        //PROPERTIES
        @BindView(R.id.lnlLinha)
        View lnlLinha;
        @BindView(R.id.txvCodLinha)
        TextView txvCodLinha;
        @BindView(R.id.txvServComp)
        TextView txvServComp;
        @BindView(R.id.txvOrigemLinha)
        TextView txvOrigemLinha;
        @BindView(R.id.txvDestinoLinha)
        TextView txvDestinoLinha;

        ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    //================================================================================
    // CONSTRUCTOR
    //================================================================================
    // Provide a suitable constructor (depends on the kind of dataset)
    public LinhaRecyclerViewAdapter(List<Linha> myDataset, OnItemClickListerner listerner) {
        mDataset = myDataset;
        mListerner = listerner;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_view_linha, parent, false);

        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Linha linha = mDataset.get(position);

        holder.lnlLinha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListerner != null) mListerner.onItemClick(mDataset.get(position));
            }
        });
        String codLinha = linha.codigo;
        String servComp = "";
        if (codLinha.length() > 4) {
            servComp = codLinha.substring(4);
            codLinha = codLinha.substring(0, 4);
        }

        holder.txvCodLinha.setText(codLinha);
        holder.txvServComp.setText(servComp);
        if (servComp.length() > 0) holder.txvServComp.setVisibility(View.VISIBLE);
        else holder.txvServComp.setVisibility(View.GONE);
        holder.txvOrigemLinha.setText(linha.origem.replaceAll("TERMINAL", "TERM.").replaceAll("RODOVIARIO", "RODOV."));
        holder.txvOrigemLinha.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        holder.txvOrigemLinha.setSingleLine(true);
        holder.txvOrigemLinha.setMarqueeRepeatLimit(5);
        holder.txvOrigemLinha.setSelected(true);
        holder.txvDestinoLinha.setText(linha.destino.replaceAll("TERMINAL", "TERM.").replaceAll("RODOVIARIO", "RODOV."));
        holder.txvDestinoLinha.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        holder.txvDestinoLinha.setSingleLine(true);
        holder.txvDestinoLinha.setMarqueeRepeatLimit(5);
        holder.txvDestinoLinha.setSelected(true);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void updateList(List<Linha> newlist) {
        mDataset = new ArrayList<>();
        mDataset.addAll(newlist);
        notifyDataSetChanged();
    }

    public interface OnItemClickListerner {
        void onItemClick(Linha linha);
    }
}




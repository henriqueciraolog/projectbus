package com.ciraolo.projectbus.model.location;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Linha implements Serializable {

    public Long id;
    public String codigo;
    public Object tp;
    public Object ts;
    public List<Rota> rotas = null;
    public List<Veiculo> veiculos = null;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
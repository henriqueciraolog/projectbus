package com.ciraolo.projectbus.adapter;

//================================================================================
// IMPORTS
//================================================================================

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ciraolo.projectbus.R;
import com.ciraolo.projectbus.model.information.Seccionamento;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SeccionamentoRecyclerViewAdapter extends RecyclerView.Adapter<SeccionamentoRecyclerViewAdapter.ViewHolder> {

    //================================================================================
    // PROPERTIES
    //================================================================================
    private List<Seccionamento> mDataset;
    private Context mContext;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    static class ViewHolder extends RecyclerView.ViewHolder {

        //PROPERTIES
        @BindView(R.id.txvSeccionamentoInicio)
        TextView txvSeccionamentoInicio;
        @BindView(R.id.txvSeccionamentoFim)
        TextView txvSeccionamentoFim;
        @BindView(R.id.txvSeccionamentoTarifa)
        TextView txvSeccionamentoTarifa;

        ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    //================================================================================
    // CONSTRUCTOR
    //================================================================================
    // Provide a suitable constructor (depends on the kind of dataset)
    public SeccionamentoRecyclerViewAdapter(List<Seccionamento> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_view_seccionamento, parent, false);
        mContext = parent.getContext();

        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Seccionamento seccionamento = mDataset.get(position);

        String[] trechos = seccionamento.trecho.split("-");

        holder.txvSeccionamentoInicio.setText(trechos[0].trim());
        holder.txvSeccionamentoFim.setText(trechos[1].trim());
        holder.txvSeccionamentoTarifa.setText(mContext.getString(R.string.tarifa,seccionamento.valor));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void updateList(List<Seccionamento> newlist) {
        mDataset.clear();
        mDataset.addAll(newlist);
        this.notifyDataSetChanged();
    }
}




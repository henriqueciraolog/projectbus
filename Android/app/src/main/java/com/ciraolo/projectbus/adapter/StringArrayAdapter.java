package com.ciraolo.projectbus.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ciraolo.projectbus.R;

import java.util.Calendar;
import java.util.List;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class StringArrayAdapter extends RecyclerView.Adapter<StringArrayAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private Context context;
    List<String> strings;

    public StringArrayAdapter(Context context, List<String> strings) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.strings = strings;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.adapter_view_horario_emtu, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        String horario = strings.get(position);
        if (horario.length() == 4) horario = "0" + horario;

        holder.txtHorario.setText(horario);

        try {
            String[] split = strings.get(position).split(":");
            Calendar c = Calendar.getInstance();

            int tempoAtual = (c.get(Calendar.HOUR_OF_DAY) * 60) + c.get(Calendar.MINUTE);
            int tempoPos = (Integer.parseInt(split[0]) * 60) + Integer.parseInt(split[1]);

            if (tempoAtual <= tempoPos) {
                if (position != 0) {
                    split = strings.get(position - 1).split(":");
                    int tempoAntPos = (Integer.parseInt(split[0]) * 60) + Integer.parseInt(split[1]);

                    if (tempoAtual >= tempoAntPos) {
                        holder.txtHorario.setTextColor(ContextCompat.getColor(context, R.color.azul_emtu));
                        holder.txtHorario.setTypeface(Typeface.DEFAULT_BOLD);
                    } else {
                        holder.txtHorario.setTextColor(ContextCompat.getColor(context, android.R.color.black));
                        holder.txtHorario.setTypeface(Typeface.DEFAULT);
                    }
                } else {
                    holder.txtHorario.setTextColor(ContextCompat.getColor(context, R.color.azul_emtu));
                    holder.txtHorario.setTypeface(Typeface.DEFAULT_BOLD);
                }
            } else {
                holder.txtHorario.setTextColor(ContextCompat.getColor(context, android.R.color.black));
                holder.txtHorario.setTypeface(Typeface.DEFAULT);
            }
        } catch (Exception ignore) {
            holder.txtHorario.setTextColor(ContextCompat.getColor(context, android.R.color.black));
            holder.txtHorario.setTypeface(Typeface.DEFAULT);
        }
    }

    @Override
    public int getItemCount() {
        return strings.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtHorario)
        TextView txtHorario;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
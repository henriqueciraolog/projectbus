package com.ciraolo.projectbus.urbes;

/**
 * Created by henriqueciraolo on 07/05/17.
 */

public class LinhaUrbes {
    public int linCod;
    public String linNumero;
    public String linDescricao;
    public String linFavorita;
    public int tarCod;
    public int linOrdemApres;
    public String linSiglaBairro;
    public String linSiglaTerminal;
}

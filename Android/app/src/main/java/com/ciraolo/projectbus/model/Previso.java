package com.ciraolo.projectbus.model;

public class Previso {

    public String codigo;
    public String nome;
    public String previsao;
    public Integer minutos;
    public Boolean aproximando;
    public Boolean noLocal;

}
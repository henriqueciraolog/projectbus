package com.ciraolo.projectbus.urbes;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Message;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationCompat.Builder;
import android.util.Log;

import com.ciraolo.projectbus.R;
import com.ciraolo.projectbus.activitys.MainActivity;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.zip.ZipInputStream;

import org.json.JSONObject;

public class DBFile extends AsyncTask<Void, Void, String> {
    public static boolean updatingVersion;
    private Context context;
    public byte[] data;
    private Handler onFinishHandler;
    private Handler onMessageHandler;

    static {
        updatingVersion = false;
    }

    public DBFile(Context context) {
        this.context = context;
    }

    public DBFile(Context context, Handler onMessageHandler, Handler onFinishHandler) {
        this(context);
        this.onFinishHandler = onFinishHandler;
        this.onMessageHandler = onMessageHandler;
    }

    private void sendMessage(String menssagem) {
        if (this.onMessageHandler != null) {
            Message m = new Message();
            m.obj = menssagem;
            this.onMessageHandler.sendMessage(m);
        }
    }

    protected String doInBackground(Void... params) {
        if (skipInterval()) {
            return "";
        }
        if (updatingVersion) {
            Log.i("F3-URBES", "SKIP UPDATE RUNNING...");
            return "";
        }
        updatingVersion = true;
        String str = "";
        try {
            sendMessage("Verificando versão...");
            str = getLocalVersion();
            String latestVersion = getLatestVersion();
            String localVersion = str;
            Log.i("URBES", "REMOTE VERSION IS " + latestVersion);
            Log.i("URBES", "LOCAL VERSION IS " + localVersion);
            if (latestVersion.equals(localVersion)) {
                Log.i("URBES", "VERSION IS UP TO DATE");
            } else {
                sendMessage("Baixando nova versão...");
                this.data = getLatestFile(localVersion);
                sendMessage("Preparando...");
                String sql = zipFileToString(this.data);
                sendMessage("Atualizando...");
                updateDb(sql);
                updateLocalVersion(latestVersion);
                str = latestVersion;
                sendMessage("Atualizado !");
                if (!isRunning()) buildNotify();
            }
        } catch (Exception e) {
            Log.e("F3-URBES", "VERIFY VERSION ERROR");
            e.printStackTrace();
        }
        updatingVersion = false;
        return str;
    }

    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (this.onFinishHandler != null) {
            this.onFinishHandler.sendEmptyMessage(0);
        }
    }

    public void checkUpdate() throws Exception {
        execute(new Void[0]);
    }

    private String getLatestVersion() throws Exception {
        Log.i("F3-URBES", "CHECKING VERSION...");
        InputStream input = new URL("https://www.f3sistemas.com.br:8443/urbesmobile/getlatestversioninfo").openStream();
        String objectData = IOUtils.toString(input);
        input.close();
        return new JSONObject(objectData).get("version").toString();
    }

    private byte[] getLatestFile(String localversion) throws Exception {
        Log.i("F3-URBES", "GETTING NEW VERSION...");
        InputStream input = new URL("https://www.f3sistemas.com.br:8443/urbesmobile/getlatestversiondata?versao=" + URLEncoder.encode(localversion, "UTF-8")).openStream();
        byte[] objectData = IOUtils.toByteArray(input);
        input.close();
        return objectData;
    }

    private String zipFileToString(byte[] zipData) throws IOException {
        Log.i("F3-URBES", "READING RAW NEW VERSION...");
        InputStream is = new ByteArrayInputStream(zipData);
        ByteArrayOutputStream myOutput = new ByteArrayOutputStream();
        ZipInputStream zis = new ZipInputStream(new BufferedInputStream(is));
        try {
            while (zis.getNextEntry() != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                byte[] buffer = new byte[1024];
                while (true) {
                    int count = zis.read(buffer);
                    if (count == -1) {
                        break;
                    }
                    baos.write(buffer, 0, count);
                }
                baos.writeTo(myOutput);
            }
        } finally {
            zis.close();
            myOutput.flush();
            myOutput.close();
            is.close();
        }
        String readFile = readFile(myOutput.toByteArray());
        return readFile;
    }

    private String readFile(byte[] file) throws IOException {
        Log.i("F3-URBES", "READING TEXT NEW VERSION...");
        return new String(file, "US-ASCII");
    }

    private void updateDb(String sql) {
        Log.i("F3-URBES", "UPDATING VERSION...");
        SQLiteDatabase sQLiteDatabase = new MainDBOpenHelper(this.context, MainDBOpenHelper.DATABASE_NAME, null, MainDBOpenHelper.DATABASE_VERSION).getWritableDatabase();
        sQLiteDatabase.beginTransaction();
        for (String statement : sql.split(";")) {
            sQLiteDatabase.execSQL(statement);
        }
        sQLiteDatabase.setVersion(sQLiteDatabase.getVersion() + 1);
        sQLiteDatabase.setTransactionSuccessful();
        sQLiteDatabase.endTransaction();
        sQLiteDatabase.close();
        Log.i("F3-URBES", "VERSION UPDATED");
    }

    public String getLocalVersion() {
        try {
            return this.context.getSharedPreferences("PREF_TARIFA", 0).getString("localVersion", "Tue Aug 30 19:44:58 GMT-03:00 2016");
        } catch (Exception e) {
            Log.e("F3-URBES", e.getMessage());
            return "";
        }
    }

    public boolean skipInterval() {
        SharedPreferences settings = this.context.getSharedPreferences("PREF_TARIFA", 0);
        long diff = System.currentTimeMillis() - Long.valueOf(settings.getLong("lastCheckVersion", 0));
        if (diff >= 86400000) {
            Editor editor = settings.edit();
            editor.putLong("lastCheckVersion", System.currentTimeMillis());
            editor.commit();
            return false;
        }
        Log.i("F3-URBES", "SKIP UPDATE REMAIN " + (86400000 - diff));
        return true;
    }

    public void updateLocalVersion(String version) {
        try {
            Editor editor = this.context.getSharedPreferences("PREF_TARIFA", 0).edit();
            editor.putString("localVersion", version);
            editor.commit();
            Log.i("F3-URBES", "LOCAL VERSION NOW IS " + version);
        } catch (Exception e) {
            Log.e("F3-URBES", e.getMessage());
        }
    }

    public boolean isRunning() {
        List<RunningAppProcessInfo> procInfos = ((ActivityManager) this.context.getSystemService(Context.ACTIVITY_SERVICE)).getRunningAppProcesses();
        int i = 0;
        while (i < procInfos.size()) {
            if (((RunningAppProcessInfo) procInfos.get(i)).processName.equals("br.com.f3.urbes.activities") && ((RunningAppProcessInfo) procInfos.get(i)).importance == 100) {
                return true;
            }
            i++;
        }
        return false;
    }

    public void buildNotify() {
        Builder mBuilder = new Builder(this.context)
                .setAutoCancel(true)
                .setContentTitle("Project Bus")
                .setContentText("Atenção senhores passageiros, novos horários das linhas municipais de Sorocaba acabam de desembarcar no aplicativo.");
        mBuilder.setSmallIcon(R.mipmap.ic_launcher);
        int i = VERSION.SDK_INT;
        Intent notificationIntent = new Intent(this.context, MainActivity.class);
        //notificationIntent.setFlags(603979776);
        mBuilder.setContentIntent(PendingIntent.getActivity(this.context, 147458116, notificationIntent, PendingIntent.FLAG_NO_CREATE));
        mBuilder.setAutoCancel(true);
        mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText("Atenção senhores passageiros, novos horários das linhas municipais de Sorocaba acabam de desembarcar no aplicativo."));
        NotificationManager mNotificationManager = (NotificationManager) this.context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notify = mBuilder.build();
        notify.flags |= 16;
        mNotificationManager.notify(147458116, notify);
    }
}
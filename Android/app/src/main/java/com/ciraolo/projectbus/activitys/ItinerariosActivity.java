package com.ciraolo.projectbus.activitys;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.ciraolo.projectbus.R;
import com.ciraolo.projectbus.api.ApontamentoLinhas;
import com.ciraolo.projectbus.api.ApontamentoLocalizacao;
import com.ciraolo.projectbus.model.information.Linha;
import com.ciraolo.projectbus.model.information.RespostaObterLinhas;
import com.ciraolo.projectbus.model.location.RepostaObterDadosPortal;
import com.ciraolo.projectbus.model.location.Rota;
import com.ciraolo.projectbus.model.location.Veiculo;
import com.ciraolo.projectbus.util.PolyUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.FragmentActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ItinerariosActivity extends FragmentActivity implements OnMapReadyCallback, Callback<RespostaObterLinhas> {

    private GoogleMap mMap;
    private List<Veiculo> mVeiculos = new ArrayList<>();

    ApontamentoLocalizacao aLocalizacao = new Retrofit.Builder()
            .baseUrl("https://bustime.noxxonsat.com.br/")
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(ApontamentoLocalizacao.class);


    ApontamentoLinhas aLinhas = new Retrofit.Builder()
            .baseUrl("http://200.144.29.93:8088/")
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(ApontamentoLinhas.class);

    private ProgressDialog progressDialog;

    private List<Linha> linhasSorocabanas = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_veiculos);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng sorocaba = new LatLng(-23.499747, -47.452970);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sorocaba));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(9));

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getString(R.string.loading_text));
        progressDialog.show();

        Call<RespostaObterLinhas> call = aLinhas.obterLinhasMetropolitanas();
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<RespostaObterLinhas> call, Response<RespostaObterLinhas> response) {
        if (response.code() == 200) {
            List<Linha> tmp = response.body().linhas;
            for (Linha linha : tmp) {
                if (linha.chRegiao == 258) {
                    linhasSorocabanas.add(linha);
                }
            }

            progressDialog.dismiss();

            new ObterItinerarios().execute();
        }
    }

    @Override
    public void onFailure(Call<RespostaObterLinhas> call, Throwable t) {
        progressDialog.dismiss();
        Toast.makeText(this, t.getClass().getSimpleName() + " " + t.getLocalizedMessage(), Toast.LENGTH_LONG);
    }

    class ObterItinerarios extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            int i = 0;
            int base = 209715;
            for (Linha linha : linhasSorocabanas) {
                Call<RepostaObterDadosPortal> call1 = aLocalizacao.obterDadosPortalCompleto(linha.codigo);
                try {
                    Response<RepostaObterDadosPortal> response = call1.execute();
                    Log.e("ONIBUS", response.body().toString());
                    List<Rota> rotas = response.body().linhas.get(0).rotas;
                    for (Rota rota : rotas) {
                        List<LatLng> list = PolyUtil.decode(rota.encode);
                        final PolylineOptions po = new PolylineOptions();
                        po.add(list.toArray(new LatLng[list.size()]));
                        int r = ((base * i) / 255) / 255;
                        int g = (base * i) / 255;
                        int b = base * i;
                        po.width(3.0f).color(Color.rgb(r,g,b));

                        if (rotas.indexOf(rota) == 0) {
                            final MarkerOptions moInicial = new MarkerOptions();
                            moInicial.position(new LatLng(rota.pontos.get(0).latitude, rota.pontos.get(0).longitude));
                            moInicial.title(linha.codigo);
                            moInicial.snippet("Terminal Secundário: " + linha.destino);

                            final MarkerOptions moFinal = new MarkerOptions();
                            moFinal.position(new LatLng(rota.pontos.get(rota.pontos.size() - 1).latitude, rota.pontos.get(rota.pontos.size() - 1).longitude));
                            moInicial.title(linha.codigo);
                            moInicial.snippet("Terminal Principal: " + linha.origem);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mMap.addMarker(moInicial);
                                    mMap.addMarker(moFinal);
                                }
                            });
                        }

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mMap.addPolyline(po);
                            }
                        });
                    }
                    i++;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
    }

}

package com.ciraolo.projectbus.activitys;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.ciraolo.projectbus.R;
import com.ciraolo.projectbus.api.ApontamentoLinhas;
import com.ciraolo.projectbus.firebase.Notificacao;
import com.ciraolo.projectbus.fragments.MapaFragment;
import com.ciraolo.projectbus.fragments.NotificacoesFragment;
import com.ciraolo.projectbus.fragments.TabLinhasFragment;
import com.ciraolo.projectbus.model.information.Linha;
import com.ciraolo.projectbus.model.information.RespostaObterLinhas;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements Callback<RespostaObterLinhas>, AHBottomNavigation.OnTabSelectedListener {
    public static List<Linha> LINHAS_METROPOLITANAS = null;

    @BindView(R.id.bottom_navigation)
    AHBottomNavigation bottomNavigation;

    ApontamentoLinhas aLinhas = new Retrofit.Builder()
            .baseUrl("http://200.144.29.93:8088/")
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(ApontamentoLinhas.class);

    Fragment mapaFragment;
    boolean openNotifications = false;

    private ProgressDialog progressDialog;
    private List<Notificacao> notificacaos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        ButterKnife.bind(this);

        //AuxiliarPreferencias auxiliarPreferencias = AuxiliarPreferencias.getInstance();
        notificacaos = new ArrayList<>();

        // Create items
        AHBottomNavigationItem item1 = new AHBottomNavigationItem(R.string.title_linhas, R.drawable.ic_list_black_24dp, R.color.verde_urbes);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem(R.string.title_veiculos, R.drawable.ic_map_black_24dp, R.color.azul_emtu);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem(R.string.title_notificacoes, R.drawable.ic_notifications_black_24dp, R.color.vermelho_emtu);

        // Add items
        bottomNavigation.addItem(item1);
        bottomNavigation.addItem(item2);
        bottomNavigation.addItem(item3);

        // Use colored navigation with circle reveal effect
        bottomNavigation.setColored(true);

        // Set listeners
        bottomNavigation.setOnTabSelectedListener(this);

        openNotifications = getIntent().getBooleanExtra("openNotifications", false);

        if (LINHAS_METROPOLITANAS == null) {
            carregarLinhas();
        } else {
            if (!openNotifications)
                carregarPrimeiraTela();
            else
                carregarTerceiraTela();
        }
    }

    private void carregarLinhas() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getString(R.string.loading_text));
        progressDialog.show();

        Call<RespostaObterLinhas> call = aLinhas.obterLinhasMetropolitanas();
        call.enqueue(this);

    }

    @Override
    public void onResponse(Call<RespostaObterLinhas> call, Response<RespostaObterLinhas> response) {
        progressDialog.dismiss();
        List<Linha> todasLinhas = response.body().linhas;
        List<Linha> apenasRms = new ArrayList<>();

        for (Linha linha : todasLinhas) {
            if (linha.chRegiao == 258) {
                apenasRms.add(linha);
            }
        }

        LINHAS_METROPOLITANAS = apenasRms;
        Log.i("ONIBUS", LINHAS_METROPOLITANAS.toString());

        if (!openNotifications)
            carregarPrimeiraTela();
        else
            carregarTerceiraTela();
    }

    @Override
    public void onFailure(Call<RespostaObterLinhas> call, Throwable t) {
        progressDialog.dismiss();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("O ônibus saiu do itinerário...");
        builder.setMessage("Alguma coisa aconteceu com o ônibus que trazia as inforamções do aplicativo... Deseja tentar contato novamente?");
        builder.setPositiveButton("Tentar novamente", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                carregarLinhas();
            }
        });
        builder.setNegativeButton("Sair do App", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });

        builder.create().show();
    }

    private void carregarPrimeiraTela() {
        // Set current item programmatically
        bottomNavigation.setCurrentItem(0);
        Fragment fragment = TabLinhasFragment.newInstance();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content, fragment);
        transaction.commit();
    }

    private void carregarTerceiraTela() {
        // Set current item programmatically
        bottomNavigation.setCurrentItem(2);
        Fragment fragment = NotificacoesFragment.newInstance();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content, fragment);
        transaction.commit();
    }

    @Override
    public boolean onTabSelected(int position, boolean wasSelected) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        switch (position) {
            case 0:
                if (!wasSelected) {
                    if (notificacaos.size() != 0) bottomNavigation.setNotification(String.valueOf(notificacaos.size()), 2);
                    Fragment lFrag = TabLinhasFragment.newInstance();
                    transaction.replace(R.id.content, lFrag);
                    transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                    transaction.commit();
                }
                return true;
            case 1:
                if (!wasSelected) {
                    if (notificacaos.size() != 0) bottomNavigation.setNotification(String.valueOf(notificacaos.size()), 2);
                    if (mapaFragment == null) mapaFragment = MapaFragment.newInstance();
                    transaction.replace(R.id.content, mapaFragment);
                    transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                    transaction.commit();
                }
                return true;
            case 2:
                if (!wasSelected) {
                    bottomNavigation.setNotification("", 2);
                    Fragment nFrag = NotificacoesFragment.newInstance();
                    transaction.replace(R.id.content, nFrag);
                    transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                    transaction.commit();
                }
                return true;
        }
        return true;
    }
}

package com.ciraolo.projectbus.urbes;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * @author Henrique Ciraolo Gomes
 */
public abstract class BasicoDao {
    protected SQLiteDatabase mDb;

    protected BasicoDao(Context context) {
        mDb = DatabaseControl.openDatabase(context);
    }

    public static void release() {
        DatabaseControl.closeDatabase();
        Log.i(BasicoDao.class.getSimpleName(), "A referencia da base de dados foi liberada.");
    }
}
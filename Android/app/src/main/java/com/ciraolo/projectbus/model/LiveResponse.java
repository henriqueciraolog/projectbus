package com.ciraolo.projectbus.model;

import java.util.List;

public class LiveResponse {

    public int pointCode;
    public String mensagem;
    public Integer validade;
    public List<Previso> previsoes = null;

}
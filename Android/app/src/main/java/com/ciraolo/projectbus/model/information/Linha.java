package com.ciraolo.projectbus.model.information;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class Linha implements Serializable {

    public Integer chElemento;
    public Integer chRegiao;
    public String codigo;
    public String destino;
    public String origem;
    public String sentido;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
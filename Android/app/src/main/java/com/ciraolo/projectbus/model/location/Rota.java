package com.ciraolo.projectbus.model.location;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Rota implements Serializable {

    public Long id;
    public Long version;
    public String codigoLinha;
    public String sentido;
    public List<Ponto> pontos = null;
    public String destino;
    public String encode;
    public Integer distancia;
    public Integer distanciaGestec;
    public Integer tempo;
    public Integer raioCerca;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
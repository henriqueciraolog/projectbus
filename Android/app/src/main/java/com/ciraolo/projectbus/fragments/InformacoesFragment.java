package com.ciraolo.projectbus.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ciraolo.projectbus.R;
import com.ciraolo.projectbus.adapter.SeccionamentoRecyclerViewAdapter;
import com.ciraolo.projectbus.model.information.RespostaObterInformacoes;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;


public class InformacoesFragment extends Fragment {
    private static final String ARG_INFORMACOES_IDA = "informacoes_ida";
    private static final String ARG_INFORMACOES_VOLTA = "informacoes_volta";
    private static final String ARG_SENTIDO_IDA = "sentido_ida";
    private static final String ARG_SENTIDO_VOLTA = "sentido_volta";
    private static final String ARG_CIRCULAR = "circular";

    //VIEWS
    @BindView(R.id.txvTarifa)
    TextView txvTarifa;
    @BindView(R.id.txvTempoViagemIda)
    TextView txvTempoViagemIda;
    @BindView(R.id.txvTempoViagemVolta)
    TextView txvTempoViagemVolta;
    @BindView(R.id.txvSeccionamentos)
    TextView txvSeccionamentos;
    @BindView(R.id.rcvSeccionamentos)
    RecyclerView rcvSeccionamentos;

    private String mSentidoIda;
    private String mSentidoVolta;
    private RespostaObterInformacoes mInformacoesIda;
    private RespostaObterInformacoes mInformacoesVolta;
    private boolean mIsCircular;

    public InformacoesFragment() {
    }

    public static InformacoesFragment newInstance(String sentidoIda, String sentidoVolta, RespostaObterInformacoes informacoesIda, RespostaObterInformacoes informacoesVolta, boolean isCircular) {
        InformacoesFragment fragment = new InformacoesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_SENTIDO_IDA, sentidoIda);
        args.putString(ARG_SENTIDO_VOLTA, sentidoVolta);
        args.putSerializable(ARG_INFORMACOES_IDA, informacoesIda);
        args.putSerializable(ARG_INFORMACOES_VOLTA, informacoesVolta);
        args.putBoolean(ARG_CIRCULAR, isCircular);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mSentidoIda = getArguments().getString(ARG_SENTIDO_IDA);
            mSentidoVolta = getArguments().getString(ARG_SENTIDO_VOLTA);
            mInformacoesIda = (RespostaObterInformacoes) getArguments().getSerializable(ARG_INFORMACOES_IDA);
            mInformacoesVolta = (RespostaObterInformacoes) getArguments().getSerializable(ARG_INFORMACOES_VOLTA);
            mIsCircular = getArguments().getBoolean(ARG_CIRCULAR);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_informacoes, container, false);
        ButterKnife.bind(this, view);
        setupFragment();
        return view;
    }

    private void setupFragment() {
        txvTarifa.setText(getString(R.string.tarifa, mInformacoesIda.valorTarifa));

        if (mInformacoesVolta != null) {
            txvTempoViagemIda.setText(getString(R.string.viagem_tempo, mSentidoIda, mSentidoVolta, mInformacoesIda.tempoDePercurso));
            txvTempoViagemVolta.setText(getString(R.string.viagem_tempo, mSentidoVolta, mSentidoIda, mInformacoesVolta.tempoDePercurso));
        } else {
            if (mIsCircular) {
                txvTempoViagemIda.setText(getString(R.string.viagem_circular, mInformacoesIda.tempoDePercurso));
            } else {
                txvTempoViagemIda.setText(getString(R.string.viagem_tempo, mSentidoIda, mSentidoVolta, mInformacoesIda.tempoDePercurso));
            }
            txvTempoViagemVolta.setVisibility(View.GONE);
        }

        if (mInformacoesIda.seccionamento.size() != 0) {
            SeccionamentoRecyclerViewAdapter adapter = new SeccionamentoRecyclerViewAdapter(mInformacoesIda.seccionamento);
            rcvSeccionamentos.setAdapter(adapter);

            rcvSeccionamentos.setHasFixedSize(true);

            LinearLayoutManager rcvLayoutManager = new LinearLayoutManager(getActivity());
            rcvSeccionamentos.setLayoutManager(rcvLayoutManager);
        } else {
            txvSeccionamentos.setVisibility(View.GONE);
            rcvSeccionamentos.setVisibility(View.GONE);
        }
    }
}

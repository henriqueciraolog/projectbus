package com.ciraolo.projectbus.model.information;

import java.util.List;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class RespostaObterLinhas {

public List<Linha> linhas = null;

@Override
public String toString() {
return ToStringBuilder.reflectionToString(this);
}

}
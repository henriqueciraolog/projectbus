package com.ciraolo.projectbus.activitys;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.ciraolo.projectbus.R;
import com.ciraolo.projectbus.api.ApontamentoLinhas;
import com.ciraolo.projectbus.api.ApontamentoLocalizacao;
import com.ciraolo.projectbus.fragments.HorariosFragment;
import com.ciraolo.projectbus.fragments.InformacoesFragment;
import com.ciraolo.projectbus.model.information.Linha;
import com.ciraolo.projectbus.model.information.RespostaObterInformacoes;
import com.ciraolo.projectbus.model.location.RepostaObterDadosPortal;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LinhaDetalheActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;

    ApontamentoLinhas aLinhas = new Retrofit.Builder()
            .baseUrl("http://200.144.29.93:8088/")
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(ApontamentoLinhas.class);

    ApontamentoLocalizacao aLocalizacao = new Retrofit.Builder()
            .baseUrl("https://bustime.noxxonsat.com.br/")
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(ApontamentoLocalizacao.class);

    private Toolbar toolbar;
    private ViewPager viewPager;
    private TabLayout tabLayout;

    private Linha linha;
    private com.ciraolo.projectbus.model.location.Linha linhaLoc;
    private RespostaObterInformacoes respostaSentidoIda;
    private RespostaObterInformacoes respostaSentidoVolta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_linha_detalhe);

        linha = (Linha) getIntent().getSerializableExtra("linha");
        if (linha == null) {
            return;
        }

        setupLayout();

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getString(R.string.loading_text));
        progressDialog.show();

        /*obterDadosLocation();*/
        obterDadosGestec();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    private void obterDadosLocation() {
        Call<RepostaObterDadosPortal> call = aLocalizacao.obterDadosPortalCompleto(linha.codigo);
        call.enqueue(new Callback<RepostaObterDadosPortal>() {
            @Override
            public void onResponse(Call<RepostaObterDadosPortal> call, Response<RepostaObterDadosPortal> response) {
                linhaLoc = response.body().linhas.get(0);
                obterDadosGestec();
            }

            @Override
            public void onFailure(Call<RepostaObterDadosPortal> call, Throwable t) {
                linhaLoc = null;
                obterDadosGestec();
            }
        });
    }

    private void obterDadosGestec() {
        if (linha.sentido.equals("CIRCULAR")) {
            Call<RespostaObterInformacoes> call = aLinhas.obterInformacoesSobreLinha(linha.codigo, "CIRCULAR");
            call.enqueue(new Callback<RespostaObterInformacoes>() {
                @Override
                public void onResponse(Call<RespostaObterInformacoes> call, Response<RespostaObterInformacoes> response) {
                    respostaSentidoIda = response.body();
                    setupViewPager();
                }

                @Override
                public void onFailure(Call<RespostaObterInformacoes> call, Throwable t) {
                    setupViewPager();
                }
            });
        } else {
            Call<RespostaObterInformacoes> call = aLinhas.obterInformacoesSobreLinha(linha.codigo, "IDA");
            call.enqueue(new Callback<RespostaObterInformacoes>() {
                @Override
                public void onResponse(Call<RespostaObterInformacoes> call, Response<RespostaObterInformacoes> response) {
                    if (response.body() != null) {
                        respostaSentidoIda = response.body();
                    }

                    Call<RespostaObterInformacoes> call2 = aLinhas.obterInformacoesSobreLinha(linha.codigo, "VOLTA");
                    call2.enqueue(new Callback<RespostaObterInformacoes>() {
                        @Override
                        public void onResponse(Call<RespostaObterInformacoes> call, Response<RespostaObterInformacoes> response) {
                            if (response.body() != null) {
                                respostaSentidoVolta = response.body();
                            }
                            setupViewPager();
                        }

                        @Override
                        public void onFailure(Call<RespostaObterInformacoes> call, Throwable t) {
                            setupViewPager();
                        }
                    });
                }

                @Override
                public void onFailure(Call<RespostaObterInformacoes> call, Throwable t) {
                    Call<RespostaObterInformacoes> call2 = aLinhas.obterInformacoesSobreLinha(linha.codigo, "VOLTA");
                    call2.enqueue(new Callback<RespostaObterInformacoes>() {
                        @Override
                        public void onResponse(Call<RespostaObterInformacoes> call, Response<RespostaObterInformacoes> response) {
                            if (response.body() != null) {
                                respostaSentidoVolta = response.body();
                            }
                            setupViewPager();
                        }

                        @Override
                        public void onFailure(Call<RespostaObterInformacoes> call, Throwable t) {
                            setupViewPager();
                        }
                    });
                }
            });
        }
    }

    private void setupLayout() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle("Detalhes da linha " + linha.codigo);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        //setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        InformacoesFragment informacoesFragment = InformacoesFragment.newInstance(linha.origem, linha.destino, respostaSentidoIda, respostaSentidoVolta, linha.sentido.equals("CIRCULAR"));
        adapter.addFragment(informacoesFragment, "Info");

        /*if (linhaLoc != null) {
            ItinerarioFragment itinerarioFragment = ItinerarioFragment.newInstance(linhaLoc);
            adapter.addFragment(itinerarioFragment, "Itinerário");
        }*/

        List<String> horariosIda;
        List<String> horariosVolta;

        if (respostaSentidoIda != null) {
            horariosIda = respostaSentidoIda.horarios.diasUteis;
        } else {
            horariosIda = new ArrayList<>();
        }
        if (respostaSentidoVolta != null) {
            horariosVolta = respostaSentidoVolta.horarios.diasUteis;
        } else {
            horariosVolta = new ArrayList<>();
        }

        if (!(horariosIda.size() == 0 && horariosVolta.size() == 0)) {
            HorariosFragment horariosFragment = HorariosFragment.newInstance(linha.origem, linha.destino, horariosIda, horariosVolta);
            adapter.addFragment(horariosFragment, "Dias úteis");
        }

        if (respostaSentidoIda != null) {
            horariosIda = respostaSentidoIda.horarios.sabado;
        } else {
            horariosIda = new ArrayList<>();
        }
        if (respostaSentidoVolta != null) {
            horariosVolta = respostaSentidoVolta.horarios.sabado;
        } else {
            horariosVolta = new ArrayList<>();
        }

        if (!(horariosIda.size() == 0 && horariosVolta.size() == 0)) {
            HorariosFragment horariosFragment = HorariosFragment.newInstance(linha.origem, linha.destino, horariosIda, horariosVolta);
            adapter.addFragment(horariosFragment, "Sabados");
        }

        if (respostaSentidoIda != null) {
            horariosIda = respostaSentidoIda.horarios.domingoFeriado;
        } else {
            horariosIda = new ArrayList<>();
        }
        if (respostaSentidoVolta != null) {
            horariosVolta = respostaSentidoVolta.horarios.domingoFeriado;
        } else {
            horariosVolta = new ArrayList<>();
        }

        if (!(horariosIda.size() == 0 && horariosVolta.size() == 0)) {
            HorariosFragment horariosFragment = HorariosFragment.newInstance(linha.origem, linha.destino, horariosIda, horariosVolta);
            adapter.addFragment(horariosFragment, "Domingos/Feriados");
        }

        if (adapter.getCount() == 0) {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Não foi possível carregar os dados nesse momento.", Snackbar.LENGTH_LONG);
            snackbar.show();
        }

        viewPager.setAdapter(adapter);
        progressDialog.dismiss();
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}

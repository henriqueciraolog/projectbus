package com.ciraolo.projectbus.model.information;

import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Horarios {

    public List<String> diasUteis = null;
    public List<String> domingoFeriado = null;
    public List<String> sabado = null;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
package com.ciraolo.projectbus.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ciraolo.projectbus.R;
import com.ciraolo.projectbus.adapter.StringArrayAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;


public class HorariosFragment extends Fragment {
    private static final String ARG_SENTIDO_IDA = "sentido_ida";
    private static final String ARG_SENTIDO_VOLTA = "sentido_volda";
    private static final String ARG_HORARIOS_IDA = "horarios_ida";
    private static final String ARG_HORARIOS_VOLTA = "horarios_volta";

    //VIEWS
    @BindView(R.id.lnlHorariosIda)
    LinearLayout lnlHorariosIda;
    @BindView(R.id.txvHorarioIda)
    TextView txvHorarioIda;
    @BindView(R.id.rcvHorariosIda1)
    RecyclerView rcvHorariosIda1;
    @BindView(R.id.rcvHorariosIda2)
    RecyclerView rcvHorariosIda2;
    @BindView(R.id.lnlHorariosVolta)
    LinearLayout lnlHorariosVolta;
    @BindView(R.id.txvHorarioVolta)
    TextView txvHorarioVolta;
    @BindView(R.id.rcvHorariosVolta1)
    RecyclerView rcvHorariosVolta1;
    @BindView(R.id.rcvHorariosVolta2)
    RecyclerView rcvHorariosVolta2;

    private String mSentidoIda;
    private String mSentidoVolta;
    private List<String> mHorariosIda;
    private List<String> mHorariosVolta;

    public HorariosFragment() {
    }

    public static HorariosFragment newInstance(String sentidoIda, String sentidoVolta, List<String> horariosIda, List<String> horariosVolta) {
        HorariosFragment fragment = new HorariosFragment();
        Bundle args = new Bundle();
        args.putString(ARG_SENTIDO_IDA, sentidoIda);
        args.putString(ARG_SENTIDO_VOLTA, sentidoVolta);
        args.putStringArrayList(ARG_HORARIOS_IDA, (ArrayList<String>) horariosIda);
        args.putStringArrayList(ARG_HORARIOS_VOLTA, (ArrayList<String>) horariosVolta);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mSentidoIda = getArguments().getString(ARG_SENTIDO_IDA);
            mSentidoVolta = getArguments().getString(ARG_SENTIDO_VOLTA);
            mHorariosIda = getArguments().getStringArrayList(ARG_HORARIOS_IDA);
            mHorariosVolta = getArguments().getStringArrayList(ARG_HORARIOS_VOLTA);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_horarios, container, false);
        ButterKnife.bind(this, view);
        setupFragment();
        return view;
    }

    private void setupFragment() {
        if (mHorariosIda.size() == 0) {
            lnlHorariosIda.setVisibility(View.GONE);
        } else {
            txvHorarioIda.setText(mSentidoIda);

            Collections.sort(mHorariosIda, new Comparator<String>() {
                @Override
                public int compare(String o1, String o2) {

                    String[] splito1 = o1.split(":");
                    String[] splito2 = o2.split(":");
                    int tempoo1 = (Integer.parseInt(splito1[0]) * 60) + Integer.parseInt(splito1[1]);
                    int tempoo2 = (Integer.parseInt(splito2[0]) * 60) + Integer.parseInt(splito2[1]);
                    return tempoo1 - tempoo2;
                }
            });

            if (mHorariosIda.size() > 10) {
                int pos = (mHorariosIda.size() - 1)/2;
                List<String> mHorariosIda1 = mHorariosIda.subList(0,pos);
                List<String> mHorariosIda2 = mHorariosIda.subList(pos,mHorariosIda.size() - 1);

                StringArrayAdapter adapter = new StringArrayAdapter(getActivity(), mHorariosIda1);
                rcvHorariosIda1.setAdapter(adapter);

                adapter = new StringArrayAdapter(getActivity(), mHorariosIda2);
                rcvHorariosIda2.setAdapter(adapter);
            } else {
                StringArrayAdapter adapter = new StringArrayAdapter(getActivity(), mHorariosIda);
                rcvHorariosIda1.setAdapter(adapter);
                rcvHorariosIda2.setVisibility(View.GONE);
            }

            rcvHorariosIda1.setHasFixedSize(true);
            rcvHorariosIda2.setHasFixedSize(true);

            LinearLayoutManager rcvLayoutManager = new LinearLayoutManager(getActivity());
            rcvHorariosIda1.setLayoutManager(rcvLayoutManager);
            rcvLayoutManager = new LinearLayoutManager(getActivity());
            rcvHorariosIda2.setLayoutManager(rcvLayoutManager);
        }

        if (mHorariosVolta.size() == 0) {
            lnlHorariosVolta.setVisibility(View.GONE);
        } else {
            txvHorarioVolta.setText(mSentidoVolta);

            Collections.sort(mHorariosVolta, new Comparator<String>() {
                @Override
                public int compare(String o1, String o2) {

                    String[] splito1 = o1.split(":");
                    String[] splito2 = o2.split(":");
                        int tempoo1 = (Integer.parseInt(splito1[0]) * 60) + Integer.parseInt(splito1[1]);
                        int tempoo2 = (Integer.parseInt(splito2[0]) * 60) + Integer.parseInt(splito2[1]);
                    return tempoo1 - tempoo2;
                }
            });

            if (mHorariosVolta.size() > 10) {
                int pos = (mHorariosVolta.size() - 1)/2;
                List<String> mHorariosVolta1 = mHorariosVolta.subList(0,pos);
                List<String> mHorariosVolta2 = mHorariosVolta.subList(pos,mHorariosVolta.size() - 1);

                StringArrayAdapter adapter = new StringArrayAdapter(getActivity(), mHorariosVolta1);
                rcvHorariosVolta1.setAdapter(adapter);
                adapter = new StringArrayAdapter(getActivity(), mHorariosVolta2);
                rcvHorariosVolta2.setAdapter(adapter);
            } else {
                StringArrayAdapter adapter = new StringArrayAdapter(getActivity(), mHorariosVolta);
                rcvHorariosVolta1.setAdapter(adapter);
                rcvHorariosVolta2.setVisibility(View.GONE);
            }

            rcvHorariosVolta1.setHasFixedSize(true);
            rcvHorariosVolta2.setHasFixedSize(true);

            LinearLayoutManager rcvLayoutManager = new LinearLayoutManager(getActivity());
            rcvHorariosVolta1.setLayoutManager(rcvLayoutManager);
            rcvLayoutManager = new LinearLayoutManager(getActivity());
            rcvHorariosVolta2.setLayoutManager(rcvLayoutManager);
        }
    }
}

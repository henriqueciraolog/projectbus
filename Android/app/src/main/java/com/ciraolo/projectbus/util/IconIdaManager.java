package com.ciraolo.projectbus.util;

import java.util.Hashtable;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

public class IconIdaManager {
	private static final String TAG = "IconIdaManager";

	private static final Hashtable<String, BitmapDescriptor> cache = new Hashtable<>();
	public static BitmapDescriptor get(Context c, String key) {

		synchronized (cache) {
			if (!cache.containsKey(key)) {
				try {
					int id = c.getResources().getIdentifier(String.format("onibus_ida_%1d", cache.size() + 1), "mipmap","com.ciraolo.onibusteste");
					BitmapDescriptor resource = BitmapDescriptorFactory.fromResource(id);
					cache.put(key, resource);
				} catch (Exception e) {
					Log.e(TAG, "Could not get typeface '" + key + "' because " + e.getMessage());
					return null;
				}
			}
			return cache.get(key);
		}
	}
			
}
package com.ciraolo.projectbus.api;

import com.ciraolo.projectbus.model.information.RespostaObterInformacoes;
import com.ciraolo.projectbus.model.information.RespostaObterLinhas;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by henriqueciraolo on 22/01/17.
 */

public interface ApontamentoLinhas {
        @Headers({"Authorization: Basic cm9iby1lbXR1OmVtdHUxMDMw"})
        @GET("services.svc/linhas/getListaDeLinhas")
        Call<RespostaObterLinhas> obterLinhasMetropolitanas();

        @Headers({"Authorization: Basic cm9iby1lbXR1OmVtdHUxMDMw"})
        @GET("services.svc/linhas/getInformacoesGestecDaLinha")
        Call<RespostaObterInformacoes> obterInformacoesSobreLinha(@Query("codigoLinha") String codigoLinha, @Query("sentidoLinha") String sentidoLinha);
}

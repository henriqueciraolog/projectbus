package com.ciraolo.projectbus.urbes;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.ZipInputStream;

public class MainDBOpenHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME;
    public static final String DATABASE_NAME_OLD;
    public static int DATABASE_VERSION;
    private Context context;

    static {
        DATABASE_VERSION = 132;
        DATABASE_NAME_OLD = "urbes_database_".concat(String.valueOf(DATABASE_VERSION - 1)).concat(".db");
        DATABASE_NAME = "urbes_database_".concat(String.valueOf(DATABASE_VERSION)).concat(".db");
    }

    public MainDBOpenHelper(Context context, String name, CursorFactory factory, int version) {
        super(context, name, factory, version);
        this.context = context;
        if (!checkDataBase()) {
            getWritableDatabase();
            try {
                copyFromZipFile();
            } catch (IOException e) {
                throw new Error("Error copying database");
            }
        }
    }

    public void onCreate(SQLiteDatabase db) {
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        DATABASE_VERSION = newVersion;
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i("F3-URBES", "*** onUpgrade");
    }

    private boolean checkDataBase() {
        return this.context.getDatabasePath(DATABASE_NAME).exists();
    }

    private void copyFromZipFile() throws IOException {
        File dir = new File(this.context.getDatabasePath(DATABASE_NAME).getParent());
        Log.i("F3-URBES", "REMOVENDO BANCO MAIN...");
        for (File file : dir.listFiles()) {
            if (file.getName().contains("urbes_database")) {
                file.delete();
            }
        }
        Log.i("F3-URBES", "BANCO REMOVIDO MAIN...");
        Log.i("F3-URBES", "CRIANDO BANCO MAIN...");
        InputStream is = this.context.getAssets().open("db/urbes_database.zip");
        OutputStream myOutput = new FileOutputStream(this.context.getDatabasePath(DATABASE_NAME));
        ZipInputStream zis = new ZipInputStream(new BufferedInputStream(is));
        try {
            while (zis.getNextEntry() != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                byte[] buffer = new byte[1024];
                while (true) {
                    int count = zis.read(buffer);
                    if (count == -1) {
                        break;
                    }
                    baos.write(buffer, 0, count);
                }
                baos.writeTo(myOutput);
            }
        } finally {
            zis.close();
            myOutput.flush();
            myOutput.close();
            is.close();
        }
        Log.i("F3-URBES", "BANCO CRIADO MAIN...");
    }
}
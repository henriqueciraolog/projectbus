package com.ciraolo.projectbus.fragments;


import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.ciraolo.projectbus.R;
import com.ciraolo.projectbus.activitys.LinhaDetalheActivity;
import com.ciraolo.projectbus.activitys.MainActivity;
import com.ciraolo.projectbus.api.ApontamentoLocalizacao;
import com.ciraolo.projectbus.model.information.Linha;
import com.ciraolo.projectbus.model.location.RepostaObterDadosPortal;
import com.ciraolo.projectbus.model.location.Veiculo;
import com.ciraolo.projectbus.util.IconIdaManager;
import com.ciraolo.projectbus.util.IconVoltaManager;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapaFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, Callback<RepostaObterDadosPortal>, AdapterView.OnItemClickListener {

    @BindView(R.id.bottomSheetLayout)
    View bottomSheetLayout;
    @BindView(R.id.txvBusPrefixo)
    TextView txvBusPrefixo;
    @BindView(R.id.txvBusConsorcio)
    TextView txvBusConsorcio;
    @BindView(R.id.txvBusEmpresa)
    TextView txvBusEmpresa;
    @BindView(R.id.txvBusAcessibilidade)
    TextView txvBusAcessibilidade;
    @BindView(R.id.txvBusModelo)
    TextView txvBusModelo;
    @BindView(R.id.txvBusAno)
    TextView txvBusAno;
    @BindView(R.id.txvBusLinha)
    TextView txvBusLinha;
    @BindView(R.id.txvBusCapSentado)
    TextView txvBusCapSentado;
    @BindView(R.id.txvBusCapEmPe)
    TextView txvBusCapEmPe;
    @BindView(R.id.prbLoading)
    ProgressBar prbLoading;
    @BindView(R.id.ctvInputLinha)
    AutoCompleteTextView ctvInputLinha;

    private GoogleMap mMap;
    private List<Veiculo> mVeiculos = new ArrayList<>();

    private int i = 0;

    private String[] empresas = new String[]{"Viação Calvip RMS", "Rápido Campinas RMS", "Viação São Roque RMS", "Viação Piracema RMS", "Vila Élvio RMS", "São João RMS", "Tieteense Turismo RMS", "VB RMS"};

    List<Marker> listMarker = new ArrayList<>();

    ApontamentoLocalizacao aLocalizacao = new Retrofit.Builder()
            .baseUrl("https://bustime.noxxonsat.com.br/")
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(ApontamentoLocalizacao.class);

    private List<Linha> linhasSorocabanas = MainActivity.LINHAS_METROPOLITANAS;

    // BottomSheetBehavior variable
    private BottomSheetBehavior bottomSheetBehavior;
    private Call<RepostaObterDadosPortal> locCall;

    public MapaFragment() {
        // Required empty public constructor
    }

    public static MapaFragment newInstance() {
        MapaFragment fragment = new MapaFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_veiculos, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        prbLoading.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getActivity(), R.color.vermelho_emtu), PorterDuff.Mode.SRC_IN);

        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetLayout);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

        for (String empresa : empresas) {
            IconVoltaManager.get(getActivity(), empresa);
            IconIdaManager.get(getActivity(), empresa);
        }

        carregarLinhas();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void carregarLinhas() {
        List<String> linhasString = new ArrayList<>();


        for (Linha linha : linhasSorocabanas) {
            linhasString.add(String.format("%s %s %s", linha.codigo, linha.origem, linha.destino));
        }

        linhasString.add("Mostrar todas");

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, linhasString.toArray(new String[linhasString.size()]));
        ctvInputLinha.setAdapter(adapter);
        ctvInputLinha.setOnItemClickListener(this);

        ctvInputLinha.setText("Mostrar todas");
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng sorocaba = new LatLng(-23.499747, -47.452970);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sorocaba));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(9));

        mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getActivity(), R.raw.google_maps));

        mMap.setOnMarkerClickListener(this);

        getVehiclesData();
    }

    private void getVehiclesData() {
        if (i == linhasSorocabanas.size()) {
            prbLoading.setVisibility(View.GONE);
            startUpdate();
        } else {
            prbLoading.setVisibility(View.VISIBLE);
            locCall = aLocalizacao.obterDadosPortalApenasVeiculos(linhasSorocabanas.get(i++).codigo);
            locCall.enqueue(this);
        }
    }

    private void startUpdate() {
        new CountDownTimer(30000, 30000) { //40000 milli seconds is total time, 1000 milli seconds is time interval

            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                if (isResumed()) {
                    i = 0;
                    getVehiclesData();
                }
            }
        }.start();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        Veiculo veiculo = (Veiculo) marker.getTag();

        final Linha linha = buscaInformacaoPelaLinha(veiculo.codigoLinha);

        txvBusPrefixo.setText(getString(R.string.onibus_prefixo, veiculo.prefixo));
        txvBusConsorcio.setText(getString(R.string.onibus_consorcio, veiculo.consorcio));
        txvBusEmpresa.setText(getString(R.string.onibus_empresa, veiculo.empresa.replace(" RMS", "")));

        txvBusAcessibilidade.setText(veiculo.acessibilidade);
        txvBusModelo.setText(String.format("%s %s", veiculo.fabricante, veiculo.modelo));
        txvBusAno.setText(veiculo.ano);
        txvBusLinha.setText(String.format("%s %s", veiculo.codigoLinha, veiculo.sentidoLinha.equals("volta") ? linha.origem : linha.destino));
        txvBusCapSentado.setText(veiculo.capacidadeSentados.toString());
        txvBusCapSentado.setText(veiculo.capacidadeEmPe.toString());

        txvBusLinha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it = new Intent(getActivity(), LinhaDetalheActivity.class);
                Bundle extras = new Bundle();
                extras.putSerializable("linha", linha);
                it.putExtras(extras);
                getActivity().startActivity(it);
            }
        });

        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

        return false;
    }

    private Linha buscaInformacaoPelaLinha(String codLinha) {
        for (Linha linha : MainActivity.LINHAS_METROPOLITANAS) {
            if (linha.codigo.equals(codLinha)) {
                return linha;
            }
        }

        return null;
    }

    @Override
    public void onResponse(Call<RepostaObterDadosPortal> call, Response<RepostaObterDadosPortal> response) {

        if (!call.isCanceled()) {
            List<Veiculo> veiculos = response.body().linhas.get(0).veiculos;

            String busId = "L" + response.body().linhas.get(0).codigo;

            List<Marker> toRemove = new ArrayList<>();

            for (Marker m : listMarker) {
                Veiculo veiculo = (Veiculo) m.getTag();
                String busIdComp = "L" + veiculo.codigoLinha;
                if (busId.equals(busIdComp)) {
                    m.remove();
                    toRemove.add(m);
                }
            }

            listMarker.removeAll(toRemove);

            for (Veiculo veiculo : veiculos) {

                LatLng onibus = new LatLng(veiculo.latitude, veiculo.longitude);
                mVeiculos.add(veiculo);
                MarkerOptions mo = new MarkerOptions();

                if (veiculo.sentidoLinha.equals("volta")) {
                    mo.icon(IconVoltaManager.get(MapaFragment.this.getActivity(), veiculo.empresa));
                } else {
                    mo.icon(IconIdaManager.get(MapaFragment.this.getActivity(), veiculo.empresa));
                }

                mo.position(onibus);

                Marker maker = mMap.addMarker(mo);
                maker.setTag(veiculo);

                listMarker.add(maker);
                Log.e("ONIBUS", veiculo.toString());
            }

            if (!call.isCanceled()) getVehiclesData();
        }
    }

    @Override
    public void onFailure(Call<RepostaObterDadosPortal> call, Throwable t) {
        if (!call.isCanceled()) {
            String busId = "L" + linhasSorocabanas.get((i - 1)).codigo;

            List<Marker> toRemove = new ArrayList<>();

            for (Marker m : listMarker) {
                Veiculo veiculo = (Veiculo) m.getTag();
                String busIdComp = "L" + veiculo.codigoLinha;
                if (busId.equals(busIdComp)) {
                    m.remove();
                    toRemove.add(m);
                }
            }

            listMarker.removeAll(toRemove);

            Log.i("ONIBUS", "", t);

            if (!call.isCanceled()) getVehiclesData();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if (locCall != null && locCall.isExecuted()) {
            locCall.cancel();
        }

        for (Marker m : listMarker) {
            m.remove();
        }

        listMarker.clear();

        LatLng sorocaba = new LatLng(-23.499747, -47.452970);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sorocaba));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(9));

        String[] strs = ctvInputLinha.getText().toString().split(" ");

        if (ctvInputLinha.getText().toString().equals("Mostrar todas")) {
            linhasSorocabanas = MainActivity.LINHAS_METROPOLITANAS;
            this.i = 0;
            getVehiclesData();
        } else {
            linhasSorocabanas = new ArrayList<>();
            linhasSorocabanas.add(buscaInformacaoPelaLinha(strs[0]));
            this.i = 0;
            getVehiclesData();
        }

    }

    /*private class ObterVeiculos extends AsyncTask<Linha, String, List<Veiculo>> {

        @Override
        protected List<Veiculo> doInBackground(Linha... linhas) {
            Call<RepostaObterDadosPortal> locCall = aLocalizacao.obterDadosPortalApenasVeiculos(linhas[0].codigo);
            try {
                Response<RepostaObterDadosPortal> locResponse = locCall.execute();

                return veiculos;
            } catch (IOException e) {
                e.printStackTrace();
            }

            return new ArrayList<>();
        }

        @Override
        protected void onProgressUpdate(String... values) {
        }

        @Override
        protected void onPostExecute(List<Veiculo> veiculos) {
        }
    }*/
}

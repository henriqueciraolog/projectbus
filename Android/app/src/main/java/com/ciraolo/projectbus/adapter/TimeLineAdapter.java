package com.ciraolo.projectbus.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ciraolo.projectbus.R;
import com.ciraolo.projectbus.model.location.Ponto;
import com.ciraolo.projectbus.model.location.Veiculo;
import com.github.vipulasri.timelineview.TimelineView;

import java.util.List;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by HP-HP on 05-12-2015.
 */
public class TimeLineAdapter extends RecyclerView.Adapter<TimeLineAdapter.TimeLineViewHolder> {

    private final List<Veiculo> mVehicleList;
    private List<Ponto> mFeedList;
    private Context mContext;
    private boolean mWithLinePadding;
    private LayoutInflater mLayoutInflater;

    public TimeLineAdapter(List<Ponto> feedList, List<Veiculo> vehicleList, boolean withLinePadding) {
        mFeedList = feedList;
        mVehicleList = vehicleList;
        mWithLinePadding = withLinePadding;
    }

    @Override
    public int getItemViewType(int position) {
        return TimelineView.getTimeLineViewType(position, getItemCount());
    }

    @Override
    public TimeLineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        mLayoutInflater = LayoutInflater.from(mContext);
        View view = mLayoutInflater.inflate(R.layout.adapter_itinerario, parent, false);

        return new TimeLineViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(TimeLineViewHolder holder, int position) {

        Ponto ponto = mFeedList.get(position);
        boolean hasBus = false;
        for (Veiculo v : mVehicleList) {
            if (position == v.seqPonto.intValue() && !v.sentidoLinha.equals("volta")) {
                hasBus = true;
            }
        }
        if (!hasBus) holder.mTimelineView.setMarker(ContextCompat.getDrawable(mContext,R.drawable.ic_marker_inactive));
        else holder.mTimelineView.setMarker(ContextCompat.getDrawable(mContext,R.drawable.ic_marker_active));

        holder.mAddress.setText(ponto.endereco);
    }

    @Override
    public int getItemCount() {
        return (mFeedList != null ? mFeedList.size() : 0);
    }

    class TimeLineViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.address)
        TextView mAddress;
        @BindView(R.id.time_marker)
        TimelineView mTimelineView;

        public TimeLineViewHolder(View itemView, int viewType) {
            super(itemView);

            ButterKnife.bind(this, itemView);
            mTimelineView.initLine(viewType);
        }
    }

}
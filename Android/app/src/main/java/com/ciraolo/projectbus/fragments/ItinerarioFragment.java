package com.ciraolo.projectbus.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ciraolo.projectbus.R;
import com.ciraolo.projectbus.adapter.TimeLineAdapter;
import com.ciraolo.projectbus.model.location.Linha;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ItinerarioFragment extends Fragment {
    private static final String ARG_LINHA_LOC = "linha_loc";

    // TODO: Rename and change types of parameters
    private Linha mLinhaLoc;

    @BindView(R.id.rcvItinerarioIda)
    RecyclerView rcvItinerarioIda;
    private TimeLineAdapter mTimeLineAdapter;

    /*@BindView(R.id.vsfVolta)
    VerticalStepperFormLayout vsfVolta;*/


    public ItinerarioFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment ItinerarioFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ItinerarioFragment newInstance(Linha param1) {
        ItinerarioFragment fragment = new ItinerarioFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_LINHA_LOC, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mLinhaLoc = (Linha) getArguments().getSerializable(ARG_LINHA_LOC);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_itinerario, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        setupLayout();

    }

    private void setupLayout() {
        rcvItinerarioIda.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rcvItinerarioIda.setHasFixedSize(true);
        mTimeLineAdapter = new TimeLineAdapter(mLinhaLoc.rotas.get(0).pontos, mLinhaLoc.veiculos, false);
        rcvItinerarioIda.setAdapter(mTimeLineAdapter);
    }
}

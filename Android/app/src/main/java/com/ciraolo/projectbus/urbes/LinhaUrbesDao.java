package com.ciraolo.projectbus.urbes;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
public class LinhaUrbesDao extends BasicoDao {
    private static String tag = LinhaUrbesDao.class.getSimpleName();

    public static final String TABELA_NOME = "LINHA";

    public static final String COLUNA_LIN_COD = "LIN_COD";
    public static final String COLUNA_LIN_NUMERO = "LIN_NUMERO";
    public static final String COLUNA_LIN_DESCRICAO = "LIN_DESCRICAO";
    public static final String COLUNA_LIN_FAVORITA = "LIN_FAVORITA";
    public static final String COLUNA_TAR_COD = "TAR_COD";
    public static final String COLUNA_LIN_ORDEM_APRES = "LIN_ORDEM_APRES";
    public static final String COLUNA_LIN_SIGLA_BAIRRO = "LIN_SIGLA_BAIRRO";
    public static final String COLUNA_LIN_SIGLA_TERMINAL = "LIN_SIGLA_TERMINAL";

    public LinhaUrbesDao(Context ctx) {
        super(ctx);
    }

    public List<LinhaUrbes> selectAll() {

        Cursor c = mDb.query(TABELA_NOME, null, null, null, null, null, null);

        c.moveToFirst();

        return fromCursorToCollection(c);
    }

    public List<LinhaUrbes> fromCursorToCollection(Cursor cursor) {
        List<LinhaUrbes> listLinhaUrbes = new ArrayList<>();

        if (cursor == null) {
            return listLinhaUrbes;
        } else if (cursor.getCount() <= 0) {
            cursor.close();
            return listLinhaUrbes;
        }

        cursor.moveToFirst();

        do {
            LinhaUrbes linhaUrbes = new LinhaUrbes();

            if (cursor.getColumnIndex(COLUNA_LIN_COD) != -1)
                linhaUrbes.linCod = (cursor.getInt(cursor.getColumnIndexOrThrow(COLUNA_LIN_COD)));

            if (cursor.getColumnIndex(COLUNA_LIN_NUMERO) != -1)
                linhaUrbes.linNumero = (cursor.getString(cursor.getColumnIndexOrThrow(COLUNA_LIN_NUMERO)));

            if (cursor.getColumnIndex(COLUNA_LIN_DESCRICAO) != -1)
                linhaUrbes.linDescricao = (cursor.getString(cursor.getColumnIndexOrThrow(COLUNA_LIN_DESCRICAO)));

            if (cursor.getColumnIndex(COLUNA_LIN_FAVORITA) != -1)
                linhaUrbes.linFavorita = (cursor.getString(cursor.getColumnIndexOrThrow(COLUNA_LIN_FAVORITA)));

            if (cursor.getColumnIndex(COLUNA_TAR_COD) != -1)
                linhaUrbes.tarCod = (cursor.getInt(cursor.getColumnIndexOrThrow(COLUNA_TAR_COD)));

            if (cursor.getColumnIndex(COLUNA_LIN_ORDEM_APRES) != -1)
                linhaUrbes.linOrdemApres = (cursor.getInt(cursor.getColumnIndexOrThrow(COLUNA_LIN_ORDEM_APRES)));

            if (cursor.getColumnIndex(COLUNA_LIN_SIGLA_BAIRRO) != -1)
                linhaUrbes.linSiglaBairro = (cursor.getString(cursor.getColumnIndexOrThrow(COLUNA_LIN_SIGLA_BAIRRO)));

            if (cursor.getColumnIndex(COLUNA_LIN_SIGLA_TERMINAL) != -1)
                linhaUrbes.linSiglaTerminal = (cursor.getString(cursor.getColumnIndexOrThrow(COLUNA_LIN_SIGLA_TERMINAL)));

            listLinhaUrbes.add(linhaUrbes);
        } while (cursor.moveToNext());
        cursor.close();

        return listLinhaUrbes;
    }
}
